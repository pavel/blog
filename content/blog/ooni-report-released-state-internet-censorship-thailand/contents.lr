title: OONI report released: The State of Internet Censorship in Thailand
---
pub_date: 2017-03-20
---
author: jgay
---
tags:

censorship
press release
ooni
news
thailand
---
categories: circumvention
---
_html_body:

<p>SEATTLE, WA, USA  – Monday, March 20th, 2017  – The Tor Project announces the release of <a href="https://ooni.torproject.org/post/thailand-internet-censorship/" rel="nofollow"><em>The State of Internet Censorship in Thailand</em></a>, a report from a joint research study by Open Observatory of Network Interference (OONI), Sinar Project, and the Thai Netizen Network. The study aims to increase transparency of Internet controls in Thailand and to collect data that can potentially corroborate rumors and reports of Internet censorship events. The key finding of this report reveal that Internet Service Providers (ISPs) in Thailand appear to be blocking websites at their own discretion.<br />
"We hope the findings of this report will enhance public debate around the necessity and proportionality of information controls," said Maria Xynou, Research and Partnerships Coordinator for OONI. Adding further that "A dozen websites, including The New York Post (nypost.com), were blocked in some networks, while accessible in others, indicates that Thai ISPs are likely blocking content at their own discretion."<br />
Multiple censorship events in Thailand have been reported over the last decade. More than 10,000 URLs were <a href="https://thainetizen.org/wp-content/uploads/2013/11/netizen-report-2011-en.pdf" rel="nofollow">reportedly blocked</a> by the Government in 2010. Following Thailand’s most recent coup d’etat, <a href="https://citizenlab.org/2014/07/information-controls-thailand-2014-coup/" rel="nofollow">Citizen Lab reported</a> that 56 websites were blocked between May and June of 2014. One importance of undertaking this study, which collects and analyzes network measurements, is to examine whether Internet censorship events are persisting in the country.<br />
Anyone can contribute to the research efforts by OONI by <a href="https://ooni.torproject.org/install/" rel="nofollow">installing and running ooniprobe</a>, thus increasing the transparency of Internet censorship in Southeast Asia and beyond.</p>

<h2>About Open Observatory of Network Interference</h2>

<p>The <a href="https://ooni.torproject.org/" rel="nofollow">Open Observatory of Network Interference</a> (OONI) is a free software project under The Tor Project that aims to empower decentralized efforts in increasing transparency of Internet censorship around the world. Since 2012, OONI has collected millions of network measurements from more than 190 countries, shedding light on multiple instances of network interference.</p>

<h2>About Sinar Project</h2>

<p><a href="http://sinarproject.org/en" rel="nofollow">Sinar Project</a> is an initiative using open technology and applications to systematically make important information public and more accessible to the Malaysian people. It aims to improve governance and encourage greater citizen involvement in the public affairs of the nation by making the Malaysian Government more open, transparent and accountable. We build open source civic tech applications, work to open government with open data and defend digital rights for citizens to apply their democratic rights.</p>

<h2>About Thai Netizen Network</h2>

<p><a href="https://thainetizen.org/" rel="nofollow">Thai Netizen Network</a> (TNN), founded in 2008, is a leading nonprofit organization in Thailand that advocates for digital rights and civil liberties. The group was officially registered as มูลนิธิเพื่ออินเทอร์เน็ตและวัฒนธรรมพลเมือง (Foundation for Internet and Civic Culture) in May 2014.</p>

<h2>About Tor Project, Inc</h2>

<p><a href="https://torproject.org" rel="nofollow">The Tor Project</a> develops and distributes free software and has built an open and free network that helps people defend against online surveillance that threatens personal freedom and privacy. Tor is used by human rights defenders, diplomats, government officials, and millions of ordinary people who value freedom from surveillance.<br />
The Tor Project's Mission Statement: "To advance human rights and freedoms by creating and deploying free and open anonymity and privacy technologies, supporting their unrestricted availability and use, and furthering their scientific and popular understanding."</p>

<h2>Media Contacts</h2>

<p>Joshua Gay<br />
Communications Director<br />
Tor Project<br />
<a href="mailto:jgay@torproject.org" rel="nofollow">jgay@torproject.org</a><br />
Maria Xynou (OONI)<br />
<a href="mailto:maria@openobservatory.org" rel="nofollow">maria@openobservatory.org</a><br />
Arturo Filasto (OONI)<br />
<a href="mailto:arturo@openobservatory.org" rel="nofollow">arturo@openobservatory.org</a></p>

---
_comments:

<a id="comment-247889"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-247889" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2017</p>
    </div>
    <a href="#comment-247889">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-247889" class="permalink" rel="bookmark">Congrats Joshua Gay for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Congrats Joshua Gay for being the Tor Project's new communications director! :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-248915"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-248915" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 22, 2017</p>
    </div>
    <a href="#comment-248915">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-248915" class="permalink" rel="bookmark">Good morning tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Good morning tor</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-251389"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-251389" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 27, 2017</p>
    </div>
    <a href="#comment-251389">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-251389" class="permalink" rel="bookmark">I hope OONI reports will</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I hope OONI reports will become standard citations in annual human rights reports such as these:</p>
<p>hrw.org/world-report/2017/country-chapters/thailand</p>
<p><a href="https://www.amnesty.org/en/countries/asia-and-the-pacific/thailand/report-thailand/" rel="nofollow">https://www.amnesty.org/en/countries/asia-and-the-pacific/thailand/repo…</a></p>
<p>Americans who has been following the abuse of libel laws to shutter Gawker (and to try to shut down Techdirt) may be interested in how libel laws are abused by the Thai government; see articles at the links above and here:</p>
<p><a href="https://rsf.org/en/thailand" rel="nofollow">https://rsf.org/en/thailand</a></p>
<p>[I offered detailed citations in a longer version but the moderators rejected that comment]</p>
<p>Free speech is under attack everywhere, and not everyone who believes they live in a "free country" (e.g. USA) has sufficiently recognized the threats posed by foreign entities applying bad US laws to threaten journalists.</p>
<p>It's all of them against all of us.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-252745"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-252745" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 29, 2017</p>
    </div>
    <a href="#comment-252745">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-252745" class="permalink" rel="bookmark">The current resident of the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The current resident of the US White House openly admires President Putin of Russia, and shares Putin's view that the press is "the Enemy of the People" [sic].  What can Americans expect to happen in the US if Drump continues to closely imitate Putin?  This:</p>
<p><a href="https://www.theguardian.com/world/2017/mar/23/former-russian-mp-denis-voronenkov-shot-dead-in-kiev" rel="nofollow">https://www.theguardian.com/world/2017/mar/23/former-russian-mp-denis-v…</a><br />
Denis Voronenkov: ex-Russian MP who fled to Ukraine killed in Kiev<br />
Vladimir Putin’s spokesman dismisses claims that Moscow is linked to the killing of Kremlin critic who left Russia last year and renounced citizenship<br />
Shaun Walker<br />
23 Mar 2017</p>
<p>&gt; A former Russian MP who had fled to Ukraine was shot dead on a busy street in central Kiev on Thursday. Denis Voronenkov, who had spoken out against Vladimir Putin and Kremlin policies, was shot three times outside the upmarket Premier Palace hotel.</p>
<p><a href="https://www.theguardian.com/world/2017/mar/26/opposition-leader-alexei-navalny-arrested-amid-protests-across-russia" rel="nofollow">https://www.theguardian.com/world/2017/mar/26/opposition-leader-alexei-…</a><br />
Opposition leader Alexei Navalny detained amid protests across Russia<br />
Crowds gather in cities to protest against corruption in largest anti-government rallies for five years, with hundreds held<br />
Shaun Walker and Alec Luhn in Moscow<br />
27 Mar 2017</p>
<p>&gt; Hundreds of protesters have been detained by riot police in cities across Russia, as some of the largest anti-government protests in years swept the country.  The call to protest came from the opposition politician and anti-corruption campaigner Alexei Navalny, who was himself detained at the Moscow demonstration. A monitoring group said at least 850 people were detained in Moscow alone, while the news agency Tass gave a figure of 500.</p>
<p><a href="https://www.theguardian.com/world/2017/mar/27/why-have-i-been-arrested-maybe-you-killed-kennedy-the-russian-officer-said" rel="nofollow">https://www.theguardian.com/world/2017/mar/27/why-have-i-been-arrested-…</a><br />
Why have I been arrested? Maybe you killed Kennedy, the Russian officer said<br />
I was detained by riot police while covering a demonstration in Moscow. The treatment of peaceful protesters was shocking<br />
Alec Luhn<br />
27 Mar 2017</p>
<p>&gt; I raised my phone to take a photo as riot police suddenly began detaining protesters, but before I could get the picture a pair of thick arms grabbed me. A trooper in a black helmet and flak jacket was barrelling me toward a police van. “I’m a foreign journalist,” I kept repeating in Russian. “Open your legs wider,” was all he said as he pushed me face-first up against the truck and started patting me down.</p>
<p>jsonline.com<br />
Photojournalist Alec Luhn, a UW-Madison graduate, was detained while covering anti-government protests in Russia<br />
James B. Nelson, Milwaukee Journal Sentinel<br />
27 Mar 2017</p>
<p>&gt; A photographer for The Guardian, Alec Luhn, who is from Stoughton, was detained for hours while covering anti-government protests led by Alexei Navalny that drew thousands to the streets of Moscow and other rallies across the country. Navalny was arrested and charged with resisting police orders and organizing a public gathering without a permit. On Monday, Navalny was sentenced to 15 days in jail and fined $350.<br />
<a href="https://twitter.com/Snowden" rel="nofollow">https://twitter.com/Snowden</a><br />
Edward Snowden‏<br />
26 Mar 2017</p>
<p>&gt; Sanctioned or unsanctioned, meeting peaceful protesters with force is clear injustice. 850+ arrests in Moscow, including journalist @ASLuhn.</p>
<p>All of which raises the question: is OONI working on a report on Russia?</p>
</div>
  </div>
</article>
<!-- Comment END -->
