title: Tor Weekly News — June 24th, 2015
---
pub_date: 2015-06-24
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the twenty-fifth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Adopt an onion with Nos Oignons</h1>

<p>Alongside the thousands of individual Tor relay operators who donate their time, expertise, and resources in order to build a fast and stable Tor network, a number of Tor relay organizations (independent of the Tor Project itself) have been set up in several countries. These groups make use of the benefits that formal non-profit status brings — such as funding opportunities, resource pooling, and legal advice — to set up and operate fast, secure Tor relays, and often to represent Tor and Tor users in local-language media. Torservers.net — the Germany-based relay organization and umbrella group for these projects — currently <a href="https://www.torservers.net/partners.html" rel="nofollow">lists</a> fourteen partner organizations in eleven countries, with more on the way.</p>

<p><a href="https://nos-oignons.net" rel="nofollow">Nos Oignons</a>, the French Torservers.net partner, runs five high-capacity relays on three machines that together handle a fiftieth of current Tor traffic. The bandwidth for one of these is generously provided by the registrar and hosting company <a href="https://www.gandi.net" rel="nofollow">Gandi</a>, but the other two are funded by the organization itself, at a cost of around 300 euros per month. With only three months’ worth of financing left, Nos Oignons is holding its first <a href="https://nos-oignons.net/campagne2015/" rel="nofollow">funding drive</a> to ensure these major relays stay online for the benefit of all Tor users.</p>

<p>If you donate more than 2 euros to Nos Oignons between 15th June and 15th August, you can suggest a name for their next Tor relay. The current set are named after the philosopher Herbert Marcuse, Ursula K. Le Guin’s “Ekumen” universe, and the protagonist of Walter Tevis’ novel “Mockingbird”, so use your imagination! At the end of the fundraiser, three entries will be chosen at random and the team will pick one of them; see the campaign page (or the <a href="https://nos-oignons.net/pipermail/announces/2015/000005.html" rel="nofollow">English announcement</a>) for information on how to take part.</p>

<h1>Miscellaneous news</h1>

<p>Anthony G. Basile put out <a href="https://lists.torproject.org/pipermail/tor-talk/2015-June/038199.html" rel="nofollow">version 20150616</a> of Tor-ramdisk, featuring updates to core software.</p>

<p>meejah <a href="https://lists.torproject.org/pipermail/tor-dev/2015-June/008961.html" rel="nofollow">announced</a> that txtorcon, the Twisted-based asynchronous Tor controller, now supports David Stainton’s “tor:” endpoint parser.  “This means two things: txtorcon now depends on txsocksx, and you can do "client-type" things directly with endpoints”. See meejah’s message for more details.</p>

<p>Jesse Victors published his second Tor Summer of Privacy <a href="https://lists.torproject.org/pipermail/tor-dev/2015-June/008964.html" rel="nofollow">status report</a> for the OnioNS (Onion Name System) project, detailing further work to decentralize the system and improvements to event logging.</p>

<p>Arturo Filastò published a <a href="https://lists.torproject.org/pipermail/ooni-dev/2015-June/000295.html" rel="nofollow">summary</a> of the costs incurred by OONI’s next-generation data-processing pipeline since March.</p>

<p>Thanks to <a href="https://lists.torproject.org/pipermail/tor-mirrors/2015-June/000904.html" rel="nofollow">Ana Lucia Cortez</a> for running a mirror of the Tor Project website and software archive!</p>

<p>This issue of Tor Weekly News has been assembled by Harmony and other contributors.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

