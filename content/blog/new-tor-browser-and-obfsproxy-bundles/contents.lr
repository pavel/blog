title: New Tor Browser and Obfsproxy Bundles
---
pub_date: 2012-08-12
---
author: erinn
---
tags:

release candidate
tbb
obfsproxy
tor alpha
---
categories:

applications
circumvention
releases
---
_html_body:

<p>The alpha Tor Browser Bundles have all been updated to the latest Tor 0.2.3.20-rc release candidate as well as being updated with some bugfixes. We're getting closer and closer to releasing the 0.2.3.x series as stable, so please give these bundles a lot of testing and help us shake out all of the remaining bugs! The regular bundles have also been updated.</p>

<p><a href="https://www.torproject.org/download" rel="nofollow">https://www.torproject.org/download</a></p>

<p>The Tor Obfsproxy Browser Bundles have also been brought up to date with all of the same software as the regular alpha Tor Browser Bundles. These are still a work in progress, so please remember to <a href="https://trac.torproject.org" rel="nofollow">report bugs</a>! You can download them from the <a href="https://www.torproject.org/projects/obfsproxy.html.en" rel="nofollow">obfsproxy page</a>.</p>

<p><strong>Tor Browser Bundle (2.3.20-alpha-1)</strong></p>

<ul>
<li>Update Tor to 0.2.3.20-rc</li>
<li>Update NoScript to 2.5</li>
<li>Change the urlbar search engine to Startpage (closes: #5925)</li>
<li>Firefox patch updates:</li>
<ul>
<li>Fix the Tor Browser SIGFPE crash bug (closes: #6492)</li>
<li>Add a redirect API for HTTPS-Everywhere (closes: #5477)</li>
<li>Enable WebGL (as click-to-play only) (closes: #6370)</li>
</ul>
</ul>

---
_comments:

<a id="comment-16880"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16880" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 13, 2012</p>
    </div>
    <a href="#comment-16880">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16880" class="permalink" rel="bookmark">Download links for the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Download links for the 2.3.20-alpha-1 bundles are at <a href="https://www.torproject.org/projects/torbrowser.html.en" rel="nofollow">https://www.torproject.org/projects/torbrowser.html.en</a> (search on page for ALPHA).  That page is reached via the "Learn more" link on the regular download page, but it's not immediately clear from "Learn more" that there are additional versions behind the link.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16883"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16883" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 13, 2012</p>
    </div>
    <a href="#comment-16883">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16883" class="permalink" rel="bookmark">If this is only the *alpha*</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If this is only the *alpha* TBB's, then the header should note this.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16889"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16889" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 13, 2012</p>
    </div>
    <a href="#comment-16889">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16889" class="permalink" rel="bookmark">As Firefox for Android is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As Firefox for Android is becoming really good and NoScript (planned for September) and HTTPSEverywhere (not ready yet) are preparing Fennec/Electrolysis versions, I would like to suggest that the wonderfulTor Browser Bundle should be built for Android, too (maybe start with the Firefox Beta channel).</p>
<p>At least provide a build script for Linux developers to try their own builds, i.e.<br />
<a href="https://gitweb.torproject.org/torbrowser.git/blob_plain/HEAD:/build-scripts/android.mk" rel="nofollow">https://gitweb.torproject.org/torbrowser.git/blob_plain/HEAD:/build-scr…</a></p>
<p><a href="https://trac.torproject.org/projects/tor/ticket/5709" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/5709</a> Tor Browser Bundle for Android<br />
<a href="https://trac.torproject.org/projects/tor/ticket/2471" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/2471</a> HTTPSEverywhere for Fennec</p>
<p>Climate-friendly greetings from Germany,<br />
René</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16892"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16892" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 14, 2012</p>
    </div>
    <a href="#comment-16892">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16892" class="permalink" rel="bookmark">Where can we download Alpha</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where can we download Alpha version from?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16900"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16900" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 14, 2012</p>
    </div>
    <a href="#comment-16900">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16900" class="permalink" rel="bookmark">Browser crashes now and then</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Browser crashes now and then on openSUSE 12.1/KDE 4.9...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16903"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16903" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 15, 2012</p>
    </div>
    <a href="#comment-16903">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16903" class="permalink" rel="bookmark">I had a hard time learning</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I had a hard time learning the download location of Alpha packages. Please make it clearer. Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16904"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16904" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 15, 2012</p>
    </div>
    <a href="#comment-16904">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16904" class="permalink" rel="bookmark">Hi Torproject!
I offer to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi Torproject!<br />
I offer to you. Please lay on your website checksums (MD5 / SHA256) TorBrowser, ObfsProxyBrowser etc. It will be VERY CONVENIENT!</p>
<p>Not all people have the opportunity to verify signature .asc<br />
And check the MD5-hash is available to all.</p>
<p>Thanks.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16907"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16907" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 15, 2012</p>
    </div>
    <a href="#comment-16907">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16907" class="permalink" rel="bookmark">CYBEROM FIREWALL BLOCKING</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>CYBEROM FIREWALL BLOCKING US!</p>
<p>Sirs,</p>
<p>We are a group of whistleblowers working in the mining sector. As you know there are a lot of instances of corruption, illegal activities and human rights violation to which the higher officials indulge in as a matter of routine, we had formed an association of certain whisteblowers to apprise the public in general and human right organisations of the human rights violations of the downtrodden, bonded labourers and child labourers who are not even given two times meal a day, not to speak of giving them monthly remuneration! Due to our hidden but dedicated efforts that went on successfully in curbing the menace to a large extent (all thanks to Tails and TOR networks), a nationwide protest followed against the mine owners and mafia due to which many bonded labourers were got freed.</p>
<p>As the companies knew that only the people working for them (insiders) could leak the sensitive information about the things going on behind the four walls of the areas, to the outside world, they have set up a networking department and implemented a system called CYBEROM firewall due to which we have become helpless because both TAILS and TOR don’t work/run on any computer.</p>
<p>In order to access the internet, we have to use our username and password (allotted to us by the NETWORKING DEPARTMENT). This firewall doesn’t allow any proxy, TOR and Tails. Even if we succeed in getting the dummy firewall account (with username and password), yet TOR will not work after logging into that firewalled dummy account.</p>
<p>How it works:<br />
(1) When we start computer, we have to go to network properties/settings to fill in (feed) some IP addresses, eg.. my computer has IP address 192.168.41.88. Another friend deployed in another building has IP address of 192.168.57.29. (here the internal IP parts “41” and “57” denote the building code; and the parts 88 and 29 show the computer code.) So if we send any email to anyone (without TOR or Tails which don’t run), the email header shows the following IP address: “192.168.41.88 [115.118.47.22 ISP’s external IP address]”<br />
(2) So you see, they have installed this firewall to trace or find out the person who blows the whistle against their illegal activities and illegal utilization of natural resources for personal use.</p>
<p>Kindly give us solution to use TAILS or TOR on such firewalled network.</p>
<p>If any person has any way out or technique to bypass this firewall, kindly tell us about that.</p>
<p>Thanking you in anticipaton,<br />
Mc Peterson.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-16955"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16955" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 19, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-16907" class="permalink" rel="bookmark">CYBEROM FIREWALL BLOCKING</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-16955">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16955" class="permalink" rel="bookmark">Have you tried using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Have you tried using bridges?<br />
<a href="https://www.torproject.org/docs/bridges" rel="nofollow">https://www.torproject.org/docs/bridges</a></p>
<p>If that does not work, you could try the obfsproxy browser bundle:<br />
<a href="https://www.torproject.org/projects/obfsproxy.html.en" rel="nofollow">https://www.torproject.org/projects/obfsproxy.html.en</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-16909"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16909" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 15, 2012</p>
    </div>
    <a href="#comment-16909">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16909" class="permalink" rel="bookmark">can anyone of you explain to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>can anyone of you explain to me how to make a http proxy connection. I need it badly to do a legal pen testing in my home town. but unfortunately the law in my place make it so damn hard. I using windows platform in which that's why I needed an advice on how to add polipo. I had make vidalia run polipo but the message is cannot attach the polipo with vidalia.<br />
Can anyone of you explain on how to make the polipo and the config.txt run automatically in TBB for windows. Thank you very much for your advice.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16912"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16912" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 16, 2012</p>
    </div>
    <a href="#comment-16912">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16912" class="permalink" rel="bookmark">Why in the world would you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why in the world would you want to enable WebGL with a click. That is asking for trouble.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16921"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16921" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 17, 2012</p>
    </div>
    <a href="#comment-16921">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16921" class="permalink" rel="bookmark">Very important:
Sir,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Very important:<br />
Sir, generally people like to use TOR over VPN (which according to TOR is not at all a safe thing to do) just because they want to hide their usage of TOR from the ISP.</p>
<p>In such a case, should one use Obfsproxy Bundle because it hides the traffic of TOR and shows as an innocent traffic? Does this bundle also use 3 nodes in addition to the initial node which interacts with the ISP?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-17016"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17016" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 23, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-16921" class="permalink" rel="bookmark">Very important:
Sir,</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-17016">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17016" class="permalink" rel="bookmark">Hi,
Tor uses 3 hop circuits,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,</p>
<p>Tor uses 3 hop circuits, it doesn't use "3 nodes in addition to the initial node".</p>
<p>The obfsproxy bundle does the exact same thing, but it also obfuscates your connection to the initial node. The obfsproxy bundle *does not change anything else*, apart from adding that obfuscation.</p>
<p>Also, note that the obfuscation of obfsproxy is not provably secure. If your ISP invests lots of resources to detect that obfuscation it will probably be able to find it.</p>
<p>Take care.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-16927"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16927" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 17, 2012</p>
    </div>
    <a href="#comment-16927">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16927" class="permalink" rel="bookmark">Some time before, I really</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Some time before, I really needed to buy a good house for my organization but I didn't have enough money and could not buy something. Thank heaven my mate suggested to try to take the <a href="http://goodfinance-blog.com/topics/mortgage-loans" rel="nofollow">mortgage loans</a> from trustworthy bank. Thus, I did so and was happy with my car loan.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-16988"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16988" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 21, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-16927" class="permalink" rel="bookmark">Some time before, I really</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-16988">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16988" class="permalink" rel="bookmark">I&#039;ve seen posts like this on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've seen posts like this on other forums and still cannot figure them out.</p>
<p>Look like spam but there is no URL or other contact, brand name, etc. </p>
<p>Anyone know?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-17414"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17414" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 19, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-16988" class="permalink" rel="bookmark">I&#039;ve seen posts like this on</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-17414">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17414" class="permalink" rel="bookmark">They - or whatever *they*</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>They - or whatever *they* should be called - the bots place theURL in name or title field, but I guess they still try when there is no such fields. I know this stuff from experience with my blog too - pieces of text that, up to a point, look incredibly reasonable for something that makes incredibly little sense :p</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-16947"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16947" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 18, 2012</p>
    </div>
    <a href="#comment-16947">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16947" class="permalink" rel="bookmark">Thanks for the Tor Browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for the Tor Browser Bundle (2.3.20-alpha-1)!!! Greatly decreased time from start to green onion. Doesn't hang frequently at 'Bootstrapped 50%' as other earlier versions did. Decreased latency ( do you still have 3 hops as default setting )? Oh, and all this on my Dialup account! Simply amazing how this alpha version runs so much faster than earlier versions I used. Thanks again.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-17224"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17224" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 03, 2012</p>
    </div>
    <a href="#comment-17224">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17224" class="permalink" rel="bookmark">I have installed</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have installed tor-0.2.3.20.rc-tor.1.rh17.i686.rpm following the instructions in the Tor docs to my new Fedora 17. Everything went OK with the install, and with Torbutton I have a confirmed connection to Tor,although I'm not sure I chose the correct version from the rpm repository.<br />
I don't seem to have Vidalia, Noscript or HTTPS Everywhere. Can you advise on this , please?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-17281"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-17281" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">September 08, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-17224" class="permalink" rel="bookmark">I have installed</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-17281">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-17281" class="permalink" rel="bookmark">If you want the normal</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you want the normal client experience, you should probably download the Tor Browser Bundle instead.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
