title: Tor Weekly News — November 6th, 2013
---
pub_date: 2013-11-06
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the nineteenth issue of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/tor-news" rel="nofollow">weekly newsletter</a> that covers what is happening in the up-to-date Tor community.</p>

<h1>Tails 0.21 is out</h1>

<p>The <a href="https://tails.boum.org/news/version_0.21/" rel="nofollow">Tails developers anounced the 34th release</a> of the live system based on Debian and Tor that preserves the privacy and anonymity of its users.</p>

<p>The new version fixes two holes that gave too much power to the POSIX user running the desktop: Tor control port cannot be directly accessed anymore to disallow configuration changes and IP address retrieval, and the persistence settings now requires privileged access. On top of these specific changes, the release include <a href="https://tails.boum.org/security/Numerous_security_holes_in_0.20.1/" rel="nofollow">security fixes</a> from the Firefox 17.0.10esr release and for a few other Debian packages.</p>

<p>More visible improvements include the ability to persist printer settings, support for running from more SD card reader types, and a panel launcher for the password manager. For the curious, more details can be found in the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">full changelog</a>.</p>

<p>As with every releases: be sure to upgrade!</p>

<h1>New Tor Browser Bundles based on Firefox 17.0.10esr</h1>

<p>Erinn Clark released <a href="https://blog.torproject.org/blog/new-tor-browser-bundles-firefox-17010esr" rel="nofollow">new versions of the Tor Browser Bundle</a> on November 1st. The previously “beta” bundles have moved to the “release candidate” stage and are almost identical to the stable ones, except for the version of the tor daemon. A couple of days later, David Fifield also released <a href="https://blog.torproject.org/blog/pluggable-transports-bundles-2417-rc-1-pt1-firefox-17010esr" rel="nofollow">updated “pluggable transport“ bundles</a>.</p>

<p>The new bundles include all <a href="https://www.mozilla.org/security/known-vulnerabilities/firefoxESR.html#firefox17.0.10" rel="nofollow">security fixes from Firefox 17.0.10esr</a>, and updated versions of libpng, NoScript and HTTPS Everywhere. It also contains a handful of improvements and fixes to the Tor Browser patches.</p>

<p>Users of older version of the Tor Browser bundles should already have been reminded to upgrade by the notification system. Don't forget about it!</p>

<p>This should be the last bundles based on the 17 branch of Firefox as it is going to be superseded by the 24 branch as the new long-term supported version in 6 weeks. Major progress has already been made by Mike Perry and Pearl Crescent to <a href="https://trac.torproject.org/projects/tor/query?keywords=~ff24-esr" rel="nofollow">update the Tor Browser changes and review the new code base</a>.</p>

<h1>Monthly status reports for October 2013</h1>

<p>The wave of regular monthly reports from Tor project members for the<br />
month of October has begun early this time to reach the tor-reports<br />
mailing-list: <a href="https://lists.torproject.org/pipermail/tor-reports/2013-October/000367.html" rel="nofollow">Damian Johnson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-October/000369.html" rel="nofollow">Linus Nordberg</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-October/000370.html" rel="nofollow">Karsten Loesing</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000371.html" rel="nofollow">Philipp Winter</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000372.html" rel="nofollow">Ximin Luo</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2012-November/000373.html" rel="nofollow">Lunar</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000374.html" rel="nofollow">Kelley Misata</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000375.html" rel="nofollow">Matt Pagan</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000376.html" rel="nofollow">Sherief Alaa</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000377.html" rel="nofollow">Nick Mathewson</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000378.html" rel="nofollow">Pearl Crescent</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000379.html" rel="nofollow">George Kadianakis</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000380.html" rel="nofollow">Colin Childs</a>, <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000381.html" rel="nofollow">Arlo Breault</a>, and <a href="https://lists.torproject.org/pipermail/tor-reports/2013-November/000382.html" rel="nofollow">Sukhbir Singh</a>.</p>

<h1>Tor Help Desk Roundup</h1>

<p>One person asked why the lock icon on the Tor Project's website was not outlined  in green. Sites that use HTTPS can purchase different types of SSL certificates. Some certificate issuers offer certificates that supply ownership information, such  as the physical address of the website operator, for a higher price. Sites that use  these certificates get the lock icon by their URL outlined in green. The Tor Project  adds protection to the validity of our SSL certificate a different way, by supplying our <a href="https://www.torproject.org/docs/faq.html#SSLcertfingerprint" rel="nofollow">SSL certificate fingerprint on our FAQ page</a>. You can double check that  fingerprint on any of the <a href="https://torproject.org/getinvolved/mirrors.html" rel="nofollow">Tor Project's mirror pages</a> as well.</p>

<p>One person wanted to known why a website they were visiting over Firefox was telling them that they were not using Tor, even though Vidalia told them that Tor was running. By default, the Tor Browser Bundle does not anonymize all the traffic on your computer. Only the traffic you send through the Tor Browser Bundle will be anonymized. If you  have Firefox and the Tor Browser open at the same time, the traffic you send through Firefox will not be anonymous. Using Firefox and Tor Browser Bundle at the same time is  not a great idea because the two interfaces are almost identical, and it is easy to get  the two browsers mixed up, even if you know what you are doing.</p>

<h1>Miscellaneous news</h1>

<p>The <a href="https://blog.torproject.org/blog/torbirdy-012-our-third-beta-release" rel="nofollow">third beta release of TorBirdy has been released</a> as version 0.1.2. Among several other fixes and improvements it restores proper usage of Tor when used with Thunderbird 24. Be sure to <a href="https://www.torproject.org/dist/torbirdy/torbirdy-0.1.2.xpi" rel="nofollow">upgrade</a>!</p>

<p>starlight <a href="https://lists.torproject.org/pipermail/tor-relays/2013-October/003187.html" rel="nofollow">reported</a> on running a Tor relays with the daemon compiled with the AddressSanitizer <a href="https://code.google.com/p/address-sanitizer/" rel="nofollow"></a> memory error detector available since GCC 4.8.</p>

<p>Isis Lovecruft <a href="https://lists.torproject.org/pipermail/tor-dev/2013-November/005713.html" rel="nofollow">has sent two proposals</a> for improvements to BridgeDB.  One is finished and addresses the switch to a “Distributed Database System and RDBMS”. The second is still in draft stage and “specifies a system for social distribution of the centrally-stored bridges within BridgeDB”.</p>

<p>Karsten Loesing <a href="https://lists.torproject.org/pipermail/tor-dev/2013-October/005700.html" rel="nofollow">announced</a> the availability of a new tech report he wrote with Steven J. Murdoch, and Rob Jansen: <a href="https://research.torproject.org/techreports/libutp-2013-10-30.pdf" rel="nofollow">Evaluation of a libutp-based Tor Datagram Implementation</a>. Be sure to have a look if you are interested in one of the “promising approach to overcome Tor’s performance-related problems”.</p>

<p>SiNA Rabbani <a href="https://lists.torproject.org/pipermail/tor-dev/2013-October/005703.html" rel="nofollow">has been asking</a> for comments on two documents he wrote about how use cases and design of a “point-and-click” hidden service blogging tool, as part of the <a href="https://trac.torproject.org/projects/tor/attachment/wiki/org/sponsors/Otter/Cute" rel="nofollow">Cute Otter project</a>.</p>

<p>David Goulet <a href="https://lists.torproject.org/pipermail/tor-dev/2013-November/005728.html" rel="nofollow">released third rc of Torsocks 2.0.0</a> with a lot of fixes and  improvements. Available to download from GitHub <a href="https://github.com/dgoulet/torsocks/archive/v2.0.0-rc3.tar.gz" rel="nofollow"></a> and also as Debian <a href="http://packages.debian.org/experimental/torsocks" rel="nofollow">package from the experimental distribution</a>.</p>

<p>Christian is working on a <a href="https://lists.torproject.org/pipermail/tor-dev/2013-November/005725.html" rel="nofollow">new round of improvements for Globe</a>, a web application to learn about relays and bridges of the Tor network.  The project seems close to be mature enough to replace Atlas <a href="https://lists.torproject.org/pipermail/tor-dev/2013-November/005735.html" rel="nofollow">according to some</a>.</p>

<p>A discussion on the tor-relays mailing list prompted Roger Dingledine to ask about <a href="https://lists.torproject.org/pipermail/tor-relays/2013-November/003240.html" rel="nofollow">changing the current default exit policy</a> of the tor daemon. The current “restricted exit node” policy has been in place since 2003. As this has surprised some operators, switching the default policy to “middle node” is under consideration.</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, dope457, Matt Pagan, and Philipp Winter.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

