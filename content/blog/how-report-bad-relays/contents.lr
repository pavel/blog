title: How to report bad relays
---
pub_date: 2014-07-28
---
author: phw
---
tags:

exit relays
mitm
badexit
---
categories: relays
---
_html_body:

<p>We now have a <a href="https://community.torproject.org/relay/community-resources/bad-relays/" rel="nofollow">page</a> which explains how bad relays should be reported to the Tor Project.  A bad relay can be malicious, misconfigured, or otherwise broken.  Once such a relay is reported, a subset of vigilant Tor developers (currently Roger, Peter, Damian, Karsten, and I) first tries to reproduce the issue.  If it's reproducible, we attempt to get in touch with the relay operator and work on the issue together.  However, if the relay has no contact information or we cannot reach the operator, we will resort to assigning flags (such as BadExit) to the reported relay which instructs clients to no longer use the relay in the future. In severe cases, we are also able to remove the relay descriptor from the network consensus which effectively makes the relay disappear. To get an idea of what bad behavior was documented in the past, have a look at this (no longer maintained) <a href="https://trac.torproject.org/projects/tor/wiki/doc/badRelays" rel="nofollow">wiki page</a> or these <a href="http://www.cs.columbia.edu/~mikepo/papers/tordecoys.raid11.pdf" rel="nofollow">research</a> <a href="http://www.cs.kau.se/philwint/spoiled_onions/pets2014.pdf" rel="nofollow">papers</a>.</p>

<p>We regularly scan the network for bad relays using <a href="https://gitweb.torproject.org/user/phw/exitmap.git" rel="nofollow">exitmap</a> but there are several other great tools such as <a href="https://gitweb.torproject.org/torflow.git/blob/HEAD:/NetworkScanners/ExitAuthority/README.ExitScanning" rel="nofollow">Snakes on a Tor</a>, <a href="https://code.google.com/p/torscanner/" rel="nofollow">torscanner</a>, <a href="http://www.thoughtcrime.org/software/tortunnel/" rel="nofollow">tortunnel</a>, and <a href="http://detector.io/DetecTor.html" rel="nofollow">DetecTor</a>.  We are also dependent on the wider community to help us spot relays which don't act as they should. So if you think that you stumbled upon a bad relay while using Tor, please report it to us by sending an email to <a href="mailto:bad-relays@lists.torproject.org" rel="nofollow">bad-relays@lists.torproject.org</a>. To find out which relay is currently being used as your exit relay, please visit our <a href="https://check.torproject.org" rel="nofollow">Check service</a>. Just tell us the relay's IP address (Check tells you what your IP address appears to be) and the behavior you observed. Then, we can begin to investigate!</p>

---
_comments:

<a id="comment-66518"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66518" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2014</p>
    </div>
    <a href="#comment-66518">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66518" class="permalink" rel="bookmark">I know some evil nodes are</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I know some evil nodes are DROPping HTTPS request!!(80/tcp allowed, 443/tcp blocked)</p>
<p>But, does this problem happens by ONLY exit nodes?<br />
What about:</p>
<p>You-&gt;Node1-&gt;Node2(BADGUY)-&gt;Node3(Exit)-&gt;Target</p>
<p>Node2 is blocking outbound connection?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66533"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66533" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66518" class="permalink" rel="bookmark">I know some evil nodes are</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66533">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66533" class="permalink" rel="bookmark">Node2 in this case can&#039;t see</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Node2 in this case can't see what your destination is, and can't modify your traffic (including what destination you ask for) or it will fail the checksum at the edge of the circuit.</p>
<p>So yes, Node2 can do things like refuse to let you extend your circuit to Node3. See e.g. the work on<br />
<a href="https://trac.torproject.org/projects/tor/ticket/12131" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/12131</a></p>
<p>But that's a different class of attack.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-66747"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66747" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 30, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66518" class="permalink" rel="bookmark">I know some evil nodes are</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66747">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66747" class="permalink" rel="bookmark">Help is TOR SAFE? :( I have</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Help is TOR SAFE? :( I have JAVA OFF AND COOKIE SECURED. IS TOR on windows safe? I mailing on mail2tor whit java off....</p>
<p>People see my TOR is bad and not safe!<br />
HELP!! IS TOR SAFE OR NOT SAFE? :( ♥</p>
<p>xxLisaxx</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66844"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66844" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 31, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66747" class="permalink" rel="bookmark">Help is TOR SAFE? :( I have</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66844">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66844" class="permalink" rel="bookmark">Nothing is safe. The tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Nothing is safe. The tor team appear to be doing a fabulous job given the circumstances.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-66520"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66520" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 28, 2014</p>
    </div>
    <a href="#comment-66520">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66520" class="permalink" rel="bookmark">&gt; To find out which relay is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; To find out which relay is currently being used as your exit relay, please visit our Check service.</p>
<p>This is NOT true.</p>
<p>xxxx.com -&gt; Node1-&gt;Ghost-&gt;Syrup<br />
check.torproject.org -&gt; Node1-&gt;Somma-&gt;Testing</p>
<p>If you are using "IsolateDestAddr", both are using DIFFERENT route.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66534"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66534" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 28, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66520" class="permalink" rel="bookmark">&gt; To find out which relay is</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66534">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66534" class="permalink" rel="bookmark">If you are using</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you are using IsolateDestAddr, we assume you know what you're doing and how to see your circuits. Tor Browser Bundle doesn't set that by default (and I think that's wise).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66652"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66652" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 30, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-66652">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66652" class="permalink" rel="bookmark">Still, doesn&#039;t tor load</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Still, doesn't tor load balance over multiple circuits even without isolation?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-67312"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-67312" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 03, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66652" class="permalink" rel="bookmark">Still, doesn&#039;t tor load</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-67312">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-67312" class="permalink" rel="bookmark">Not really. You use one</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not really. You use one circuit until it's sufficiently old, and then you move to the next.</p>
<p><a href="https://gitweb.torproject.org/torspec.git/blob/HEAD:/path-spec.txt" rel="nofollow">https://gitweb.torproject.org/torspec.git/blob/HEAD:/path-spec.txt</a> describes some of the design -- and if you are looking for something to do, it would be great for somebody to look through the code and then update that spec!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-66550"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66550" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 29, 2014</p>
    </div>
    <a href="#comment-66550">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66550" class="permalink" rel="bookmark">&quot;In severe cases, we are</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"In severe cases, we are also able to remove the relay descriptor from the network consensus which effectively makes the relay disappear."</p>
<p>So you guys are able to control the TOR network?</p>
<p>I mean if the NSA or some other agency comes to you with a court warrant...the whole TOR network would be shut down...or replaced with the nodes that they control....</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66564"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66564" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">July 29, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66550" class="permalink" rel="bookmark">&quot;In severe cases, we are</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66564">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66564" class="permalink" rel="bookmark">If a sufficient number of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If a sufficient number of directory authority operators agree (which is not always the case), then they are able to disable a selected relay. This happens every other day or week when we discover a malicious or broken relay. Also, our directory authority operators as well as their servers are in different jurisdiction which makes political attacks harder.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-66554"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66554" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 29, 2014</p>
    </div>
    <a href="#comment-66554">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66554" class="permalink" rel="bookmark">Please remove KasperskyTor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please remove KasperskyTor (or any anti-virus company's node)<br />
It's spying on HTTP, and write about it on their blog.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66578"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66578" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 29, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66554" class="permalink" rel="bookmark">Please remove KasperskyTor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66578">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66578" class="permalink" rel="bookmark">Links and details?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Links and details?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-66566"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66566" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 29, 2014</p>
    </div>
    <a href="#comment-66566">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66566" class="permalink" rel="bookmark">tor is dead</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>tor is dead</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66614"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66614" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 29, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66566" class="permalink" rel="bookmark">tor is dead</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66614">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66614" class="permalink" rel="bookmark">See</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>See <a href="https://blog.torproject.org/blog/being-targeted-nsa#comment-64456" rel="nofollow">https://blog.torproject.org/blog/being-targeted-nsa#comment-64456</a> for the last thread by this guy (or to be fair, gal).</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-66597"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66597" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 29, 2014</p>
    </div>
    <a href="#comment-66597">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66597" class="permalink" rel="bookmark">Kaspersky (the company)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Kaspersky (the company) openly runs a number of published Tor nodes.  This is not necessarily a bad thing, although I would prefer that businesses sponsor Noisebridge nodes in preference to running their own.  </p>
<p>The Kaspersky blog is notable for treating USIC malware just like any other state-sponsored malware, which is valuable to our cause.</p>
<p>Like any successful Russian businessman, Kaspersky (the person) presumably finds it advisable to stay within the party line, for example by calling for the de-anonymization of the entire www. And he does have known ties to the Russian military and to the former KGB, so presumably he is not a natural ally of our most lethal enemy.</p>
<p>If the poster's more serious claim is true, something may soon appear here:</p>
<p><a href="http://blog.kaspersky.com/tor-faq/" rel="nofollow">http://blog.kaspersky.com/tor-faq/</a></p>
<p>Exodus and like-minded entities are currently trying hard to intimidate the Tor userbase.  The Tor Project can respond by working closely with experts at CCC, Kaspersky, Citizen Lab, etc., to reverse engineer and publish analyses of any state-sponsored malware found to be attacking fast Tor nodes, the torproject.org network, and personal devices used by Tor node operators and your staffers.  </p>
<p>In fact, you can quietly set up honeypots designed to attract state-sponsored attacks, such as hidden services with not-elsewhere-published discussions of advanced steganography.  We need to let our enemies know that they are likely to pay a price for unleashing their nastiest techniques against us.</p>
<p>We seem to be locked in a death spiral of mutual recrimination here, since by USIC standards, threatening to run a honeypot probably counts as "intimidation" of the NSA, and thus as "terrorism" [sic].  I beg to differ with any such "legal analysis", since running a honeypot is perfectly legal, and everyone enjoys a natural right of self defense when he comes under direct attack.  But we are very obviously not getting a fair hearing in the corridors of power.</p>
<p>Some posters (but not you, I think) seem to advocate that we should conclude there is nothing to be done about NSA.  But that's not true.  There is much we can do about NSA, especially if we do not limit the scope of considered countermeasures to purely technological measures, but also include political, psychological, and economic strategies.</p>
<p>We have the inestimable advantage that OUR activity is entirely legal; THEY are the lawbreakers, the unauthorized intruders, the porn-passers, the kidnappers, the lethal drone-strikers.  Taking the wider view, their problems are much worse and more intractable than ours.  To name just two:</p>
<p>1. When the USIC adopted "collect it all", and determined that non-US citizens have no rights whatever, they in effect declared war on the entire world.  Other governments have previously declared war on the entire world and they all were in the end decisively eradicated.  Such will be the ultimate fate of NSA.</p>
<p>2. The people the USIC fears the most are demonstrably their own employees, who must inevitably feel resentment at the "continuous monitoring" BS.   We can employ a little "suasion" of our own here.</p>
<p>We can win the War on US, and everyone will be the better for it.</p>
<p>Best of all, once we resolve the issue of regime change, we can all turn our attention to the issue which really matters: climate change.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-66621"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66621" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 29, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-66597" class="permalink" rel="bookmark">Kaspersky (the company)</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-66621">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66621" class="permalink" rel="bookmark">This Russian-style</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This Russian-style pontificator's last irony is that while he abuses this Tor blog to proselytize for his personal climate beliefs, his Russian government heros are not on his own side (in practice).</p>
<p>Now his other bigger irony is actually Tor related.<br />
Russian government is not happy with Tor.</p>
<p>"<b>Putin Sets $110,000 Bounty for Cracking Tor</b>"<br />
<i> <a href="http://www.bloomberg.com/news/2014-07-29/putin-sets-110-000-bounty-for-cracking-tor-as-anonymous-internet-usage-in-russia-surges.html" rel="nofollow">http://www.bloomberg.com/news/2014-07-29/putin-sets-110-000-bounty-for-…</a>  </i></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-66841"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66841" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 31, 2014</p>
    </div>
    <a href="#comment-66841">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66841" class="permalink" rel="bookmark">It is possible to identify</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It is possible to identify tor.</p>
<p>You---&gt;NSA1---&gt;NSA2---&gt;NSA3---&gt;Web</p>
<p>NSA owns 3 nodes.<br />
NSA1 and 2 are non-exit nodes.</p>
<p>They are configured like this:<br />
&gt; StrictNodes 1<br />
&gt; ExitNode NSA3<br />
&gt; ExcludeNodes (IP range ALL - NSA nodes - Known Bridge IP)</p>
<p>So NSA nodes send data only to NSA nodes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-66856"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-66856" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 31, 2014</p>
    </div>
    <a href="#comment-66856">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-66856" class="permalink" rel="bookmark">No mainstream operating</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No mainstream operating system is safe. Windows have backdoors for enforcement and government.</p>
<p>No hardware or electronic is safe because enforcement and government add bugs during manufacturing or intercept the electronics during shipping to you.</p>
<p>Some software is not safe because they have backdoor for enforcement and government.</p>
<p>Tor is our only hope and we need more programmers and developers right now!</p>
<p>Please spread the world as this is a cyber war against various enemies that want to put you on a spy grid!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-67151"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-67151" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 01, 2014</p>
    </div>
    <a href="#comment-67151">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-67151" class="permalink" rel="bookmark">FRom Iran:
ORBOT Is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>FRom Iran:<br />
ORBOT Is Dead....<br />
not working on versions 12.xx 13.xx 14.xx<br />
Please help....!!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-67196"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-67196" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 01, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-67151" class="permalink" rel="bookmark">FRom Iran:
ORBOT Is</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-67196">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-67196" class="permalink" rel="bookmark">You have to use bridges. I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You have to use bridges. I believe any (types of) bridges will do.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-70220"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-70220" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 16, 2014</p>
    </div>
    <a href="#comment-70220">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-70220" class="permalink" rel="bookmark">Hi
After instal android L on</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi<br />
After instal android L on my nexus 5 tor dose not work<br />
please help me<br />
Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-70372"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-70372" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 17, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-70220" class="permalink" rel="bookmark">Hi
After instal android L on</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-70372">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-70372" class="permalink" rel="bookmark">You&#039;re unlikely to find</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You're unlikely to find useful user support here -- on the blog in general and especially by picking an unrelated blog post.</p>
<p>Try the contacts listed on <a href="https://guardianproject.info/apps/orbot/" rel="nofollow">https://guardianproject.info/apps/orbot/</a><br />
or the general Tor helpdesk (which will probably tell you to ask the Guardian people):<br />
<a href="https://www.torproject.org/about/contact#support" rel="nofollow">https://www.torproject.org/about/contact#support</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
