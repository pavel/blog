title: New Release: Tor Browser 12.0
---
author: duncan
---
pub_date: 2022-12-07
---
categories:
applications
releases
---
summary: Tor Browser 12.0 is now available. This new release updates Tor Browser to Firefox Extended Support Release 102.
---
body:

Tor Browser 12.0 is now available from the Tor Browser [download page](https://www.torproject.org/download/) and also from our [distribution directory](https://dist.torproject.org/torbrowser/12.0/). This new release updates Tor Browser to Firefox Extended Support Release 102.

## What's new?

### Upgraded to Extended Support Release 102

![Image reading "Firefox Extended Support Release 102"](12-esr-102.png)

Once again, the time has come to upgrade Tor Browser to Firefox's newest Extended Support Release. We've spent the past few months since [Tor Browser 11.5's release](/new-release-tor-browser-115/) reviewing [ESR 102's release notes](https://www.mozilla.org/en-US/firefox/102.0esr/releasenotes/) to ensure each change is compatible with Tor Browser. As part of that process, anything that may conflict with Tor Browser's strict privacy and security principles has been carefully disabled.

### Multi-locale support for desktop

<video width="100%" autoplay loop muted>
  <source src="12-multi-locale.mp4" type="video/mp4" />
  ![Visualization of the menu used to select Tor Browser 12.0's display language](12-multi-locale.png)
</video>

Previously, if you wanted to use Tor Browser for desktop in a language other than English, you needed to find and download one of the matching language versions from our download page. Switching language after installing Tor Browser wasn't an easy task either, and would either require adding the new language pack to your existing installation, or redownloading Tor Browser from scratch.

As of today we're pleased to announce that this is a thing of the past: Tor Browser for desktop is now truly multi-locale, meaning all supported languages are now included in a single bundle. For new users, Tor Browser 12.0 will update itself automatically when launched to match your system language. And if you've upgraded from Tor Browser 11.5.8, the browser will attempt to maintain your previously chosen display language.

Either way, you can now switch display language without any additional downloads via the Language menu in General settings – but we'd still recommend giving Tor Browser a quick restart before the change can take complete effect.

Naturally, bundling multiple languages in a single download should increase Tor Browser's filesize – we are very conscious of this; however, we've found a way to make efficiency savings elsewhere, meaning the difference in filesize between Tor Browser 11.5 and 12.0 is minor.

### Native Apple Silicon support

![Apple Silicon logo](12-native-apple-support.png)

This was no small task, but we're happy to say that Tor Browser 12.0 now supports Apple Silicon natively. Like Mozilla's approach for Firefox, we've opted for a Universal Binary too – meaning both x86-64 (i.e. Intel compatible) and ARM64 (i.e. Apple Silicon compatible) builds are bundled together with the correct version chosen automatically when run.

### HTTPS-Only by default for Android

![Image reading "HTTPS Only Mode" and a switch turned on](12-https-only-android.png)

Back in July, we shared an update about Tor Browser for Android and our aspirations for its near future in the [Tor Browser 11.5 release post](/new-release-tor-browser-115/). Since the beginning of the year our developers have been working hard to recommence regular updates for Android, improve the app's stability, and catch up to Fenix's (Firefox for Android's) release cycle.

The next phase in our plan for Android is to begin porting selected, high-priority features that have recently been launched for desktop over to Android – starting with enabling HTTPS-Only Mode by default. This change will help provide the same level of protection against SSL stripping attacks by [malicious exit relays](/bad-exit-relays-may-june-2020/) that we introduced to desktop in Tor Browser 11.5.

### Prioritize .onion sites for Android

<video width="100%" autoplay loop muted>
  <source src="12-prioritize-onions-android.mp4" type="video/mp4" />
  ![Visualization of the option to prioritize onion sites in Tor Browser for Android's Privacy and Security settings screen](12-prioritize-onions-android.png)
</video>

Another small but mighty improvement to Tor Browser 12.0 for Android is the option to "prioritize .onion sites" where available. When enabled, you will be redirected automatically to the matching .onion site for any web site that has [Onion-Location](https://community.torproject.org/onion-services/advanced/onion-location/) configured – helping you to discover new .onion sites in the wild.

You can turn "Prioritize .onion sites" on under the Privacy and Security section within Tor Browser for Android's Settings menu. Please note that this update does not include the purple ".onion avilable" button in the address bar, which is still unique to Tor Browser for desktop.

### And more...

12.0 is the first stable release of Tor Browser that supports Albanian (sq) and Ukranian (uk). We owe a huge thank you to all the volunteers who worked hard to translate Tor Browser into each language <3

If you spot a string that still needs to be translated, or would like to contribute towards the localization of another language, please visit our [Community portal](https://community.torproject.org/localization/) to find out how to get started.

We've also been busy making various behind-the-scenes improvements to features like tor-launcher (which starts tor within Tor Browser), the code for which has undergone a significant refactoring. As such, if you run a non-standard Tor Browser setup (like using system tor in conjunction with Tor Browser, or very partiular network settings) and experience an unexpected error message when launching Tor - please let us know by [filing an issue in our Gitlab repo](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues).

Lastly, Tor Browser's [letterboxing](https://support.torproject.org/tbb/maximized-torbrowser-window/) feature has received a number of minor improvements to its user experience, including (but not limited to) fixing potantial leaks and bypasses, removing the 1px border in fullscreen videos, and disabling the feature entirely on trusted pages like the Connect to Tor screen, among others.

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/). Thanks to all of the teams across Tor, and the many volunteers, who contributed to this release.

## Full changelog

The full changelog since [Tor Browser 11.5.10](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-12.0/projects/browser/Bundle-Data/Docs/ChangeLog.txt) is:

- All Platforms
  - Update Translations
  - Update tor to 0.4.7.12
  - [Bug tor-browser#17228](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/17228): Consideration for disabling/trimming referrers within TBB
  - [Bug tor-browser#24686](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/24686): In Tor Browser context, should network.http.tailing.enabled be set to false?
  - [Bug tor-browser#27127](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/27127): Audit and enable HTTP/2 push
  - [Bug tor-browser#27258](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/27258): font whitelist means we don't have to set gfx.downloadable_fonts.fallback_delay
  - [Bug tor-browser#40057](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40057): ensure that CSS4 system colors are not a fingerprinting vector
  - [Bug tor-browser#40058](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40058): ensure no locale leaks from new Intl APIs
  - [Bug tor-browser#40183](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40183): Consider disabling TLS ciphersuites containing SHA-1
  - [Bug tor-browser#40251](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40251): Clear obsolete prefs after torbutton!27
  - [Bug tor-browser#40406](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40406): Remove Presentation API related prefs
  - [Bug tor-browser#40491](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40491): Don't auto-pick a v2 address when it's in Onion-Location header
  - [Bug tor-browser#40494](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40494): Update Startpage and Blockchair onion search providers
  - [Bug tor-browser-build#40580](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40580): Add support for Ukranian (uk)
  - [Bug tor-browser-build#40646](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40646): Don't build Español AR anymore
  - [Bug tor-browser-build#40674](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40674): Add Secondary Snowflake Bridge
  - [Bug tor-browser#40783](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40783): Review 000-tor-browser.js and 001-base-profile.js for 102
  - [Bug tor-browser#41098](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41098): Compare Tor Browser's and GeckoView's profiles
  - [Bug tor-browser#41125](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41125): Review Mozilla 1732792: retry polling requests without proxy
  - [Bug tor-browser#41154](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41154): Review Mozilla 1765167: Deprecate Cu.import
  - [Bug tor-browser#41164](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41164): Add some #define for the base-browser section
  - [Bug tor-browser#41306](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41306): Typo "will not able" in "Tor unexpectedly exited" dialog
  - [Bug tor-browser#41317](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41317): Tor Browser leaks banned ports in network.security.ports.banned
  - [Bug tor-browser#41345](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41345): fonts: windows whitelist contains supplemental fonts
  - [Bug tor-browser#41398](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41398): Disable Web MIDI API
  - [Bug tor-browser#41406](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41406): Do not define `--without-wasm-sandboxed-libraries` if `WASI_SYSROOT` is defined
  - [Bug tor-browser#41420](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41420): Remove brand.dtd customization on nightly
  - [Bug tor-browser#41457](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41457): Remove more Mozilla permissions
  - [Bug tor-browser#41473](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41473): Add support for Albanian (sq)
- Windows + macOS + Linux
  - Update Firefox to 102.5.0esr
  - [Bug tor-browser#17400](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/17400): Decide how to use the multi-lingual Tor Browser in the alpha/release series
  - [Bug tor-browser-build#40595](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40595): Migrate to 102 on desktop
  - [Bug tor-browser-build#40638](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40638): Visit our website link after build-to-build upgrade in Nightly channel points to old v2 onion
  - [Bug tor-browser-build#40648](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40648): Do not customize update.locale in multi-lingual builds
  - [Bug tor-browser#40853](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40853): use Subprocess.jsm to launch tor
  - [Bug tor-browser#40933](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40933): Migrate remaining tor-launcher functionality to tor-browser
  - [Bug tor-browser#41011](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41011): The Internet and Tor status are visible when opening the settings
  - [Bug tor-browser#41044](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41044): Content exceeding the height of the connection settings modals
  - [Bug tor-browser#41116](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41116): Review Mozilla 1226042: add support for the new 'system-ui' generic font family
  - [Bug tor-browser#41117](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41117): Review Mozilla 1512851:  Add Share Menu to File Menu on macOS
  - [Bug tor-browser#41283](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41283): Toolbar buttons missing their label attribute
  - [Bug tor-browser#41284](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41284): Stray security-level- fluent ids
  - [Bug tor-browser#41287](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41287): New identity button inactive if added after customization
  - [Bug tor-browser#41292](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41292): moreFromMozilla pane in about:preferences in 12.0a2
  - [Bug tor-browser#41293](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41293): Incomplete branding in German with 12.0a2
  - [Bug tor-browser#41337](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41337): Add a title to the new identity confirmation
  - [Bug tor-browser#41342](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41342): Update the New Identity dialog to the proton modal style
  - [Bug tor-browser#41344](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41344): Authenticated Onion Services do not prompt for auth key in 12.0 alpha series
  - [Bug tor-browser#41352](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41352): Update or drop the show manual logic in torbutton
  - [Bug tor-browser#41369](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41369): Consider a different list-order for locales in language menu
  - [Bug tor-browser#41374](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41374): Missing a few torconnect strings in the DTD
  - [Bug tor-browser#41377](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41377): Hide `Search for more languages...` from Language selector in multi-locale build
  - [Bug tor-browser#41378](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41378): Inform users when Tor Browser sets their language automatically
  - [Bug tor-browser#41385](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41385): Bootstrap failure is logged but not raised up to about:torconnect
  - [Bug tor-browser#41386](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41386): The new tor-launcher has a problem when another Tor is running
  - [Bug tor-browser#41387](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41387): New identity and new circuit ended up inside history
  - [Bug tor-browser#41400](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41400): Missing onionAuthViewKeys causes "Onion Services Keys" dialog to be empty.
  - [Bug tor-browser#41401](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41401): Missing some mozilla icons because we still loading them from "chrome://browser/skin" rather than "chrome://global/skin/icons"
  - [Bug tor-browser#41404](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41404): Fix blockchair-onion search extension
  - [Bug tor-browser#41409](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41409): Circuit display is broken on Tails
  - [Bug tor-browser#41410](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41410): Opening and closing HTTPS-Only settings make the identity panel shrink
  - [Bug tor-browser#41412](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41412): New Identity shows "Tor Browser" instead of "Restart Tor Browser" in unstranslated locales
  - [Bug tor-browser#41417](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41417): Prompt users to restart after changing language
  - [Bug tor-browser#41429](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41429): TorConnect and TorSettings are initialized twice
  - [Bug tor-browser#41435](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41435): Add a Tor Browser migration function
  - [Bug tor-browser#41436](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41436): The new tor-launcher handles arrays in the wrong way
  - [Bug tor-browser#41437](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41437): Use the new media query for dark theme for the "Connected" pill in bridges
  - [Bug tor-browser#41449](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41449): Onion authentication's learn more should link to the offline manual
  - [Bug tor-browser#41451](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41451): Still using requestedLocales in torbutton
  - [Bug tor-browser#41455](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41455): Tor Browser dev build cannot launch tor
  - [Bug tor-browser#41458](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41458): Prevent `mach package-multi-locale` from actually creating a package
  - [Bug tor-browser#41462](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41462): Add anchors to bridge-moji and onion authentication entries
  - [Bug tor-browser#41466](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41466): Port YEC 2022 campaign to Tor Browser 12.0 (Desktop)
  - [Bug tor-browser#41495](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41495): Clicking on "View Logs" in the "Establishing Connection" page takes you to about:preferences#connection and not logs
  - [Bug tor-browser#41498](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41498): The Help panel is empty in 12.0a4
  - [Bug tor-browser#41508](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41508): Update the onboarding link for 12.0
- Windows
  - [Bug tor-browser#41149](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41149): Review Mozilla 1762576: Firefox is not allowing Symantec DLP to inject DLL into the browser for Data Loss Prevention software
  - [Bug tor-browser#41278](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41278): Create Tor Browser styled pdf logo similar to the vanilla Firefox one
  - [Bug tor-browser#41426](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41426): base-browser nightly fails to build for windows-i686
- macOS
  - [Bug tor-browser#23451](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/23451): Adapt font whitelist to changes on macOS (zh locales)
  - [Bug tor-browser#41004](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41004): The Bangla font does not display correctly on MacOs
  - [Bug tor-browser#41108](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41108): Remove privileged macOS installation from 102
  - [Bug tor-browser#41294](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41294): Bookmarks manager broken in 12.0a2 on MacOS
  - [Bug tor-browser#41348](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41348): cherry-pick macOS OSSpinLock  replacements
  - [Bug tor-browser#41370](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41370): Find a way to ship custom default bookmarks without changing language-packs on macOS
  - [Bug tor-browser#41372](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41372): "Japanese" language menu item is localised in multi-locale testbuild (on mac OS)
  - [Bug tor-browser#41379](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41379): The new tor-launcher is broken also on macOS
- Linux
  - [Bug tor-browser#40359](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40359): Tor Browser Launcher has Wrong Icon
  - [Bug tor-browser-build#40626](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40626): Define the replacements for generic families on Linux
  - [Bug tor-browser#41163](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41163): Fixing loading of bundled fonts on linux
- Android
  - Update GeckoView to 102.5.0esr
  - [Bug tor-browser#40014](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40014): Check which of our mobile prefs and configuration changes are still valid for GeckoView
  - [Bug tor-browser-build#40631](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40631): Stop bundling HTTPS Everywhere on Android
  - [Bug tor-browser#41394](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41394): Implement a setting to always prefer onion sites
  - [Bug tor-browser#41465](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41465): Port YEC 2022 campaign to Tor Browser 12.0 (Android)
- Build System
  - All Platforms
    - Update Go to 1.19.3
    - [Bug tor-browser-build#23656](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/23656): Use .mozconfig files in tor-browser repo for rbm builds
    - [Bug tor-browser-build#28754](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/28754): make testbuild-android-armv7 stalls during sha256sum step
    - [Bug rbm#40049](https://gitlab.torproject.org/tpo/applications/rbm/-/issues/40049): gpg_keyring should allow for array of keyrings
    - [Bug tor-browser-build#40397](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40397): Create a new build target to package tor daemon, pluggable transports and dependencies
    - [Bug tor-browser-build#40407](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40407): Bump binutils version to pick up security improvements for Windows users
    - [Bug tor-browser-build#40585](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40585): Prune the manual more
    - [Bug tor-browser-build#40587](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40587): Migrate tor-browser-build configs from gitolite to gitlab repos
    - [Bug tor-browser-build#40591](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40591): Rust 1.60 not working to build 102 on Debian Jessie
    - [Bug tor-browser-build#40592](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40592): Consider re-using our LLVM/Clang to build Rust
    - [Bug tor-browser-build#40593](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40593): Update signing scripts to take into account new project names and layout
    - [Bug tor-browser-build#40607](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40607): Add alpha-specific release prep template
    - [Bug tor-browser-build#40610](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40610): src-*.tar.xz tarballs are missing in https://dist.torproject.org/torbrowser/12.0a1/
    - [Bug tor-browser-build#40612](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40612): Migrate Release Prep template to Release Prep - Stable
    - [Bug tor-browser-build#40619](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40619): Make sure translations are taken from gitlab.tpo and not git.tpo
    - [Bug torbrowser-build#40627](https://gitlab.torproject.org/tpo/applications/torbrowser-build/-/issues/40627): Add boklm to torbutton.gpg
    - [Bug tor-browser-build#40634](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40634): Update the project/browser path in tools/changelog-format-blog-post and other files
    - [Bug tor-browser-build#40636](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40636): Remove https-everywhere from projects/browser/config
    - [Bug tor-browser-build#40639](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40639): Remove tor-launcher references
    - [Bug tor-browser-build#40643](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40643): Update Richard's key in torbutton.gpg
    - [Bug tor-browser-build#40645](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40645): Remove unused signing keys and create individual keyrings for Tor Project developers
    - [Bug tor-browser-build#40655](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40655): Published tor-expert-bundle tar.gz files should not include their tor-browser-build build id
    - [Bug tor-browser-build#40656](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40656): Improve get_last_build_version in tools/signing/nightly/sign-nightly
    - [Bug tor-browser-build#40660](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40660): Update changelog-format-blog-post script to point gitlab rather than gitolite
    - [Bug tor-browser-build#40662](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40662): Make base-browser nightly build from tag
    - [Bug tor-browser-build#40663](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40663): Do not ship bookmarks in tor-browser-build anymore
    - [Bug tor-browser-build#40667](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40667): Update Node.js to 12.22.12
    - [Bug tor-browser-build#40669](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40669): Remove HTTPS-Everywhere keyring
    - [Bug tor-browser-build#40671](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40671): Update langpacks URL
    - [Bug tor-browser-build#40675](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40675): Update tb_builders list in set-config
    - [Bug tor-browser-build#40690](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40690): Revert fix for zlib build break
    - [Bug tor-browser#41308](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41308): Use the same branch for Desktop and GeckoView
    - [Bug tor-browser#41321](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41321): Delete various master branches after automated build/testing scripts are updated
    - [Bug tor-browser#41340](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41340): Opt in to some of the NIGHTLY_BUILD features
    - [Bug tor-browser#41357](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41357): Enable browser toolbox debugging by default for dev builds
    - [Bug tor-browser#41446](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41446): Multi-lingual alpha bundles break make fetch
  - Windows + macOS + Linux
    - [Bug tor-browser-build#40499](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40499): Update firefox to enable building from new 'base-browser' tag
    - [Bug tor-browser-build#40500](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40500): Add base-browser package project
    - [Bug tor-browser-build#40501](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40501): Makefile updates to support building base-browser packages
    - [Bug tor-browser-build#40503](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40503): Update Release Prep issue template with base-browser and privacy browser changes
    - [Bug tor-browser-build#40547](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40547): Remove container/remote_* from rbm.conf
    - [Bug tor-browser-build#40581](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40581): Update reference to master branches
    - [Bug tor-browser-build#40641](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40641): Fetch Firefox locales from l10n-central
    - [Bug tor-browser-build#40678](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40678): Force all 11.5 users to update through 11.5.8 before 12.0
    - [Bug tor-browser-build#40685](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40685): Remove targets/nightly/var/mar_locales from rbm.conf
    - [Bug tor-browser-build#40686](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40686): Add a temporary project to fetch Fluent tranlations for base-browser
    - [Bug tor-browser-build#40691](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40691): Update firefox config to point to base-browser branch rather than a particular tag in nightly
    - [Bug tor-browser-build#40699](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40699): Fix input_files in projects/firefox-l10n/config
    - [Bug tor-browser#41099](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41099): Update+comment the update channels in update_responses.config.yaml
  - Windows
    - [Bug tor-browser-buid#29318](https://gitlab.torproject.org/tpo/applications/tor-browser-buid/-/issues/29318): Use Clang for everything on Windows
    - [Bug tor-browser-build#29321](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/29321): Use mingw-w64/clang toolchain to build tor
    - [Bug tor-browser-build#29322](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/29322): Use mingw-w64/clang toolchain to build OpenSSL
    - [Bug tor-browser-build#40409](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40409): Upgrade NSIS to 3.08
    - [Bug tor-browser-build#40666](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40666): Fix compiler depedencies for Firefox on Windows
  - macOS
    - [Bug tor-browser-build#40067](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40067): Rename "OS X" to "macOS"
    - [Bug tor-browser-build#40158](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40158): Add support for macOS AArch64
    - [Bug tor-browser-build#40439](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40439): Create universal x86-64/arm64 mac builds
    - [Bug tor-browser-build#40605](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40605): Reworked the macOS toolchain creation
    - [Bug tor-browser-build#40620](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40620): Update macosx-sdk to 11.0
    - [Bug tor-browser-build#40687](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40687): macOS nightly builds with packaged locales fail
    - [Bug tor-browser-build#40694](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40694): aarch64 tor-expert-bundle for macOS is not exported as part of the browser build
  - Linux
    - [Bug tor-browser-build#31321](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/31321): Add cc -&gt; gcc link to projects/gcc
    - [Bug tor-browser-build#40621](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40621): Update namecoin patches for linted TorButton
    - [Bug tor-browser-build#40659](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40659): Error building goservice for linux in nightly build
    - [Bug tor-browser#41343](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41343): Add -without-wam-sandboxed-libraries to mozconfig-linux-x86_64-dev for local builds
  - Android
    - [Bug tor-browser-build#40574](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40574): Improve tools/signing/android-signing
    - [Bug tor-browser-build#40604](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40604): Fix binutils build on android
    - [Bug tor-browser-build#40640](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40640): Extract Gradle in the toolchain setup
    - [Bug tor-browser#41304](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41304): Add Android-specific targets to makefiles

