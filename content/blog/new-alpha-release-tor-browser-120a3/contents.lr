title: New Alpha Release: Tor Browser 12.0a3 (Android, Windows, macOS, Linux)
---
pub_date: 2022-09-29
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 12.0a3 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 12.0a3 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/12.0a3/).

Tor Browser 12.0a3 updates Firefox on Android, Windows, macOS, and Linux to 102.3.0esr.

We use this opportunity to update various other components of Tor Browser as well :
- NoScript 11.4.11

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-41/) to Firefox. We also backport the following Android-specific security updates from Firefox 105:
- [CVE-2022-40961](https://www.mozilla.org/en-US/security/advisories/mfsa2022-40/#CVE-2022-40961)

Additionally, the HTTPS-Everywhere extension has been removed and its functionality replaced with HTTP-Only mode on Android.

The full changelog since [Tor Browser 12.0a2](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=main) is:

- All Platforms
  - Update Firefox to 102.3.0esr
  - Update NoScript to 11.4.11
  - Update Translations
  - [Bug tor-browser-build#40624](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40624): Change placeholder bridge addresses to make snowflake and meek work with ReachableAddresses/FascistFirewall
  - [Bug tor-browser#41125](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41125): Review Mozilla 1732792: retry polling requests without proxy
- Windows + macOS + Linux
  - [Bug tor-browser#41116](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41116): Review Mozilla 1226042: add support for the new 'system-ui' generic font family
  - [Bug tor-browesr#41283](https://gitlab.torproject.org/tpo/applications/tor-browesr/-/issues/41283): Toolbar buttons missing their label attribute
  - [Bug tor-browser#41284](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41284): Stray security-level- fluent ids
  - [Bug tor-browser#41287](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41287): New identity button inactive if added after customization
  - [Bug tor-browser#41292](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41292): moreFromMozilla pane in about:preferences in 12.0a2
  - [Bug tor-browser#41307](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41307): font whitelist typos
- Linux
  - [Bug tor-browser-build#40626](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40626): Define the replacements for generic families on Linux
  - [Bug tor-browser#41163](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41163): Fixing loading of bundled fonts on linux
- Android
  - [Bug tor-browser#41159](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41159): Remove HTTPS-Everywhere extension from esr102-based Tor Browser Android
  - [Bug tor-browser#41312](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41312): Backport Firefox 105 Android security fixes to 102.3-based Tor Browser
- Build System
  - All Platforms
    - [Bug tor-browser-build#40587](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40587): Migrate tor-browser-build configs from gitolite to gitlab repos
    - [Bug tor-browser#41321](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41321): Delete various master branches after automated build/testing scripts are updated
  - Linux
    - [Bug tor-browser-build#40621](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40621): Update namecoin patches for linted TorButton
  - Android
    - [Bug tor-browser#41304](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41304): Add Android-specific targets to makefiles
