title: New Release: Tor Browser 11.0.12 (Android)
---
pub_date: 2022-05-12
---
author: pierov
---
categories:

applications
releases
---
summary: Tor Browser 11.0.12 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.0.12 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.0.12/).

This version solves the startup crash that many users reported:
https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40212

Tor Browser 11.0.12 updates GeckoView to 96.0.3.

We use the opportunity as well to update various other components of Tor Browser:
- NoScript 11.4.5
- OpenSSL 1.1.1o

We switch to Go 1.17.9, too, for building our Go-related projects.

The full changelog since [Tor Browser 11.0.8](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.0) is:

- Android
  - Update NoScript to 11.4.5
  - Update OpenSSL to 1.1.1o
  - [Bug fenix#40202](https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40202): Install fails for nightly testbuild on emulator running Android API v30
  - [Bug fenix#40212](https://gitlab.torproject.org/tpo/applications/fenix/-/issues/40212): Tor Browser crashing on launch
  - [Bug tor-browser#40908](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40908): Rebase to Geckoview v96.0.3 for TBA 11.0
  - [Bug tor-browser#40913](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40913): Investigate+possible revert fix for Bugzilla 1732388
- Build System
  - Android
    - Update Go to 1.17.9

