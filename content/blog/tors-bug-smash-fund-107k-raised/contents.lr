title: Tor’s Bug Smash Fund Year 3: $107K Raised!
---
pub_date: 2021-10-14
---
author: alsmith
---
tags: fundraising
---
categories: fundraising
---
summary: Hello, Tor world! We owe you a thank you. In August, you helped us raise $107,672.20 for the Bug Smash Fund this year! Thank you to everybody who made a contribution to the Bug Smash Fund during the month of August. This work is critical in helping us to provide safer tools for millions of people around the world exercising their human rights to privacy and freedom online.
---
_html_body:

<p>Hello, Tor world! We owe you a thank you.</p>
<p>This August, we asked you to contribute to the Tor Project's Bug Smash Fund. We created this fund in order to create a reserve that would help us find bugs, complete maintenance work, and do all of the “dirty jobs” that are necessary to keep Tor Browser, the Tor network, and the many tools that rely on Tor strong, safe, and running smoothly.</p>
<p><strong>Today, I'm here to share the exciting news that you helped us raise $107,672.20 for the Bug Smash Fund this year! </strong>Of that total, about 44% came in the form of 10 different cryptocurrencies. Thank you to everyone from the cryptocurrency community for supporting Tor.</p>
<p>What’s next: as we did in 2019 and 2020, we’ll tag bugs in GitLab with <a href="https://gitlab.torproject.org/groups/tpo/-/issues?scope=all&amp;state=opened&amp;label_name[]=BugSmashFund">BugSmashFund</a> so you can follow along with how we’re allocating the fund: to things like fixing bugs on our website, in core Tor, and BridgeDB; tracking censorship events in Iran, Venezuela; and improving Tor Browser’s build process. We’ll also make periodic updates here on the blog (you can see previous versions of this blog post by <a href="https://blog.torproject.org/search/node?keys=bug+smash+fund">searching “bug smash fund”</a>) and through the newsletter about our progress with these BugSmashFund tickets. </p>
<p>Thank you to everybody who made a contribution to the Bug Smash Fund during the month of August. This work is critical in helping us to provide safer tools for millions of people around the world exercising their human rights to privacy and freedom online.</p>

