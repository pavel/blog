title: Tor Browser 6.5a5-hardened is released
---
pub_date: 2016-12-02
---
author: gk
---
tags:

tor browser
tbb
tbb-6.5
---
categories:

applications
releases
---
_html_body:

<p>A new hardened Tor Browser release is available. It can be found in the <a href="https://dist.torproject.org/torbrowser/6.5a5-hardened/" rel="nofollow">6.5a5-hardened distribution directory</a> and on the <a href="https://www.torproject.org/projects/torbrowser.html.en#downloads-hardened" rel="nofollow">download page for hardened builds</a>.</p>

<p>This release features an <a href="https://blog.mozilla.org/security/2016/11/30/fixing-an-svg-animation-vulnerability/" rel="nofollow">important security update to Firefox</a> and contains, in addition to that, an update to NoScript (2.9.5.2) and a fix of our updater code so it can handle unix domain sockets.</p>

<p>The Firefox security flaw responsible for this urgent release is already actively exploited on Windows systems. Even though there is currently, to the best of our knowledge, no similar exploit for OS X or Linux users available the underlying bug affects those platforms as well. Thus <strong> we strongly recommend that all users apply the update to their Tor Browser immediately. A restart is required for it to take effect</strong>.</p>

<p>Tor Browser users who had set their security slider to "High" are believed to have been safe from this vulnerability.</p>

<p><strong>Note regarding updating:</strong> We still require the same update procedure as experienced during an update to 6.5a4-hardened: a dialog will be shown asking to either set `app.update.staging.enabled` or `extensions.torlauncher.control_port_use_ipc` and `extensions.torlauncher.socks_port_use_ipc` to `false` (and restart the browser in the latter case) before attempting to update. The fix for this problem is shipped with this release and we will be back to a normal update experience with the update to 6.5a6-hardened. We are sorry for this inconvenience.</p>

<p>Here is the full changelog since 6.5a5-hardened:</p>

<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 45.5.1esr</li>
<li>Update NoScript to 2.9.5.2</li>
<li><a href="https://trac.torproject.org/projects/tor/ticket/20691" rel="nofollow">Bug 20691</a>: Updater breaks if unix domain sockets are used</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-223831"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223831" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
    <a href="#comment-223831">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223831" class="permalink" rel="bookmark">For those who don&#039;t know how</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>For those who don't know how different Tor Browser-hardened from the stable release: <a href="https://lists.torproject.org/pipermail/tbb-dev/2016-June/000382.html" rel="nofollow">https://lists.torproject.org/pipermail/tbb-dev/2016-June/000382.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-223851"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223851" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
    <a href="#comment-223851">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223851" class="permalink" rel="bookmark">Was the previous hardened</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Was the previous hardened version vulnerable to the recent exploit? I understand it was a memory corruption vuln of some kind caused by the use of some clever JavaScript. I'm assuming it was probably a heap-based attack (will have to dig into the details), so SSP wouldn't apply if that's the case, but what about AddressSanitizer or other measures. Has anyone tested the previous hardened against a proof-of-concept exploit? It's interesting to see how much more secure hardened really is in real-world scenarios.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223903"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223903" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 03, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223851" class="permalink" rel="bookmark">Was the previous hardened</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223903">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223903" class="permalink" rel="bookmark">I also would like to know</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I also would like to know this. I remember seeing some of the Tor Browser folks saying "somebody should test it", but I don't know if anybody did that. It could be you! Let us know what you learn.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223980"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223980" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 04, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-223980">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223980" class="permalink" rel="bookmark">I don&#039;t subscribe to the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't subscribe to the mailing list, but I did run across a link to a post that had the exploit code in it [1]. I could tell the developer of it is obviously a lot smarter than I am, but I might just be able to glue the pieces together well enough to create a test harness of sorts.</p>
<p>I've since learned from an article [2] that it was a use-after-free vuln, so heap-based, and I believe HeapSanitizer does attempt to catch use-after-free conditions. </p>
<p>I can't make any promises, but I think I might play around with this and see if I come up with anything.</p>
<p>1. <a href="https://lists.torproject.org/pipermail/tor-talk/2016-November/042639.html" rel="nofollow">https://lists.torproject.org/pipermail/tor-talk/2016-November/042639.ht…</a><br />
2. <a href="http://arstechnica.com/security/2016/11/firefox-0day-used-against-tor-users-almost-identical-to-one-fbi-used-in-2013/" rel="nofollow">http://arstechnica.com/security/2016/11/firefox-0day-used-against-tor-u…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
