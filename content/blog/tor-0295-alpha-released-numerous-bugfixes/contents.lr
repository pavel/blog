title: Tor 0.2.9.5-alpha is released, with numerous bugfixes
---
pub_date: 2016-11-08
---
author: nickm
---
tags:

tor
alpha
release
0.2.9.5-alpha
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.9.5-alpha fixes numerous bugs discovered in the previous alpha version. We believe one or two probably remain, and we encourage everyone to test this release.<br />
You can download the source from the usual place on the website. Packages should be available over the next several days. Remember to check the signatures!<br />
Please note: This is an alpha release. You should only try this one if you are interested in tracking Tor development, testing new features, making sure that Tor still builds on unusual platforms, or generally trying to hunt down bugs. If you want a stable experience, please stick to the stable releases.<br />
Below are the changes since 0.2.9.4-alpha.</p>

<h2>Changes in version 0.2.9.5-alpha - 2016-11-08</h2>

<ul>
<li>Major bugfixes (client performance):
<ul>
<li>Clients now respond to new application stream requests immediately when they arrive, rather than waiting up to one second before starting to handle them. Fixes part of bug <a href="https://bugs.torproject.org/19969" rel="nofollow">19969</a>; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Major bugfixes (client reliability):
<ul>
<li>When Tor leaves standby because of a new application request, open circuits as needed to serve that request. Previously, we would potentially wait a very long time. Fixes part of bug <a href="https://bugs.torproject.org/19969" rel="nofollow">19969</a>; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
</ul>

<p> </p>

<ul>
<li>Major bugfixes (download scheduling):
<ul>
<li>When using an exponential backoff schedule, do not give up on downloading just because we have failed a bunch of times. Since each delay is longer than the last, retrying indefinitely won't hurt. Fixes bug <a href="https://bugs.torproject.org/20536" rel="nofollow">20536</a>; bugfix on 0.2.9.1-alpha.
  </li>
<li>If a consensus expires while we are waiting for certificates to download, stop waiting for certificates.
  </li>
<li>If we stop waiting for certificates less than a minute after we started downloading them, do not consider the certificate download failure a separate failure. Fixes bug <a href="https://bugs.torproject.org/20533" rel="nofollow">20533</a>; bugfix on 0.2.0.9-alpha.
  </li>
<li>Remove the maximum delay on exponential-backoff scheduling. Since we now allow an infinite number of failures (see ticket <a href="https://bugs.torproject.org/20536" rel="nofollow">20536</a>), we must now allow the time to grow longer on each failure. Fixes part of bug <a href="https://bugs.torproject.org/20534" rel="nofollow">20534</a>; bugfix on 0.2.9.1-alpha.
  </li>
<li>Make our initial download delays closer to those from 0.2.8. Fixes another part of bug <a href="https://bugs.torproject.org/20534" rel="nofollow">20534</a>; bugfix on 0.2.9.1-alpha.
  </li>
<li>When determining when to download a directory object, handle times after 2038 if the operating system supports them. (Someday this will be important!) Fixes bug <a href="https://bugs.torproject.org/20587" rel="nofollow">20587</a>; bugfix on 0.2.8.1-alpha.
  </li>
<li>When using exponential backoff in test networks, use a lower exponent, so the delays do not vary as much. This helps test networks bootstrap consistently. Fixes bug <a href="https://bugs.torproject.org/20597" rel="nofollow">20597</a>; bugfix on 20499.
  </li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the November 3 2016 Maxmind GeoLite2 Country database.
  </li>
</ul>
</li>
<li>Minor bugfixes (client directory scheduling):
<ul>
<li>Treat "relay too busy to answer request" as a failed request and a reason to back off on our retry frequency. This is safe now that exponential backoffs retry indefinitely, and avoids a bug where we would reset our download schedule erroneously. Fixes bug <a href="https://bugs.torproject.org/20593" rel="nofollow">20593</a>; bugfix on 0.2.9.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (client, logging):
<ul>
<li>Remove a BUG warning in circuit_pick_extend_handshake(). Instead, assume all nodes support EXTEND2. Use ntor whenever a key is available. Fixes bug <a href="https://bugs.torproject.org/20472" rel="nofollow">20472</a>; bugfix on 0.2.9.3-alpha.
  </li>
<li>On DNSPort, stop logging a BUG warning on a failed hostname lookup. Fixes bug <a href="https://bugs.torproject.org/19869" rel="nofollow">19869</a>; bugfix on 0.2.9.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (hidden services):
<ul>
<li>When configuring hidden services, check every hidden service directory's permissions. Previously, we only checked the last hidden service. Fixes bug <a href="https://bugs.torproject.org/20529" rel="nofollow">20529</a>; bugfix the work to fix 13942 in 0.2.6.2-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (portability):
<ul>
<li>Fix compilation with OpenSSL 1.1 and less commonly-used CPU architectures. Closes ticket <a href="https://bugs.torproject.org/20588" rel="nofollow">20588</a>.
  </li>
<li>Use ECDHE ciphers instead of ECDH in tortls tests. LibreSSL has removed the ECDH ciphers which caused the tests to fail on platforms which use it. Fixes bug <a href="https://bugs.torproject.org/20460" rel="nofollow">20460</a>; bugfix on 0.2.8.1-alpha.
  </li>
<li>Fix implicit conversion warnings under OpenSSL 1.1. Fixes bug <a href="https://bugs.torproject.org/20551" rel="nofollow">20551</a>; bugfix on 0.2.1.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (relay bootstrap):
<ul>
<li>Ensure relays don't make multiple connections during bootstrap. Fixes bug <a href="https://bugs.torproject.org/20591" rel="nofollow">20591</a>; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>Work around a memory leak in OpenSSL 1.1 when encoding public keys. Fixes bug <a href="https://bugs.torproject.org/20553" rel="nofollow">20553</a>; bugfix on 0.0.2pre8.
  </li>
<li>Avoid a small memory leak when informing worker threads about rotated onion keys. Fixes bug <a href="https://bugs.torproject.org/20401" rel="nofollow">20401</a>; bugfix on 0.2.6.3-alpha.
  </li>
<li>Do not try to parallelize workers more than 16x without the user explicitly configuring us to do so, even if we do detect more than 16 CPU cores. Fixes bug <a href="https://bugs.torproject.org/19968" rel="nofollow">19968</a>; bugfix on 0.2.3.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (single onion services):
<ul>
<li>Start correctly when creating a single onion service in a directory that did not previously exist. Fixes bug <a href="https://bugs.torproject.org/20484" rel="nofollow">20484</a>; bugfix on 0.2.9.3-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Avoid a unit test failure on systems with over 16 detectable CPU cores. Fixes bug <a href="https://bugs.torproject.org/19968" rel="nofollow">19968</a>; bugfix on 0.2.3.1-alpha.
  </li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Clarify that setting HiddenServiceNonAnonymousMode requires you to also set "SOCKSPort 0". Fixes bug <a href="https://bugs.torproject.org/20487" rel="nofollow">20487</a>; bugfix on 0.2.9.3-alpha.
  </li>
<li>Module-level documentation for several more modules. Closes tickets 19287 and 19290.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-217741"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-217741" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 08, 2016</p>
    </div>
    <a href="#comment-217741">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-217741" class="permalink" rel="bookmark">And the expert bundle is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>And the expert bundle is still in 0.2.8.7 :/ </p>
<p><a href="https://www.torproject.org/dist/torbrowser/6.0.5/tor-win32-0.2.8.7.zip" rel="nofollow">https://www.torproject.org/dist/torbrowser/6.0.5/tor-win32-0.2.8.7.zip</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-217757"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-217757" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 08, 2016</p>
    </div>
    <a href="#comment-217757">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-217757" class="permalink" rel="bookmark">Though it&#039;s a highly</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Though it's a highly technical matter, Tor community is doing good for society. Please keep it up.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-217760"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-217760" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 08, 2016</p>
    </div>
    <a href="#comment-217760">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-217760" class="permalink" rel="bookmark">When are you going to update</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When are you going to update the Tor Browser bundle alpha??? macOS Sierra users have been waiting forever for a fix for the broken window dragging in Firefox among many other things... <a href="https://trac.torproject.org/projects/tor/ticket/20204" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/20204</a></p>
<p>pls</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-217789"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-217789" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 08, 2016</p>
    </div>
    <a href="#comment-217789">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-217789" class="permalink" rel="bookmark">Thank you so much for all</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you so much for all your time and effort, everyone in this great project.<br />
Is <a href="https://panopticlick.eff.org/" rel="nofollow">https://panopticlick.eff.org/</a> accurate for Tor Browser Bundle, or just mainstream browsers?<br />
Specifically, how much privacy is lost by disabling all cookies, all javascript, images, two of the above, or all three of the above?<br />
Please keep up the good work and don't let the public opinion against all things private get you down.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-217939"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-217939" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 09, 2016</p>
    </div>
    <a href="#comment-217939">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-217939" class="permalink" rel="bookmark">Any official statement</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Any official statement concerning recent Europol and the Five Eyes capture of hidden services?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-217959"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-217959" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 09, 2016</p>
    </div>
    <a href="#comment-217959">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-217959" class="permalink" rel="bookmark">Im wondering how debugging</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Im wondering how debugging an alpha version works with Tor.<br />
Its all client based? (do I have to generate my own debugging framework?)<br />
Or,  do the network have capabilities to debug errors from alpha clients? If yes, are there "prefered debug (entry) nodes"?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-218138"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-218138" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 10, 2016</p>
    </div>
    <a href="#comment-218138">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-218138" class="permalink" rel="bookmark">Any chances to disable the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Any chances to disable the forcefully imposed elliptic curve cryptography in future releases and let advanced users choose how they want to secure their connections?<br />
According to a recent report, elliptic curves don't look trustworthy even for the professionals:<br />
<a href="https://www.schneier.com/blog/archives/2016/11/whistleblower_i.html" rel="nofollow">https://www.schneier.com/blog/archives/2016/11/whistleblower_i.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-218288"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-218288" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 12, 2016</p>
    </div>
    <a href="#comment-218288">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-218288" class="permalink" rel="bookmark">Hello,
I am using Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello,</p>
<p>I am using Tor 6.0.5</p>
<p>Starting this week ie about November 8 2016 onwards Tor the slowest I have known even if I reload or use a new circuit with a new isp number or close down and restart.  In other words just very slow no matter what!  Is Tor being interferred with to slow it down?</p>
<p>cheers</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-218364"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-218364" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 12, 2016</p>
    </div>
    <a href="#comment-218364">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-218364" class="permalink" rel="bookmark">When could we hope to get</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When could we hope to get updated Windows Expert Bundle?<br />
It's still v0.2.8.7</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-218877"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-218877" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 16, 2016</p>
    </div>
    <a href="#comment-218877">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-218877" class="permalink" rel="bookmark">Tor 0.2.9.5-alpha fixes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor 0.2.9.5-alpha fixes numerous bugs discovered in the previous alpha version. We believe one or two probably remain, and we encourage everyone to test this release.</p>
<p>You can download the source from the usual place on the website. Packages should be available over the next several days. Remember to check the signatures!</p>
<p>Please note: This is an alpha release. You should only try this one if you are interested in tracking Tor development, testing new features, making sure that Tor still builds on unusual platforms, or generally trying to hunt down bugs. If you want a stable experience, please stick to the stable releases.</p>
<p>Below are the changes since 0.2.9.4-alpha.<br />
thanksssssss</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-219082"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-219082" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 16, 2016</p>
    </div>
    <a href="#comment-219082">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-219082" class="permalink" rel="bookmark">Something wrong with Tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Something wrong with Tor network (~3 times slower):<br />
Your network connection speed appears to have changed. Resetting timeout to 60s after 18 timeouts and 1000 buildtimes.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-220561"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-220561" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 22, 2016</p>
    </div>
    <a href="#comment-220561">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-220561" class="permalink" rel="bookmark">how to dowload it ,not with</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>how to dowload it ,not with browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-220593"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-220593" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">November 22, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-220561" class="permalink" rel="bookmark">how to dowload it ,not with</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-220593">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-220593" class="permalink" rel="bookmark">That&#039;s not possible</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's not possible currently. You have to wait until 0.2.9 gets stable and built during the stable Tor Browser build.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
