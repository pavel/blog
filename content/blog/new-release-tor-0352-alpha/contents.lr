title: New Release: Tor 0.3.5.2-alpha
---
pub_date: 2018-09-21
---
author: nickm
---
tags: alpha release
---
categories: releases
---
summary: Tor 0.3.5.2-alpha fixes several bugs in 0.3.5.1-alpha, including one that made Tor think it had run out of sockets. Anybody running a relay or an onion service on 0.3.5.1-alpha should upgrade.
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.3.5.2-alpha from the usual place on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release very soon.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.3.5.2-alpha fixes several bugs in 0.3.5.1-alpha, including one that made Tor think it had run out of sockets. Anybody running a relay or an onion service on 0.3.5.1-alpha should upgrade.</p>
<h2>Changes in version 0.3.5.2-alpha - 2018-09-21</h2>
<ul>
<li>Major bugfixes (relay bandwidth statistics):
<ul>
<li>When we close relayed circuits, report the data in the circuit queues as being written in our relay bandwidth stats. This mitigates guard discovery and other attacks that close circuits for the explicit purpose of noticing this discrepancy in statistics. Fixes bug <a href="https://bugs.torproject.org/23512">23512</a>; bugfix on 0.0.8pre3.</li>
</ul>
</li>
<li>Major bugfixes (socket accounting):
<ul>
<li>In our socket accounting code, count a socket as closed even when it is closed indirectly by the TLS layer. Previously, we would count these sockets as still in use, and incorrectly believe that we had run out of sockets. Fixes bug <a href="https://bugs.torproject.org/27795">27795</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor bugfixes (32-bit OSX and iOS, timing):
<ul>
<li>Fix an integer overflow bug in our optimized 32-bit millisecond- difference algorithm for 32-bit Apple platforms. Previously, it would overflow when calculating the difference between two times more than 47 days apart. Fixes part of bug <a href="https://bugs.torproject.org/27139">27139</a>; bugfix on 0.3.4.1-alpha.</li>
<li>Improve the precision of our 32-bit millisecond difference algorithm for 32-bit Apple platforms. Fixes part of bug <a href="https://bugs.torproject.org/27139">27139</a>; bugfix on 0.3.4.1-alpha.</li>
<li>Relax the tolerance on the mainloop/update_time_jumps test when running on 32-bit Apple platforms. Fixes part of bug <a href="https://bugs.torproject.org/27139">27139</a>; bugfix on 0.3.4.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service v3):
<ul>
<li>Close all SOCKS request (for the same .onion) if the newly fetched descriptor is unusable. Before that, we would close only the first one leaving the other hanging and let to time out by themselves. Fixes bug <a href="https://bugs.torproject.org/27410">27410</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (memory leak):
<ul>
<li>Fix an unlikely memory leak when trying to read a private key from a ridiculously large file. Fixes bug <a href="https://bugs.torproject.org/27764">27764</a>; bugfix on 0.3.5.1-alpha. This is CID 1439488.</li>
</ul>
</li>
<li>Minor bugfixes (NSS):
<ul>
<li>Correctly detect failure to open a dummy TCP socket when stealing ownership of an fd from the NSS layer. Fixes bug <a href="https://bugs.torproject.org/27782">27782</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (rust):
<ul>
<li>protover_all_supported() would attempt to allocate up to 16GB on some inputs, leading to a potential memory DoS. Fixes bug <a href="https://bugs.torproject.org/27206">27206</a>; bugfix on 0.3.3.5-rc.</li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Revise the "conditionvar_timeout" test so that it succeeds even on heavily loaded systems where the test threads are not scheduled within 200 msec. Fixes bug <a href="https://bugs.torproject.org/27073">27073</a>; bugfix on 0.2.6.3-alpha.</li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Divide the routerlist.c and dirserv.c modules into smaller parts. Closes ticket <a href="https://bugs.torproject.org/27799">27799</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-277436"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277436" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 21, 2018</p>
    </div>
    <a href="#comment-277436">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277436" class="permalink" rel="bookmark">When we close relayed…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><em>When we close relayed circuits, report the data in the circuit queues as being written in our relay bandwidth stats. This mitigates guard discovery and other attacks that close circuits for the explicit purpose of noticing this discrepancy in statistics. Fixes bug 23512</em><br />
I think we need it fixed in 0.3.4 and other branches ASAP, especially if it is the attack which is already massively observed in the wild (I've just read info from this ticket).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-277437"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277437" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 21, 2018</p>
    </div>
    <a href="#comment-277437">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277437" class="permalink" rel="bookmark">there&#039;s a bug in noscript,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>there's a bug in noscript, which doesn't apply to regular use of noscript in firefox.<br />
whenever i launch tor, the standard settings for default category allows js, fetch and others.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-277445"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277445" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>an huji (not verified)</span> said:</p>
      <p class="date-time">September 22, 2018</p>
    </div>
    <a href="#comment-277445">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277445" class="permalink" rel="bookmark">thankyou</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thankyou</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-277471"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277471" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>xyz-0 (not verified)</span> said:</p>
      <p class="date-time">September 22, 2018</p>
    </div>
    <a href="#comment-277471">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277471" class="permalink" rel="bookmark">My Tor Browser is constantly…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>My Tor Browser is constantly updating It downloads updates, installs them and than after the restart it does the same again and again and again.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-277716"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-277716" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">September 29, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-277471" class="permalink" rel="bookmark">My Tor Browser is constantly…</a> by <span>xyz-0 (not verified)</span></p>
    <a href="#comment-277716">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-277716" class="permalink" rel="bookmark">Which Tor Browser version do…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Which Tor Browser version do you have? On which platform does this happen?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-278309"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-278309" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Greg (not verified)</span> said:</p>
      <p class="date-time">October 28, 2018</p>
    </div>
    <a href="#comment-278309">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-278309" class="permalink" rel="bookmark">All was great the Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>All was great the Tor project was real since it had not forgotten the XP users though the browser started to warn me that my system was öld". its XP Pro sp 3. The browser fails now and the new updates ver isnt for XP. The dos ver is ok it loads then hangs in dos with the message now ''created a circuit''...... and nothing more the window just hangs. No browser no Duck GoGo.Cant find the manual for the dos install anywhere so her I am any help thnaks</p>
</div>
  </div>
</article>
<!-- Comment END -->
