title: Updates for old Tor stable release series: 0.2.4.28, 0.2.5.13, 0.2.6.11, 0.2.7.7, 0.2.8.13
---
pub_date: 2017-03-03
---
author: nickm
---
tags:

tor
stable
release
---
categories:

network
releases
---
_html_body:

<p>Hi!  We've just tagged and uploaded new versions for the older 0.2.4 through 0.2.8 release series, to backport important patches and extend the useful life of these versions.</p>

<p>If you have the option, we'd recommend that you run the latest stable release instead of these.  They are mainly of interest to distribution maintainers who for whatever reason want to track older release series of Tor.</p>

<p>You can, as usual, find the source at <a href="https://dist.torproject.org/" rel="nofollow">https://dist.torproject.org/</a>.  For a list of the backported changes in each release, see one of the nice handcrafted links below:</p>

<ul>
<li><a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?id=16bbbe82e42d02e2a251535de576c77d60e2a4bb" rel="nofollow">Changes in 0.2.4.28</a>
 </li>
<li><a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?id=45245fe29e2e5a0e36c6e4a340d44c6d98cbba1d" rel="nofollow">Changes in 0.2.5.13</a>
 </li>
<li><a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?id=c0c68547ead7e1884c98adf4110a155c88b1f5b5" rel="nofollow">Changes in 0.2.6.11</a>
 </li>
<li><a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?id=da819deb56009e91517f793c789308ec26468791" rel="nofollow">Changes in 0.2.7.7</a>
 </li>
<li><a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?id=4e5df7355514377338c3b284950daedd7091edbd" rel="nofollow">Changes in 0.2.8.13</a>
</li>
</ul>

<p>Please note that these releases are larger than we expect most future old-stable releases to be, because until recently we didn't have an actual policy of which releases should receive backports and support.  You can learn more about our plans for "regular" and "long-term support" releases of Tor <a href="https://trac.torproject.org/projects/tor/wiki/org/teams/NetworkTeam/CoreTorReleases" rel="nofollow">on the wiki</a>.</p>

---
_comments:

<a id="comment-239029"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-239029" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">March 03, 2017</p>
    </div>
    <a href="#comment-239029">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-239029" class="permalink" rel="bookmark">Thanks Nick!
For people</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks Nick!</p>
<p>For people reading this post: we did the work of putting out oldstable releases, but we have not yet done the work of getting any of these releases into stable distros -- Debian, Ubuntu, Gentoo, Arch, Fedora, etc.</p>
<p>So if you run one of those distros, and it's shipping something earlier than Tor 0.2.9.x, please consider launching whatever the process is for getting them to update.</p>
<p>For context, I found this site potentially interesting:<br />
<a href="https://repology.org/metapackage/tor/versions" rel="nofollow">https://repology.org/metapackage/tor/versions</a></p>
<p>Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-239666"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-239666" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 04, 2017</p>
    </div>
    <a href="#comment-239666">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-239666" class="permalink" rel="bookmark">TorProject.org only gets a C</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TorProject.org only gets a C rating in the SecurityHeaders test,</p>
<p><a href="https://securityheaders.io/?q=torproject.org&amp;followRedirects=on" rel="nofollow">https://securityheaders.io/?q=torproject.org&amp;followRedirects=on</a></p>
<p>It needs Content-Security-Policy, Public-Key-Pins and Referrer-Policy.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-240492"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-240492" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 06, 2017</p>
    </div>
    <a href="#comment-240492">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-240492" class="permalink" rel="bookmark">&gt; for whatever reason want</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; for whatever reason want to track older release series of Tor.<br />
This is not acceptable. What concrete reasons do they have to reduce privacy/anonymity of the Tor network?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-240494"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-240494" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 06, 2017</p>
    </div>
    <a href="#comment-240494">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-240494" class="permalink" rel="bookmark">Tor is not an usual program,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor is not an usual program, it is a client/peer for Tor network. So when NG onion services will become available, all Tor software must support them.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-241086"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-241086" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 07, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-240494" class="permalink" rel="bookmark">Tor is not an usual program,</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-241086">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-241086" class="permalink" rel="bookmark">More on that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>More on that here:</p>
<p><a href="https://lists.torproject.org/pipermail/tor-talk/2017-February/042929.html" rel="nofollow">https://lists.torproject.org/pipermail/tor-talk/2017-February/042929.ht…</a></p>
<p><a href="https://lists.torproject.org/pipermail/tor-dev/2016-December/011725.html" rel="nofollow">https://lists.torproject.org/pipermail/tor-dev/2016-December/011725.html</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-240645"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-240645" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 06, 2017</p>
    </div>
    <a href="#comment-240645">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-240645" class="permalink" rel="bookmark">JFYI, I&#039;ve split tor package</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>JFYI, I've split tor package on repology into</p>
<p><a href="https://repology.org/metapackage/tor/versions" rel="nofollow">https://repology.org/metapackage/tor/versions</a><br />
<a href="https://repology.org/metapackage/tor-unstable/versions" rel="nofollow">https://repology.org/metapackage/tor-unstable/versions</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-241200"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-241200" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 07, 2017</p>
    </div>
    <a href="#comment-241200">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-241200" class="permalink" rel="bookmark">Should Tor the daemon be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Should Tor the daemon be compiled with Selfrando by default?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-241439"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-241439" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">March 08, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-241200" class="permalink" rel="bookmark">Should Tor the daemon be</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-241439">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-241439" class="permalink" rel="bookmark">We have a ticket for that to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We have a ticket for that to get it going <a href="https://trac.torproject.org/projects/tor/ticket/19722" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/19722</a>. One major show stopper currently is licensing incompatibilities: tor uses a BSD license while selfrando is available under AGPL.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-253706"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-253706" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 30, 2017</p>
    </div>
    <a href="#comment-253706">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-253706" class="permalink" rel="bookmark">How do I manually replace an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How do I manually replace an older version for a newer on Windows, anyone?</p>
</div>
  </div>
</article>
<!-- Comment END -->
