title: New Release: Tails 5.0
---
pub_date: 2022-05-03
---
author: tails
---
categories:

partners
releases
---
summary:
Tails 5.0, the first version of Tails based on Debian 11 (Bullseye), is out. It brings new versions of a lot of the software included in Tails and new OpenPGP tools.
---
body:

We are especially proud to present you Tails 5.0, the first version of Tails based on Debian 11 (Bullseye). It brings new versions of a lot of the software included in Tails and new OpenPGP tools.

# New features

## Kleopatra

We added [_Kleopatra_](https://tails.boum.org/doc/encryption_and_privacy/kleopatra/) to replace the _OpenPGP Applet_ and the _Password and Keys_ utility, also known as _Seahorse_.

The _OpenPGP Applet_ was not actively developped anymore and was complicated for us to keep in Tails. The _Password and Keys_ utility was also poorly maintained and Tails users suffered from too many of its issues until now, like [#17183](https://gitlab.tails.boum.org/tails/tails/-/issues/17183).

_Kleopatra_ provides equivalent features in a single tool and is more actively developed.

# Changes and updates

*   The Additional Software feature of the Persistent Storage is enabled by default to make it faster and more robust to configure your first additional software package.
    
*   You can now use the Activities overview to access your windows and applications. To access the Activities overview, you can either:
    
    *   Click on the **Activities** button.
    *   Throw your mouse pointer to the top-left hot corner.
    *   Press the **Super** key on your keyboard.
    
    You can see your windows and applications in the overview. You can also start typing to search your applications, files, and folders.
    

## Included software

Most included software has been upgraded in Debian 11, for example:

*   Update _Tor Browser_ to [11.0.11](https://blog.torproject.org/new-release-tor-browser-11011).
    
*   Update _GNOME_ from 3.30 to 3.38, with lots of small improvements to the desktop, the core _GNOME_ utilities, and the locking screen.
    
*   Update _MAT_ from 0.8 to 0.12, which adds support to clean metadata from SVG, WAV, EPUB, PPM, and Microsoft Office files.
    
*   Update _Audacity_ from 2.2.2 to 2.4.2.
    
*   Update _Disk Utility_ from 3.30 to 3.38.
    
*   Update _GIMP_ from 2.10.8 to 2.10.22.
    
*   Update _Inkscape_ from 0.92 to 1.0.
    
*   Update _LibreOffice_ from 6.1 to 7.0.
    

## Hardware support

*   The new support for driverless printing and scanning in Linux makes it easier to make recent printers and scanners work in Tails.

# Fixed problems

*   Fix unlocking _VeraCrypt_ volumes that have very long passphrases. ([\#17474](https://gitlab.tails.boum.org/tails/tails/-/issues/17474))

For more details, read our [changelog](https://gitlab.tails.boum.org/tails/tails/-/blob/master/debian/changelog).

# Known issues

*   _Additional Software_ sometimes doesn't work when restarting for the first time right after creating a   Persistent Storage. ([\#18839](https://gitlab.tails.boum.org/tails/tails/-/issues/18839))
    
    To solve this, install the same additional software package again after restarting with the Persistent Storage for the first time.
    
*   _Thunderbird_ displays a popup to choose an application when opening links. ([\#18913](https://gitlab.tails.boum.org/tails/tails/-/issues/18913))
    
*   _Tails Installer_ sometimes fails to clone. ([\#18844](https://gitlab.tails.boum.org/tails/tails/-/issues/18844))
    

See the list of [long-standing issues](https://tails.boum.org/support/known_issues/).

# Get Tails 5.0

## To upgrade your Tails USB stick and keep your persistent storage

*   Automatic upgrades are not available to 5.0.
    
    All users have to do a [manual upgrade](https://tails.boum.org/doc/upgrade/#manual).
    

## To install Tails on a new USB stick

Follow our installation instructions:

*   [Install from Windows](https://tails.boum.org/install/windows/)
*   [Install from macOS](https://tails.boum.org/install/mac/)
*   [Install from Linux](https://tails.boum.org/install/linux/)
*   [Install from Debian or Ubuntu using the command line and GnuPG](https://tails.boum.org/install/expert/)

The Persistent Storage on the USB stick will be lost if you install instead of upgrading.

## To download only

If you don't need installation or upgrade instructions, you can download Tails 5.0 directly:

*   [For USB sticks (USB image)](https://tails.boum.org/install/download/)
*   [For DVDs and virtual machines (ISO image)](https://tails.boum.org/install/download-iso/)

# What's coming up?

Tails 5.1 is [scheduled](https://tails.boum.org/contribute/calendar/) for May 31.

Have a look at our [roadmap](https://tails.boum.org/contribute/roadmap) to see where we are heading to.

# Support and feedback

For support and feedback, visit the [Support section](https://tails.boum.org/support/) on the Tails website.
