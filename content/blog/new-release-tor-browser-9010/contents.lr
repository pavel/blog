title: New Release: Tor Browser 9.0.10
---
pub_date: 2020-05-04
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-9.0
---
categories: applications
---
_html_body:

<p>Tor Browser 9.0.10 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/9.0.10/">distribution directory</a>.</p>
<p>This release updates Firefox to 68.8.0esr, NoScript to 11.0.25, and OpenSSL to 1.1.1g.</p>
<p>Also, this release features important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020-17/">security updates</a> to Firefox.</p>
<p>The full <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-9.0">changelog</a> since Tor Browser 9.0.9 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update Firefox to 68.8.0esr</li>
<li>Bump NoScript to 11.0.25</li>
</ul>
</li>
<li> Windows + OS X + Linux
<ul>
<li><a href="https://bugs.torproject.org/34017">Bug 34017:</a> Bump openssl version to 1.1.1g</li>
</ul>
</li>
</ul>

