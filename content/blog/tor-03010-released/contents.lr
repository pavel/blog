title: Tor 0.3.0.10 is released
---
pub_date: 2017-08-02
---
author: nickm
---
tags: stable release
---
categories: releases
---
_html_body:

<p>Source code for a new Tor release (0.3.0.10) is now available on the website; packages should be available over the next several days. The Tor Browser team tells me they will have a release out next week.</p>
<p>Reminder: Tor 0.2.4, 0.2.6, and 0.2.7 are no longer supported, as of 1 August of this year.  If you need a release with long-term support, 0.2.9 is what we recommend: we plan to support it until at least 1 Jan 2020.<br />
 </p>
<p>Tor 0.3.0.10 backports a collection of small-to-medium bugfixes from the current Tor alpha series. OpenBSD users and TPROXY users should upgrade; others are probably okay sticking with 0.3.0.9.</p>
<h2>Changes in version 0.3.0.10 - 2017-08-02</h2>
<ul>
<li>Major features (build system, continuous integration, backport from 0.3.1.5-alpha):
<ul>
<li>Tor's repository now includes a Travis Continuous Integration (CI) configuration file (.travis.yml). This is meant to help new developers and contributors who fork Tor to a Github repository be better able to test their changes, and understand what we expect to pass. To use this new build feature, you must fork Tor to your Github account, then go into the "Integrations" menu in the repository settings for your fork and enable Travis, then push your changes. Closes ticket <a href="https://bugs.torproject.org/22636">22636</a>.</li>
</ul>
</li>
<li>Major bugfixes (linux TPROXY support, backport from 0.3.1.1-alpha):
<ul>
<li>Fix a typo that had prevented TPROXY-based transparent proxying from working under Linux. Fixes bug <a href="https://bugs.torproject.org/18100">18100</a>; bugfix on 0.2.6.3-alpha. Patch from "d4fq0fQAgoJ".</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major bugfixes (openbsd, denial-of-service, backport from 0.3.1.5-alpha):
<ul>
<li>Avoid an assertion failure bug affecting our implementation of inet_pton(AF_INET6) on certain OpenBSD systems whose strtol() handling of "0xfoo" differs from what we had expected. Fixes bug <a href="https://bugs.torproject.org/22789">22789</a>; bugfix on 0.2.3.8-alpha. Also tracked as TROVE-2017-007.</li>
</ul>
</li>
<li>Minor features (backport from 0.3.1.5-alpha):
<ul>
<li>Update geoip and geoip6 to the July 4 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
<li>Minor bugfixes (bandwidth accounting, backport from 0.3.1.2-alpha):
<ul>
<li>Roll over monthly accounting at the configured hour and minute, rather than always at 00:00. Fixes bug <a href="https://bugs.torproject.org/22245">22245</a>; bugfix on 0.0.9rc1. Found by Andrey Karpov with PVS-Studio.</li>
</ul>
</li>
<li>Minor bugfixes (compilation warnings, backport from 0.3.1.5-alpha):
<ul>
<li>Suppress -Wdouble-promotion warnings with clang 4.0. Fixes bug <a href="https://bugs.torproject.org/22915">22915</a>; bugfix on 0.2.8.1-alpha.</li>
<li>Fix warnings when building with libscrypt and openssl scrypt support on Clang. Fixes bug <a href="https://bugs.torproject.org/22916">22916</a>; bugfix on 0.2.7.2-alpha.</li>
<li>When building with certain versions of the mingw C header files, avoid float-conversion warnings when calling the C functions isfinite(), isnan(), and signbit(). Fixes bug <a href="https://bugs.torproject.org/22801">22801</a>; bugfix on 0.2.8.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, mingw, backport from 0.3.1.1-alpha):
<ul>
<li>Backport a fix for an "unused variable" warning that appeared in some versions of mingw. Fixes bug <a href="https://bugs.torproject.org/22838">22838</a>; bugfix on 0.2.8.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (coverity build support, backport from 0.3.1.5-alpha):
<ul>
<li>Avoid Coverity build warnings related to our BUG() macro. By default, Coverity treats BUG() as the Linux kernel does: an instant abort(). We need to override that so our BUG() macro doesn't prevent Coverity from analyzing functions that use it. Fixes bug <a href="https://bugs.torproject.org/23030">23030</a>; bugfix on 0.2.9.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (directory authority, backport from 0.3.1.1-alpha):
<ul>
<li>When rejecting a router descriptor for running an obsolete version of Tor without ntor support, warn about the obsolete tor version, not the missing ntor key. Fixes bug <a href="https://bugs.torproject.org/20270">20270</a>; bugfix on 0.2.9.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (linux seccomp2 sandbox, backport from 0.3.1.5-alpha):
<ul>
<li>Avoid a sandbox failure when trying to re-bind to a socket and mark it as IPv6-only. Fixes bug <a href="https://bugs.torproject.org/20247">20247</a>; bugfix on 0.2.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (unit tests, backport from 0.3.1.5-alpha):
<ul>
<li>Fix a memory leak in the link-handshake/certs_ok_ed25519 test. Fixes bug <a href="https://bugs.torproject.org/22803">22803</a>; bugfix on 0.3.0.1-alpha.</li>
</ul>
</li>
</ul>

