title: Arti 1.1.1 is released: Groundwork for onion services
---
author: nickm
---
pub_date: 2023-02-01
---
categories: announcements
---
summary:

Arti 1.1.1 is released and ready for download.
---
body:

Arti is our ongoing project to create an next-generation Tor client
in Rust.  In late November, we released [Arti 1.1.0].  Now we're announcing
the next release in its series, Arti 1.1.1.

Since our last release, our primary focus has been preparation for
onion service support in Arti.  To that end, we've broken the work
down into a [bunch of tickets], designed our [major][tor-hsclient]
[internal][tor-hsservice] [APIs][tor-hscrypto], and started to work on
the lower-level features.  There's nothing you can use here yet, but
the work is coming!

We hope to deliver experimental support for onion service
clients–then, support for running services later in the year.
Finally, we will build the security features allow onion services and
clients to be at least as safe as in C tor.

We've also made a number of other changes, including improved logging
security, new convenience APIs, and better handling of some SOCKS
requests. You can find a more complete list of changes in our
[CHANGELOG].

For more information on using Arti, see our top-level [README],
and the docmentation for the [`arti` binary].

Thanks to everyone who has contributed to this release, including
Alexander Færøy, coral, Dimitris Apostolou, Emil Engler, Jim Newsome,
Michael van Straten, Neel Chauhan, and Trinity Pointard.

Also, our deep thanks to [Zcash Community Grants] for funding the
development of Arti 1.1.1!



[Arti 1.1.0]: https://blog.torproject.org/arti_110_released/
[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md#arti-111-1-february-2022
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[tor-hsclient]: https://tpo.pages.torproject.net/core/doc/rust/tor_hsclient/index.html
[tor-hsservice]: https://tpo.pages.torproject.net/core/doc/rust/tor_hsservice/index.html
[tor-hscrypto]: https://tpo.pages.torproject.net/core/doc/rust/tor_hscrypto/index.html
[bunch of tickets]: https://gitlab.torproject.org/tpo/core/arti/-/milestones/11#tab-issues