title: Improvements on Hidden Service Performance -- or not?
---
pub_date: 2009-01-15
---
author: karsten
---
tags:

hidden services
performance
---
categories:

network
onion services
---
_html_body:

<p>During the past eight months we have been trying pretty hard to improve hidden service performance. This work was part of the project to <a href="https://www.torproject.org/projects/hidserv.html" rel="nofollow">Speed Up Tor Hidden Services</a>, generously funded by the NLnet Foundation. As of today, we know that we have succeeded in our attempts -- well, or not?</p>

<p>We started in June 2008 by measuring how long it takes until hidden services are registered in the network, how long it takes for clients to connect to them, and similar metrics. An <a href="http://freehaven.net/~karsten/hidserv/perfanalysis-2008-06-15.pdf" rel="nofollow">analysis</a> of these data revealed what problems could be responsible for the experienced delays: First of all, we found in total 10 bugs, some of them delaying hidden services significantly. These bugs are now fixed in the 0.2.1.x series (and the major bugs also in the 0.2.0.x series). Second, we identified certain substeps as the bottlenecks of the hidden service protocol, all of them related to building circuits on demand. We proposed a few design changes to overcome these bottlenecks by reducing circuit-build timeouts or by building circuits in parallel. These design changes have been <a href="http://freehaven.net/~karsten/hidserv/discussion-2008-07-15.pdf" rel="nofollow">discussed</a>, <a href="http://freehaven.net/~karsten/hidserv/design-2008-08-15.pdf" rel="nofollow">evaluated</a>, <a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/155-four-hidden-service-improvements.txt" rel="nofollow">specified</a>, and finally implemented in the 0.2.1.x series.</p>

<p>As of today, we know that we were successful in our attempts -- well, at least in parts. In December 2008, we performed a second set of measurements with a Tor version that had all bugfixes and design changes. Today we finished the analysis of these new data by <a href="http://freehaven.net/~karsten/hidserv/comparison-2009-01-15.pdf" rel="nofollow">comparing</a> them with the June data. We found that the time until hidden services are advertised in the network halved from 2:12 minutes to 58 seconds in the mean. This improvement is by far better than we had expected. With this improvement we might even think about reducing an internal descriptor stabilization time from 30 seconds to a lower value, which would further reduce service publication time. However, we also found that the time that clients need to wait while connecting to hidden services remained unchanged at 56 seconds in the mean. Even though our design changes were effective, the <a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/114-distributed-storage.txt" rel="nofollow">distributed storage</a> for hidden service descriptors has counter-balanced all gains achieved here. This deteriorating effect was not expected when we started. In the future, we should therefore focus on speeding up downloads from the distributed hidden service directory, for example by parallelizing requests.</p>

<p>All in all, these improvements are an important step in making hidden services faster. Now we know pretty well what the main problem of hidden services is: on-demand circuit building. Of course, this problem is not specific to hidden services, but a general problem of Tor. Fortunately, hidden services would benefit from all improvements that are made to circuit building in Tor. In the future we are focusing on improvements to circuit building in general.</p>

---
_comments:

<a id="comment-537"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-537" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 17, 2009</p>
    </div>
    <a href="#comment-537">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-537" class="permalink" rel="bookmark">portable</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I use portable mozilla firefox on a flash usb drive. How nice to be able to have a portable TOR loaded on the same flash drive, so to be able to use it with any pc !<br />
Any chance?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-539"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-539" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 17, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-537" class="permalink" rel="bookmark">portable</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-539">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-539" class="permalink" rel="bookmark">You might like the Tor Browser Bundle</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://www.torproject.org/torbrowser/" rel="nofollow">https://www.torproject.org/torbrowser/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-7255"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-7255" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 18, 2010</p>
    </div>
    <a href="#comment-7255">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-7255" class="permalink" rel="bookmark">Any chance to use TOR on PC?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Any chance to use TOR on PC?</p>
</div>
  </div>
</article>
<!-- Comment END -->
