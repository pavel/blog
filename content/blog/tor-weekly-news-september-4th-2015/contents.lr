title: Tor Weekly News — September 4th, 2015
---
pub_date: 2015-09-04
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the thirty-fourth issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>Tor Browser 5.0.2 and 5.5a2 are out</h1>

<p>The Tor Browser team announced new stable and alpha releases of the privacy-preserving web browser. <a href="https://blog.torproject.org/blog/tor-browser-502-released" rel="nofollow">Version 5.0.2</a> fixes a bug that was causing the browser’s launcher icons in the Ubuntu Unity and GNOME desktops to be duplicated, and includes a newer version of the NoScript add-on. <a href="https://blog.torproject.org/blog/tor-browser-55a2-released" rel="nofollow">Version 5.5a2</a> incorporates these updates along with another small crash bug fix from the stable series.</p>

<p>Both new releases include important security updates to their respective Firefox versions, so please ensure you upgrade as soon as possible. If you are already running a recent Tor Browser, it has probably updated itself already; if not, head to the <a href="https://www.torproject.org/projects/torbrowser.html" rel="nofollow">project page</a> to download your copy now.</p>

<h1>Final reports from two Summer of Privacy students</h1>

<p>Two of the developers participating in Tor’s first-ever <a href="https://trac.torproject.org/projects/tor/wiki/org/TorSoP" rel="nofollow">Summer of Privacy coding season</a>, <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009375.html" rel="nofollow">Jesse Victors</a> and <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009376.html" rel="nofollow">Donncha O’Cearbhaill</a>, submitted their final progress reports after months of intensive development.</p>

<p>Jesse’s DNS-like naming system for onion services is <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009221.html" rel="nofollow">already in a testable state</a>. “All of the infrastructure for OnioNS is in place”, and while a few protocols are still to be finished, “the client-side and HS-side software is pretty reliable and stable at this point”, with support for Debian, Ubuntu, Mint, and Fedora. Development will continue into the future, and “once the OnioNS software is fully ready, no modifications to Tor should be necessary to merge OnioNS into the Tor network”.</p>

<p>Donncha’s project, the onion service load-balancing manager OnionBalance, has also seen one <a href="https://lists.torproject.org/pipermail/tor-talk/2015-July/038312.html" rel="nofollow">testing release</a>, and the next steps in development are to package the software for Debian, clarify the documentation, and implement “smartcard / HSM support master service key storage and signing”. “I’ll continue developing OnionBalance so that if possible, it can facilitate some form of load balancing and redundancy with next-gen hidden services”.</p>

<p>Congratulations to Jesse and Donncha on getting their innovative projects to this stage, and thanks to the mentors and coordinators who have made the Summer of Privacy a success. The southern-hemisphere development timetable is still ongoing, however, so stay tuned for updates from Israel and Cristóbal Leiva on their TSoP projects.</p>

<h1>Should cloud-based Tor relays be rejected?</h1>

<p>Observing that “we sometimes see attacks from relays that are hosted on cloud platforms”, Philipp Winter <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009389.html" rel="nofollow">investigated</a> the actual benefit to the Tor network that these relays provide. He found that in an average consensus from July 2015, “cloud-hosted relays contributed only around 0.8% of bandwidth” (with the caveat that “this is just a lower bound”). Rejecting such relays from the consensus might force attackers to jump through more hoops, but would mean “obtaining the netblocks that are periodically published by all three (and perhaps more) cloud providers”.</p>

<p>Tim Wilson-Brown (teor) <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009390.html" rel="nofollow">wondered</a> about the effect this might have on Tor developers and researchers who would like to use cloud-based relays, while nusenu <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009391.html" rel="nofollow">requested</a> that any rejection be publicly documented “so volunteers don’t waste their time and money setting up blacklisted relays”.</p>

<h1>Miscellaneous news</h1>

<p>Karsten Loesing <a href="https://lists.torproject.org/pipermail/tor-dev/2015-August/009385.html" rel="nofollow">announced</a> version 2.6 of Onionoo, the Tor network data observatory. This release adds two new relay family-related fields to details documents that, together with the “effective_family” field introduced in version 2.4, replace the older “family” field, which is now deprecated. These new fields support different family-mapping use-cases that may be required by Tor network tools such as Atlas, Globe, and Roster. “The current ‘family’ field will stay available until Atlas and Globe are updated. If I should also wait for other clients to be updated, please let me know.”</p>

<p>After several television appearances over the past few years, Tor made its literary debut last month in the fourth installment of the late Stieg Larsson’s Millennium series. A warm Tor community welcome to Lisbeth Salander — though a subscription to Tor Weekly News might clear up some of her <a href="http://penguinrandomhouse.ca/content/penguin-random-house-canada/blog/excerpt-girl-spiders-web" rel="nofollow">misconceptions</a>…</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

