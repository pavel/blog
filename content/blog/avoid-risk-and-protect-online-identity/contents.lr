title: Avoid risk and protect online identity
---
pub_date: 2012-03-14
---
author: Runa
---
_html_body:

<p>A number of good blog posts have been written about how you can avoid risk, protect your online identity, blog safely and so on. We decided to collect links to some of these posts so that you can easily find the information most relevant to you.</p>

<p><a href="https://www.eff.org/wp/blog-safely" rel="nofollow">How to Blog Safely (About Work or Anything Else)</a> is a guide written by the <a href="https://www.eff.org/" rel="nofollow">EFF</a>. The guide talks about how you can use Tor to blog anonymously, and offers a few simple precautions to help you maintain control of your privacy.</p>

<p><a href="http://www.problogger.net/archives/2006/02/07/blog-stalkers-personal-safety-for-bloggers/" rel="nofollow">Blog Stalkers – Personal Safety for Bloggers</a> is a post written by <a href="http://www.problogger.net/archives/author/darren/" rel="nofollow">Darren Rowse</a>. The blog post tells the story of how Rowse had to deal with a stalker for some time, and lists some things to consider regarding the information you reveal online about yourself and your family.</p>

<p><a href="http://advocacy.globalvoicesonline.org/projects/guide/" rel="nofollow">Anonymous Blogging with WordPress &amp; Tor</a> is a guide written by <a href="http://globalvoicesonline.org/author/ezuckerman/" rel="nofollow">Ethan Zuckerman</a> for <a href="http://advocacy.globalvoicesonline.org/" rel="nofollow">Global Voices Advocacy</a>. The guide walks you through how you can use Tor, WordPress and various free email accounts to blog anonymously.</p>

<p><a href="https://www.eff.org/wp/defending-privacy-us-border-guide-travelers-carrying-digital-devices" rel="nofollow">Defending Privacy at the U.S. Border: A Guide for Travelers Carrying Digital Devices</a> is a guide by the <a href="https://www.eff.org/" rel="nofollow">EFF</a>. The guide explains why your devices can be searched at the border, how the U.S. Government searches devices, and gives advice on how to protect your data.</p>

<p><a href="//www.eff.org/https-everywhere" rel="nofollow">HTTPS Everywhere</a> is a Firefox and Chrome extension that encrypts your communications with many major websites, making your browsing more secure. HTTPS Everywhere is a produced as a collaboration between The Tor Project and the <a href="https://www.eff.org/" rel="nofollow">EFF</a>.</p>

<p><a href="http://www.microsoft.com/security/family-safety/blogging.aspx" rel="nofollow">Safety tips on blogging</a> from <a href="http://www.microsoft.com" rel="nofollow">Microsoft</a>. This short guide lists some basic guidelines for bloggers, and also gives some advice to parents about how they can talk to their kids about the Internet and blogging.</p>

---
_comments:

<a id="comment-14591"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14591" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 14, 2012</p>
    </div>
    <a href="#comment-14591">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14591" class="permalink" rel="bookmark">It wouldn&#039;t surprise me if</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It wouldn't surprise me if major publications are in fact providing people's IP addresses who post to their blogs to national law-enforcement (so to speak) without requiring a warrant. There is an active effort to create a totalitarian state in the US by the oligarchical elite. Orwell would be proud.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-14695"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14695" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 20, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-14591" class="permalink" rel="bookmark">It wouldn&#039;t surprise me if</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-14695">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14695" class="permalink" rel="bookmark">&quot;Orwell would be</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Orwell would be proud."</p>
<p>Uh....I don't think you actually meant to say that.</p>
<p>George Orwell would most certainly not be _proud_ at what you describe, but one could argue that he would be _vindicated_ by it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-14612"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14612" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 16, 2012</p>
    </div>
    <a href="#comment-14612">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14612" class="permalink" rel="bookmark">Similar guide to defending</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Similar guide to defending your privacy at the Canadian border by the Britsh Columbia Civil Liberties Association: <a href="https://www.bccla.org/othercontent/Electronic-devices.pdf" rel="nofollow">https://www.bccla.org/othercontent/Electronic-devices.pdf</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14640"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14640" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 18, 2012</p>
    </div>
    <a href="#comment-14640">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14640" class="permalink" rel="bookmark">Using Tor in a profesionnal</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Using Tor in a profesionnal context :<br />
Do IT and Admin can see my surf ? I mean  in the same LAN<br />
They use also PCAnywhere : same question ?<br />
thanx4all</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-14662"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14662" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 18, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-14640" class="permalink" rel="bookmark">Using Tor in a profesionnal</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-14662">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14662" class="permalink" rel="bookmark">Tor protects you at</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor protects you at different network level. Of course all traffic is encrypted anyway, as far you are browsing internet, but not using shares or f.e pcAnywhere sessions inside your LAN. In LAN Admin has more possibilities to watch and interact with your activities.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-14768"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14768" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 25, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-14662" class="permalink" rel="bookmark">Tor protects you at</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-14768">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14768" class="permalink" rel="bookmark">I am not an expert, but</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am not an expert, but wording this answer differently:<br />
browser&lt;&gt;Tor Unencrypted<br />
Tor,your PC&lt;&gt;LAN: encrypted<br />
LAN&lt;&gt;Internet (actually, the first Tor node): encrypted<br />
non-exit tor nodes&lt;&gt;non-exit tor nodes: encrypted<br />
exit node&lt;&gt; the website you see: Unencrypted<br />
_______<br />
https://www.wikipedia.org/wiki/PcAnywhere<br />
is a remote desktop, so I think the other PC (your admin) can see what you see on your display.<br />
Maybe you can block the PcAnywhere port or ports?<br />
1. PcAnywhere might try new ports when any fail. You would need to experiment. Or you might need to allow only a few ports, and I don't know how. I have only <b>read</b> related info: Maybe a socksifier, then maybe also a software firewall. I assume socksifier must be installed, but you cannot install software.<br />
2. Admin might have alert for when PcAnywhere fails. (I don't know if alert might be included feature in PcAnywhere?) Or admin might simply walk (on human legs and feet) to check why PcAnywhere seems blocked. :(</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-14891"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14891" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 05, 2012</p>
    </div>
    <a href="#comment-14891">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14891" class="permalink" rel="bookmark">The judge recommends to use</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The judge recommends to use TOR when the whistleblowers use <a href="https://www.ceic.gouv.qc.ca" rel="nofollow">https://www.ceic.gouv.qc.ca</a>.</p>
<p>In Canada, in the province of Quebec, the « Commission Charbonneau » inquires into the corruption in the public utilities and the influence of the Mafia in the governments, in the cities, etc.  </p>
<p>Judge France Charbonneau asks the citizens to use a Web form to give him the tips ones denouncing concrete examples of corruptions of civils servant and elected officials. In the policy of confidentiality of its Web site, it “strongly recommends” to use TOR.</p>
</div>
  </div>
</article>
<!-- Comment END -->
