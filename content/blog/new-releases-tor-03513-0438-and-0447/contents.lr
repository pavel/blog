title: New releases: Tor 0.3.5.13, 0.4.3.8, and 0.4.4.7
---
pub_date: 2021-02-03
---
author: nickm
---
tags: stable release
---
categories: releases
---
_html_body:

<p>We have a new stable release today. If you build Tor from source, you can download the source code for 0.4.4.7 on the <a href="https://www.torproject.org/download/tor/">download page</a>. Packages should be available within the next several weeks, with a new Tor Browser later this month.</p>
<p>We're also releasing updates for older stable release series. You can download 0.3.5.13 (<a href="&lt;br /&gt;&#10;https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.3.5.13">changelog</a>) and 0.4.3.8 (<a href="&lt;br /&gt;&#10;https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.4.3.8">changelog</a>) from <a href="https://dist.torproject.org">dist.torproject.org</a>. Note that the 0.4.3.x series will no longer be supported after 15 February.</p>
<p>Tor 0.4.4.7 backports numerous bugfixes from later releases, including one that made v3 onion services more susceptible to denial-of-service attacks, and a feature that makes some kinds of DoS attacks harder to perform.</p>
<h2>Changes in version 0.4.4.7 - 2021-02-03</h2>
<ul>
<li>Major bugfixes (onion service v3, backport from 0.4.5.3-rc):
<ul>
<li>Stop requiring a live consensus for v3 clients and services, and allow a "reasonably live" consensus instead. This allows v3 onion services to work even if the authorities fail to generate a consensus for more than 2 hours in a row. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40237">40237</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Major feature (exit, backport from 0.4.5.5-rc):
<ul>
<li>Re-entry into the network is now denied at the Exit level to all relays' ORPorts and authorities' ORPorts and DirPorts. This change should help mitgate a set of denial-of-service attacks. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/2667">2667</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor feature (build system, backport from 0.4.5.4-rc):
<ul>
<li>New "make lsp" command to generate the compile_commands.json file used by the ccls language server. The "bear" program is needed for this. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40227">40227</a>.</li>
</ul>
</li>
<li>Minor features (compilation, backport from 0.4.5.2-rc):
<ul>
<li>Disable deprecation warnings when building with OpenSSL 3.0.0 or later. There are a number of APIs newly deprecated in OpenSSL 3.0.0 that Tor still requires. (A later version of Tor will try to stop depending on these APIs.) Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40165">40165</a>.</li>
</ul>
</li>
<li>Minor features (crypto, backport from 0.4.5.3-rc):
<ul>
<li>Fix undefined behavior on our Keccak library. The bug only appeared on platforms with 32-byte CPU cache lines (e.g. armv5tel) and would result in wrong digests. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40210">40210</a>; bugfix on 0.2.8.1-alpha. Thanks to Bernhard Übelacker, Arnd Bergmann and weasel for diagnosing this.</li>
</ul>
</li>
<li>Minor bugfixes (compatibility, backport from 0.4.5.1-rc):
<ul>
<li>Strip '\r' characters when reading text files on Unix platforms. This should resolve an issue where a relay operator migrates a relay from Windows to Unix, but does not change the line ending of Tor's various state files to match the platform, and the CRLF line endings from Windows end up leaking into other files such as the extra-info document. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/33781">33781</a>; bugfix on 0.0.9pre5.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, backport from 0.4.5.3-rc):
<ul>
<li>Fix a compilation warning about unreachable fallthrough annotations when building with "--enable-all-bugs-are-fatal" on some compilers. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40241">40241</a>; bugfix on 0.3.5.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (SOCKS5, backport from 0.4.5.3-rc):
<ul>
<li>Handle partial SOCKS5 messages correctly. Previously, our code would send an incorrect error message if it got a SOCKS5 request that wasn't complete. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40190">40190</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing, backport from 0.4.5.2-alpha):
<ul>
<li>Fix the `config/parse_tcp_proxy_line` test so that it works correctly on systems where the DNS provider hijacks invalid queries. Fixes part of bug <a href="https://bugs.torproject.org/tpo/core/tor/40179">40179</a>; bugfix on 0.4.3.1-alpha.</li>
<li>Fix our Python reference-implementation for the v3 onion service handshake so that it works correctly with the version of hashlib provided by Python 3.9. Fixes part of bug <a href="https://bugs.torproject.org/tpo/core/tor/40179">40179</a>; bugfix on 0.3.1.6-rc.</li>
<li>Fix the `tortls/openssl/log_one_error` test to work with OpenSSL 3.0.0. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40170">40170</a>; bugfix on 0.2.8.1-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-291053"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291053" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 04, 2021</p>
    </div>
    <a href="#comment-291053">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291053" class="permalink" rel="bookmark">here&#039;s hoping there&#039;ll be no…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>here's hoping there'll be no more trouble with v3 onions</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291058"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291058" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>ET (not verified)</span> said:</p>
      <p class="date-time">February 04, 2021</p>
    </div>
    <a href="#comment-291058">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291058" class="permalink" rel="bookmark">When using ExcludeNodes and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When using ExcludeNodes and listing a country such as {us} WHY does the country {us} appear as one of the countires on the circuit when it was excluded ? </p>
<p>How is the circuit region selected by TOR. I notice a bounce among geographically close countries of a more constant apparances of 1 or 2 countries.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291066"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291066" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 05, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291058" class="permalink" rel="bookmark">When using ExcludeNodes and…</a> by <span>ET (not verified)</span></p>
    <a href="#comment-291066">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291066" class="permalink" rel="bookmark">Did you set :
StrictNodes 1…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Did you set :<br />
StrictNodes 1<br />
On your torrc ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291061"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291061" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 04, 2021</p>
    </div>
    <a href="#comment-291061">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291061" class="permalink" rel="bookmark">Thank you! Availability of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you! Availability of v3 onion services is vital for operators to <a href="https://blog.torproject.org/v2-deprecation-timeline" rel="nofollow">transition</a> from v2 and for <a href="https://blog.torproject.org/more-onions-end-of-campaign" rel="nofollow">#MoreOnionsPorfavor</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291107"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291107" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 09, 2021</p>
    </div>
    <a href="#comment-291107">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291107" class="permalink" rel="bookmark">Updated from 4.4.6 to 4.5.5…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Updated from 4.4.6 to 4.5.5-rc (instead of 4.4.7) with apt update.<br />
Is this the correct behavior?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291123"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291123" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 10, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291107" class="permalink" rel="bookmark">Updated from 4.4.6 to 4.5.5…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291123">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291123" class="permalink" rel="bookmark">I don&#039;t know if that was…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't know if that configuration was intentional, but it is how the repository at deb.torproject.org is configured at the time of this message.</p>
<p><a href="https://deb.torproject.org/torproject.org/dists/[YOUR_OS_CODENAME]/main/binary-[YOUR_ARCHITECTURE]/Packages" rel="nofollow">https://deb.torproject.org/torproject.org/dists/[YOUR_OS_CODENAME]/main…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291148"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291148" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 13, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291107" class="permalink" rel="bookmark">Updated from 4.4.6 to 4.5.5…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291148">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291148" class="permalink" rel="bookmark">Same here. I didn&#039;t expect…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Same here. I didn't expect this behavior in a stable release channel.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
