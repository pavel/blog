title: Lots of new Tor and Vidalia packages
---
pub_date: 2011-04-13
---
author: erinn
---
tags:

tor
vidalia
tor browser bundle
stable release
bug fixes
alpha release
---
categories:

applications
network
releases
---
_html_body:

<p>New <a href="http://blog.torproject.org/blog/vidalia-0212-released" rel="nofollow">Vidalia</a> and <a href="http://blog.torproject.org/blog/tor-02224-alpha-out" rel="nofollow">Tor</a> releases mean lots and lots of new packages. You can download most of them from the <a href="https://www.torproject.org/download/download" rel="nofollow">download page</a>.</p>

<p><strong>RPM users</strong>: we'll have all of the RPMs up within the next 24 hours. Everyone else, read on for Tor Browser Bundle changelogs and other packages.</p>

<p><strong>Bridge-by-Default Bundle</strong></p>

<ul>
<li><a href="http://www.torproject.org/dist/vidalia-bundles/vidalia-bridge-bundle-0.2.2.24-alpha-0.2.12.exe" rel="nofollow">Windows Bridge by Default Vidalia Bundle</a> (<a href="http://www.torproject.org/dist/vidalia-bundles/vidalia-bridge-bundle-0.2.2.24-alpha-0.2.12.exe.asc" rel="nofollow">sig</a>)</li>
</ul>

<p><strong>Tor Browser Bundle with Firefox 4</strong></p>

<p><strong>Tor Browser Bundle (2.2.24-1) alpha; suite=osx</strong></p>

<ul>
<li><a href="http://www.torproject.org/dist/torbrowser/osx/TorBrowser-2.2.24-1-alpha-osx-x86_64-en-US.zip" rel="nofollow">64-bit OS X Tor Browser Bundle with Firefox 4</a> (<a href="http://www.torproject.org/dist/torbrowser/osx/TorBrowser-2.2.24-1-alpha-osx-x86_64-en-US.zip.asc" rel="nofollow">sig</a>)</li>
<li><a href="http://www.torproject.org/dist/torbrowser/osx/TorBrowser-2.2.24-1-alpha-osx-i386-en-US.zip" rel="nofollow">32-bit OS X Tor Browser Bundle with Firefox 4</a> (<a href="http://www.torproject.org/dist/torbrowser/osx/TorBrowser-2.2.24-1-alpha-osx-i386-en-US.zip.asc" rel="nofollow">sig</a>)</li>
</ul>

<ul>
<li>Update Tor to 0.2.2.24-alpha</li>
<li>Update Vidalia to 0.2.12</li>
<li>Update NoScript to 2.1.0.1</li>
</ul>

<p><strong>Tor Browser Bundle (2.2.24-1) alpha; suite=linux</strong></p>

<ul>
<li><a href="http://www.torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-x86_64-2.2.24-1-alpha-en-US.tar.gz" rel="nofollow">64-bit GNU/Linux Tor Browser Bundle with Firefox 4</a> (<a href="http://www.torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-x86_64-2.2.24-1-alpha-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
<li><a href="http://www.torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-i686-2.2.24-1-alpha-en-US.tar.gz" rel="nofollow">32-bit GNU/Linux Tor Browser Bundle with Firefox 4</a> (<a href="http://www.torproject.org/dist/torbrowser/linux/tor-browser-gnu-linux-i686-2.2.24-1-alpha-en-US.tar.gz.asc" rel="nofollow">sig</a>)</li>
</ul>

<ul>
<li>Update Tor to 0.2.2.24-alpha</li>
<li>Update Vidalia to 0.2.12</li>
<li>Update NoScript to 2.1.0.1</li>
<li>Fix missing extensions by putting them in the right location (closes: #2828)</li>
<li>Disable plugin searching (closes: #2827)</li>
</ul>

<p><strong>Tor Browser Bundle with Firefox 3.6</strong></p>

<p><a href="https://www.torproject.org/projects/torbrowser" rel="nofollow">https://www.torproject.org/projects/torbrowser</a></p>

<p><strong>Windows 1.3.23: Released 2011-04-13</strong></p>

<ul>
<li>Update Vidalia to 0.2.12</li>
<li>Fix langpack mistake that made Firefox only use English</li>
</ul>

<p><strong>Linux 1.1.7: Released 2011-04-12</strong></p>

<ul>
<li>Update Tor to 0.2.2.24-alpha</li>
<li> Update Vidalia to 0.2.12</li>
<li>Update NoScript to 2.1.0.1</li>
</ul>

<p><strong>OS X 1.0.15: Released 2011-04-11</strong></p>

<ul>
<li>Update Tor to 0.2.2.24-alpha</li>
<li>Update Vidalia to 0.2.12</li>
<li>Update NoScript to 2.1.0.1</li>
</ul>

