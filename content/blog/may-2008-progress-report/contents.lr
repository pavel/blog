title: May 2008 Progress Report
---
pub_date: 2008-06-25
---
author: phobos
---
tags:

tor
vidalia
progress report
bridges
torbutton
browser bundle
---
categories:

applications
circumvention
network
reports
---
_html_body:

<p>Tor 0.2.0.26-rc (released May 13) fixes a major security vulnerability caused by a bug in Debian's OpenSSL packages. All users running any 0.2.0.x version should upgrade, whether they're running Debian or not.<br />
<a href="http://archives.seul.org/or/talk/May-2008/msg00048.html" rel="nofollow">http://archives.seul.org/or/talk/May-2008/msg00048.html</a></p>

<p>Vidalia 0.1.3 (released May 25) adds a hidden service configuration UI designed and implemented by Domenik Bork, as well as a few other bugfixes.<br />
<a href="http://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.1.3/CHANGELOG" rel="nofollow">http://trac.vidalia-project.net/browser/vidalia/tags/vidalia-0.1.3/CHAN…</a></p>

<p>The Tor Browser Bundle 1.0.2 (released May 3) and 1.0.3 (released May 16) include upgraded versions of Tor, Vidalia, Torbutton, and Firefox.</p>

<p>We added three new part-time developers in May. We hired Matt Edman as a part-time employee at the beginning of May, to work on Vidalia maintenance, bugfixes, and new features. We also are funding Karsten Loesing to work on making hidden service rendezvous and interaction faster, and Peter Palfrader to work on lowering the overhead of directory requests, especially during bootstrap, which should directly improve the experience for Tor users on modems or cell phones.</p>

<p>Google has agreed to give us some funding to work on auto-update for Windows. Our plan is for Vidalia to look at the majority-signed network status consensus to decide when to update and to what version (Tor already lists what versions are considered safe, in each network status document).  We should actually do the update via Tor if possible, for additional privacy, and we need to make sure to check package signatures to ensure package validity. Last, we need to give the user an interface for these updates, including letting her opt to migrate from one major Tor version to the next.</p>

<p>We continued enhancements to the Chinese and Russian Tor website translations. Vidalia also added a Turkish translation.</p>

<p>From the Vidalia 0.1.3 ChangeLog:<br />
"If we're running Tor &gt;= 0.2.0.13-alpha, then check the descriptor annotations for each descriptor before deciding to do a geoip lookup on its IP address. If the annotations indicate it is a special purpose descriptor (e.g., bridges), then don't do the lookup at all."</p>

<p>"Remove the 'Run Tor as a Service' checkbox. Lots of people seem to be clicking it even though they don't really need to, and we end up leaving them in a broken state after a reboot."</p>

<p>"Only display the running relays in the big list of relays to the left of the network map. Listing a big pile of unavailable relays is not particularly useful, and just clutters up the list."</p>

<p>We worked toward a Torbutton 1.2.0rc1 release candidate, which will include support for Firefox 3 along with a huge pile of privacy-related bugfixes.</p>

<p>We spent much of the first half of May dealing with a surprise massive security vulnerability in a crypto library that comes with Debian:<br />
<a href="http://archives.seul.org/or/announce/May-2008/msg00000.html" rel="nofollow">http://archives.seul.org/or/announce/May-2008/msg00000.html</a></p>

<p>You can read a more detailed explanation of the effects of the flaw here:<br />
<a href="https://blog.torproject.org/blog/debian-openssl-flaw%3A-what-does-it-mean-tor-clients%3F" rel="nofollow">https://blog.torproject.org/blog/debian-openssl-flaw%3A-what-does-it-me…</a></p>

<p>Part of dealing with the flaw meant doing some quick design work so we could let new Tor users be safe without making it so old Tor users were cut off from the network:<br />
<a href="https://www.torproject.org/svn/trunk/doc/spec/proposals/136-legacy-keys.txt" rel="nofollow">https://www.torproject.org/svn/trunk/doc/spec/proposals/136-legacy-keys…</a></p>

<p>Sometime in late June or early July we will disable this workaround, meaning all the 0.2.0.x users who haven't upgraded yet will be cut off.</p>

<p>We are preparing for the Tor gathering at the Privacy Enhancing Technologies Symposium in Leuven in July. This is looking like it will be the largest physical gathering of Tor developers ever -- main developers attending include Roger Dingledine, Nick Mathewson, Jacob Appelbaum, Mike Perry, Matt Edman, Steven Murdoch, and Karsten Loesing; Tor researchers include Paul Syverson and Ian Goldberg; and we'll have 5 of our 7 Google Summer of Code students there as well.<br />
<a href="https://blog.torproject.org/events/roger%2C-nick%2C-steven%2C-matt%2C-karsten%2C-paul%2C-jacob-pets" rel="nofollow">https://blog.torproject.org/events/roger%2C-nick%2C-steven%2C-matt%2C-k…</a><br />
<a href="http://petsymposium.org/2008/program.php" rel="nofollow">http://petsymposium.org/2008/program.php</a></p>

<p>The upcoming TBB release in June will include optional instant messaging support via Pidgin + Off-The-Record Messaging; replace the startup batch script with an actual application (named RelativeLink), so TBB now has a helpful Tor icon rather than an ugly batch file icon; and optionally support using WinRAR to produce a self-extracting split bundle.</p>

<p>We now have a more thorough set of TBB build instructions:<br />
<a href="https://svn.torproject.org/svn/torbrowser/trunk/build-scripts/INSTALL" rel="nofollow">https://svn.torproject.org/svn/torbrowser/trunk/build-scripts/INSTALL</a></p>

<p>We also documented the build and deploy process for a new TBB version:<br />
<a href="https://svn.torproject.org/svn/torbrowser/trunk/build-scripts/DEPLOYMENT" rel="nofollow">https://svn.torproject.org/svn/torbrowser/trunk/build-scripts/DEPLOYMENT</a></p>

<p>We finished integrating a UPnP library into Vidalia. This feature allows users who want to set up a Tor relay but don't want to muck with manual port forwarding on their router/firewall to just click a button and have Vidalia interact with their router/firewall automatically. This approach won't work in all cases, but it should work in at least some. The upcoming Vidalia 0.1.4 (scheduled for June) release has folded the UPnP library and GUI changes into the main Vidalia tree, along with a "test" button to try speaking UPnP at the local router and tell the user whether it worked; these features will be available by default in the 0.2.0.x stable release.</p>

<p>We spent May hunting for a better online translation option, since Launchpad (intended to be used for Vidalia translation) has an ugly interface and can't handle our file formats well, and Babelzilla (intended to be used for Torbutton translation) artificially limited the number of concurrent translators we could have.</p>

<p>In early June we hit upon Pootle, which is a translation server that we host, as opposed to a shared web service that other organizations host.  We've set up a test server at <a href="http://translation.torproject.org/" rel="nofollow">http://translation.torproject.org/</a> and imported strings for Vidalia, Torbutton, and Torcheck. We hope to have a lot more to show here in June.</p>

---
_comments:

<a id="comment-101"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-101" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">June 24, 2008</p>
    </div>
    <a href="#comment-101">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-101" class="permalink" rel="bookmark">Apologies for the delay</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry for the delay in getting this out.  I snuck a June update in their as well.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-102"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-102" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 25, 2008</p>
    </div>
    <a href="#comment-102">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-102" class="permalink" rel="bookmark">tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How do I make sure tor is encrypted from my computer to the internet?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-123"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-123" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 11, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-102" class="permalink" rel="bookmark">tor</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-123">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-123" class="permalink" rel="bookmark">re: tor</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>it is by default.  see <a href="https://www.torproject.org/overview" rel="nofollow">https://www.torproject.org/overview</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-103"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-103" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 25, 2008</p>
    </div>
    <a href="#comment-103">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-103" class="permalink" rel="bookmark">cracking the anonymity</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I sure hope some Tor developers will be attending DefCon this year, considering panels like this one:</p>
<p>de-Tor-iorate Anonymity</p>
<p>Nathan Evans<br />
Ph.D Student, University of Denver<br />
Christian Grothoff</p>
<p>Feel safe and comfortable browsing the Internet with impunity because you are using Tor? Feel safe no more! We present an attack on the Tor network that means that the bad guys could find out where you are going on the Internet while using Tor. This presentation goes over the design decisions that have made this attack possible, as well as show results from a Tor network that reveals the paths that data travels when using Tor. This method can make using the Tor network no more secure than using a simple open web proxy. We go over the attack in detail, as well as possible solutions for future versions of Tor.</p>
<p>Nathan Evans is a Ph.D student and the University of Denver working in the areas of security, privacy, anonymity, and performance in P2P networks. While he seems to be running around trying to break all the networks his intentions are to improve the current state of affairs wrt security. Previous work includes Routing in the Dark: Pitch Black (presented at Defcon 15) and work on evaluating various P2P systems published in the German magazine IX.</p>
<p>Christian Grothoff is an assistant professor of computer science at the University of Denver. He earned his PhD in computer science from UCLA in expressive type systems for object-oriented languages. His research interests include compilers, programming languages, software engineering, networking and security. He also is the primary author and maintainer of GNUnet, GNU's peer-to-peer framework.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-125"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-125" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 11, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-103" class="permalink" rel="bookmark">cracking the anonymity</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-125">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-125" class="permalink" rel="bookmark">re: cracking the anonymity</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>omfg, noooo</p>
<p>...without proof, this is rumor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-105"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-105" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 26, 2008</p>
    </div>
    <a href="#comment-105">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-105" class="permalink" rel="bookmark">Is there a TOR forum</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is there a TOR forum somewhere? If there is, what is the adress?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-122"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-122" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 11, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-105" class="permalink" rel="bookmark">Is there a TOR forum</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-122">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-122" class="permalink" rel="bookmark">re: tor forum</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="http://eqt5g4fuenphqinx.onion/" rel="nofollow">http://eqt5g4fuenphqinx.onion/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-171"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-171" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>john orford (not verified)</span> said:</p>
      <p class="date-time">August 07, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-105" class="permalink" rel="bookmark">Is there a TOR forum</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-171">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-171" class="permalink" rel="bookmark">beginner&#039;s question</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor seems to be for people well versed in computer lore.  We poor, downtrodden, much-despised ignorami don't seem to be catered for anywhere.  Should I just go away and play Freecell instead?</p>
<p>I got Tor running to my delight, despite having change the nice simple SeaMonkey to Firefox.  But now I can't google, links don't work, you know the tune.  I'm not an idiot, just an intelligent person who doesn't know much about computers.  Evidently I've missed out something elementary.  I tried to read the advice but to my horror, I couldn't understand half of that either.  </p>
<p>I shall be sorry but not upset if you think Tor is a step too far for me.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-190"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-190" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 17, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-171" class="permalink" rel="bookmark">beginner&#039;s question</a> by <span>john orford (not verified)</span></p>
    <a href="#comment-190">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-190" class="permalink" rel="bookmark">re: beginner&#039;s question</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We produce the Tor Browser Bundle for the easiest download and usage.  It just works.  <a href="https://www.torproject.org/torbrowser/" rel="nofollow">https://www.torproject.org/torbrowser/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-246"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-246" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 15, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-246">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-246" class="permalink" rel="bookmark">Tor New Install Not Working</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Since my old Torpark wasn't working, I thought I would install the newest Tor (Vidalia), and it doesn't work. Once I enable the Tor Button, links can be clicked, but nothing happens. I downloaded the bundle and installed it without changes. I've gone through setup/preferences a few times to check and uncheck things, but so far it's all no good. I downloaded the Win32 bundle for XP (0.2.0.31).</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-250"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-250" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 21, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-246" class="permalink" rel="bookmark">Tor New Install Not Working</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-250">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-250" class="permalink" rel="bookmark">try the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>try the <a href="https://www.torproject.org/torbrowser/" rel="nofollow">https://www.torproject.org/torbrowser/</a>  it's pre-configured and should work right away.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div></div><a id="comment-112"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-112" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 06, 2008</p>
    </div>
    <a href="#comment-112">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-112" class="permalink" rel="bookmark">tor button firefox</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Since upgrading to the latest Vidalia (0.0.16) with tor (0.1.2.19) on MAC OS 10.4.11 and Firefox 2.0.0.15, the tor button stopped working well as it was before. Clicking it does most of the time not work to change the status. Now that China blocks tor it does not make it easier to figure out why. Any clue?<br />
From Beijing / China</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-124"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-124" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 11, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-112" class="permalink" rel="bookmark">tor button firefox</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-124">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-124" class="permalink" rel="bookmark">re: tor button firefox</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>try the latest -rc, it has newer torbutton and vidalia that may work better.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-147"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-147" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 26, 2008</p>
    </div>
    <a href="#comment-147">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-147" class="permalink" rel="bookmark">Does tor send emails to any</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does tor send emails to any address such as 85.xxx.xxx.xxx when it working?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-149"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-149" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymouse (not verified)</span> said:</p>
      <p class="date-time">July 28, 2008</p>
    </div>
    <a href="#comment-149">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-149" class="permalink" rel="bookmark">Hi there.
Can anyone please</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi there.</p>
<p>Can anyone please tell me how it can be that I have to wait up to 4!! minutes for quite a few webpages to be  loaded completely? And more often I have to wait at least 1-2 minutes.<br />
And I don´t talk abouet pages that are covered with pictures, videos, flash, etc., since I have this stuff blocked by a firefox plugin.<br />
This is far away from "surfing the web"!<br />
My firefox version is 2.0.0.16., I use Tor 0.1.2.19 with Vidala 0.0.16 and Qt 4.3.2, which all came in a Vidalia-bundle.<br />
And, I have a 12meg conection.</p>
<p>Thank you in advance.</p>
<p>Cheers</p>
<p>Mouse</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-203"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-203" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 19, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-149" class="permalink" rel="bookmark">Hi there.
Can anyone please</a> by <span>Anonymouse (not verified)</span></p>
    <a href="#comment-203">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-203" class="permalink" rel="bookmark">&quot;Tor Firefox Perf&quot; are the magic words</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>.. for Google/Scroogle: <a href="http://wiki.noreply.org/noreply/TheOnionRouter/FireFoxTorPerf" rel="nofollow">http://wiki.noreply.org/noreply/TheOnionRouter/FireFoxTorPerf</a></p>
<p>Probably the most helpful tips are the torrc options that page mentions. Of those:</p>
<p>* CircuitBuildTimeout 5<br />
* KeepalivePeriod 60<br />
* NumEntryGuards 8</p>
<p>seem to help the most. Things sometimes can still get slow even then, but those options make the "New Identity" button kick you out a much faster circuit for future connections.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-195"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-195" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 18, 2008</p>
    </div>
    <a href="#comment-195">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-195" class="permalink" rel="bookmark">Password??</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi<br />
I have downloaded the latest version of TOR from  your website.  However, every time I launch, it keeps asking for a "password".  I went into Advance setting to disable this feature, but it still keeps asking for this password.  Nothing I do is working.  I've even tried downloading the unstable versions of TOR and I get the same password request. </p>
<p>Is this a Bug?   I never had this issue with your older version. </p>
<p>I am running Windows XP.</p>
<p>tx</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-196"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-196" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 18, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-195" class="permalink" rel="bookmark">Password??</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-196">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-196" class="permalink" rel="bookmark">re: password?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's not a bug, it's vidalia losing track of Tor.  See this FAQ answer:<br />
<a href="https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#TorPasswordPrompt" rel="nofollow">https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#TorPasswordPr…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-419"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-419" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 03, 2008</p>
    </div>
    <a href="#comment-419">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-419" class="permalink" rel="bookmark">Mac OS 10.5 &amp; Torbutton</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Help!  Using brand-new Mac but want to be sure before I download Firefox that the Torbutton will work, or is there a similar option for Safari users?   Any tips or advice appreciated as this is all new to me.  Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-420"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-420" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 04, 2008</p>
    </div>
    <a href="#comment-420">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-420" class="permalink" rel="bookmark">I&#039;ve installed the Vidalia</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've installed the Vidalia bundle on my Intel Mac using OS 10.5.5 with Firefox 3.0.4 and the Tor Button just does not want to show up in Firefox .... does it not work on FF 3.0.4???</p>
<p>Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2226"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2226" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 26, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-420" class="permalink" rel="bookmark">I&#039;ve installed the Vidalia</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2226">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2226" class="permalink" rel="bookmark">re torbutton in 3.04</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can always install torbutton via firefox through <a href="https://www.torproject.org/torbutton/" rel="nofollow">https://www.torproject.org/torbutton/</a></p>
<p>You also really want to update to 3.0.13 at least, since all earlier versions are full of security bugs, the least of which is the ssl null issue found by Moxie.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-536"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-536" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 15, 2009</p>
    </div>
    <a href="#comment-536">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-536" class="permalink" rel="bookmark">VIDALIA BUNDLE AND TOR BUTTON</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello I downloaded Vidalia bundle and it did not work so I downloaded Torbutton extension incombined it work however one without the other did not, shouldnt it have work seprate too? whats going on? also "view network" does not give me full control</p>
<p>BTW am using firefox 2.0.0.17. Windows XP My pc does not run on SP1 or SP2 could this be the problem? if your wondering I crash three times when I downloaded the SP 1,2 get back to me soon hurry!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-538"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-538" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 17, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-536" class="permalink" rel="bookmark">VIDALIA BUNDLE AND TOR BUTTON</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-538">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-538" class="permalink" rel="bookmark">vidalia-bundle</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>vidalia-bundle includes torbutton already.  View network is just for viewing the network so you can see where your traffic is traveling through which circuits.  I don't know what control you were hoping to have from View Network.  </p>
<p>The patch level of Winxp shouldn't matter to the vidalia-bundle.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2059"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2059" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 12, 2009</p>
    </div>
    <a href="#comment-2059">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2059" class="permalink" rel="bookmark">DNS Resolution</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm using Internet Explorer 8.<br />
TOR is installed and working.  I'm wondering can my firewall see my dns results.<br />
Can my company filtering system<br />
see if i searched for "red shoes" at google.com?</p>
<p>I read that the dns or the keywords in searches and web sites are resolved on the tor network,  not on the local machine, which the firewall would see.  Is this accurate?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2224"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2224" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 26, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2059" class="permalink" rel="bookmark">DNS Resolution</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2224">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2224" class="permalink" rel="bookmark">re: dns resolution</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm not sure if IE8 leaks DNS when setup with a socks proxy.  The only real way to find out is to use wireshark to watch for dns traffic exiting the network interface.  Another way to find out is to see if Tor's log complain about only receiving an IP address, not a hostname.</p>
<p>If IE 8 is working correctly, then your local network should only see ssl traffic.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2221"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2221" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 26, 2009</p>
    </div>
    <a href="#comment-2221">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2221" class="permalink" rel="bookmark">How does TOR keep the exit node from being a government server?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>oTwo question:<br />
1.  How does TOR hide datastreams from your ISP when it all has to be routed through them unencrypted, where it can be analyzed by packet sniffers</p>
<p>2.  How does TOR ensure that a secret government computer is not used as a relay (or worse) an exit relay pointing straight at you?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2225"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2225" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 26, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2221" class="permalink" rel="bookmark">How does TOR keep the exit node from being a government server?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2225">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2225" class="permalink" rel="bookmark">re: questions</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>1) the datastreams are wrapped in ssl between your client and all relays, so anyone watching just sees ssl traffic.  I suggest reading <a href="https://www.torproject.org/overview.html.en#thesolution" rel="nofollow">https://www.torproject.org/overview.html.en#thesolution</a></p>
<p>2) We don't and can't.  We encourage people to use secure protocols through Tor.  The risk of an exit relay going evil isn't much different than someone sitting in your favorite coffee shop recording all your traffic over the free wifi.  This explains it in more detail, <a href="https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#ExitEavesdroppers" rel="nofollow">https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#ExitEavesdrop…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
