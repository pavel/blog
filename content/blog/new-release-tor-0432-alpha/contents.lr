title: New Alpha Release: Tor 0.4.3.2-alpha
---
pub_date: 2020-02-11
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.3.2-alpha from the <a href="https://www.torproject.org/download/tor/">download page</a> on the website. Packages should be available over the coming weeks, with a new alpha Tor Browser release likely in the coming week.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>This is the second stable alpha release in the Tor 0.4.3.x series. It fixes several bugs present in the previous alpha release. Anybody running the previous alpha should upgrade and look for bugs in this one instead.</p>
<h2>Changes in version 0.4.3.2-alpha - 2020-02-10</h2>
<ul>
<li>Major bugfixes (onion service client, authorization):
<ul>
<li>On a NEWNYM signal, purge entries from the ephemeral client authorization cache. The permanent ones are kept. Fixes bug <a href="https://bugs.torproject.org/33139">33139</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
<li>Minor features (best practices tracker):
<ul>
<li>Practracker now supports a --regen-overbroad option to regenerate the exceptions file, but only to revise exceptions to be _less_ tolerant of best-practices violations. Closes ticket <a href="https://bugs.torproject.org/32372">32372</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (continuous integration):
<ul>
<li>Run Doxygen Makefile target on Travis, so we can learn about regressions in our internal documentation. Closes ticket <a href="https://bugs.torproject.org/32455">32455</a>.</li>
<li>Stop allowing failures on the Travis CI stem tests job. It looks like all the stem hangs we were seeing before are now fixed. Closes ticket <a href="https://bugs.torproject.org/33075">33075</a>.</li>
</ul>
</li>
<li>Minor bugfixes (build system):
<ul>
<li>Revise configure options that were either missing or incorrect in the configure summary. Fixes bug <a href="https://bugs.torproject.org/32230">32230</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (controller protocol):
<ul>
<li>Fix a memory leak introduced by refactoring of control reply formatting code. Fixes bug <a href="https://bugs.torproject.org/33039">33039</a>; bugfix on 0.4.3.1-alpha.</li>
<li>Fix a memory leak in GETINFO responses. Fixes bug <a href="https://bugs.torproject.org/33103">33103</a>; bugfix on 0.4.3.1-alpha.</li>
<li>When receiving "ACTIVE" or "DORMANT" signals on the control port, report them as SIGNAL events. Previously we would log a bug warning. Fixes bug <a href="https://bugs.torproject.org/33104">33104</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>If we encounter a bug when flushing a buffer to a TLS connection, only log the bug once per invocation of the Tor process. Previously we would log with every occurrence, which could cause us to run out of disk space. Fixes bug <a href="https://bugs.torproject.org/33093">33093</a>; bugfix on 0.3.2.2-alpha.</li>
<li>When logging a bug, do not say "Future instances of this warning will be silenced" unless we are actually going to silence them. Previously we would say this whenever a BUG() check failed in the code. Fixes bug <a href="https://bugs.torproject.org/33095">33095</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service v2):
<ul>
<li>Move a series of v2 onion service warnings to protocol-warning level because they can all be triggered remotely by a malformed request. Fixes bug <a href="https://bugs.torproject.org/32706">32706</a>; bugfix on 0.1.1.14-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service v3, client authorization):
<ul>
<li>When removing client authorization credentials using the control port, also remove the associated descriptor, so the onion service can no longer be contacted. Fixes bug <a href="https://bugs.torproject.org/33148">33148</a>; bugfix on 0.4.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (pluggable transports):
<ul>
<li>When receiving a message on standard error from a pluggable transport, log it at info level, rather than as a warning. Fixes bug <a href="https://bugs.torproject.org/33005">33005</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (rust, build):
<ul>
<li>Fix a syntax warning given by newer versions of Rust that was creating problems for our continuous integration. Fixes bug <a href="https://bugs.torproject.org/33212">33212</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (TLS bug handling):
<ul>
<li>When encountering a bug in buf_read_from_tls(), return a "MISC" error code rather than "WANTWRITE". This change might help avoid some CPU-wasting loops if the bug is ever triggered. Bug reported by opara. Fixes bug <a href="https://bugs.torproject.org/32673">32673</a>; bugfix on 0.3.0.4-alpha.</li>
</ul>
</li>
<li>Code simplification and refactoring (mainloop):
<ul>
<li>Simplify the ip_address_changed() function by removing redundant checks. Closes ticket <a href="https://bugs.torproject.org/33091">33091</a>.</li>
</ul>
</li>
<li>Documentation (manpage):
<ul>
<li>Split "Circuit Timeout" options and "Node Selection" options into their own sections of the tor manpage. Closes tickets 32928 and 32929. Work by Swati Thacker as part of Google Season of Docs.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-286701"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286701" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Guy (not verified)</span> said:</p>
      <p class="date-time">February 12, 2020</p>
    </div>
    <a href="#comment-286701">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286701" class="permalink" rel="bookmark">Its prob just a mcafee issue…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Its prob just a mcafee issue, but after i downloaded this update, mcafee tried to quarantine a couple of the cache files as "ransomware"</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-286705"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286705" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">February 12, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-286701" class="permalink" rel="bookmark">Its prob just a mcafee issue…</a> by <span>Guy (not verified)</span></p>
    <a href="#comment-286705">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286705" class="permalink" rel="bookmark">Yeah, that&#039;s known to happen…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yeah, that's known to happen sometimes. Virus scanners often give false positives on our software. Be careful, and check the signatures on anything you download, but I wouldn't worry too much, especially if you're building Tor from source.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-286917"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-286917" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>TorObsessed (not verified)</span> said:</p>
      <p class="date-time">March 04, 2020</p>
    </div>
    <a href="#comment-286917">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-286917" class="permalink" rel="bookmark">Hello Tor Comrades,
When…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hello Tor Comrades,</p>
<p>When did Tor, or what version of Tor, has the DDoS defenses enabled?<br />
Is there a flag to 'activate' it ?</p>
<p>Running 0.4.2.5. Am i covered ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
