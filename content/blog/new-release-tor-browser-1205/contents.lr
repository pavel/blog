title: New Release: Tor Browser 12.0.5
---
author: richard
---
pub_date: 2023-04-19
---
categories:
applications
releases
---
summary: Tor Browser 12.0.5 is now available from the Tor Browser download page and also from our distribution directory.
---
body:

Tor Browser 12.0.5 is now available from the Tor Browser [download page](https://www.torproject.org/download/) and also
from our [distribution directory](https://dist.torproject.org/torbrowser/12.0.5/).

This release updates Firefox to 102.10.0esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-14/). We also backported the Android-specific [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-13/) from Firefox 112.

We use this opportunity to update various components of Tor Browser as well:
- NoScript 11.4.21

## Build-Signing Infrastructure Updates

We are in the process of updating our build signing infrastructure, and unfortunately are unable to ship code-signed 12.0.5 installers for Windows systems currently. Therefore we will not be providing full Window installers for this release. However, automatic build-to-build upgrades should continue to work as expected. Everything should be back to normal for the 12.0.6 release next month!

## Send us your feedback

If you find a bug or have a suggestion for how we could improve this release, [please let us know](https://support.torproject.org/misc/bug-or-feedback/).

## Full changelog

The full changelog since [Tor Browser 12.0.4](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/maint-12.0/projects/browser/Bundle-Data/Docs/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - Updated NoScript to 11.4.21
  - Updated Go to 1.19.8
  - [Bug tor-browser#41688](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41688): Rebase Tor Browser Stable to 102.10.0esr
- Windows + macOS + Linux
  - Updated Firefox to 102.10esr
  - [Bug tor-browser#41526](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41526): "Cancel" button when establishing a connection should be grey
- Android
  - Updated GeckoView to 102.10esr
  - [Bug tor-browser#41724](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41724): Backport Android-specific security fixes from Firefox 112 to ESR 102.10-based Tor Browser
- Build System
  - Windows
    - [Bug tor-browser-build#40822](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40822): The Tor Browser installer doesn't run with mandatory ASLR on (0xc000007b)
  - Linux
    - [Bug tor-browser-build#40828](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40828): Use http://archive.debian.org/debian-archive/ for jessie
    - [Bug tor-browser-build#40835](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40835): Update faketime URLs in projects/container-image/config
