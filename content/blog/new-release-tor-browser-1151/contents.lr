title: New Release: Tor Browser 11.5.1 (Windows, macOS, Linux)
---
pub_date: 2022-07-26
---
author: pierov
---
categories:

applications
releases
---
summary: Tor Browser 11.5.1 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5.1 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5.1/).

Tor Browser 11.5.1 updates Firefox on Windows, macOS, and Linux to 91.12.0esr.

We would like to thank [WofWca](https://gitlab.torproject.org/WofWca) for sending us some patches for the preferences page.

The full changelog since [Tor Browser 11.5](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=tbb-11.5.1-build1) is:

- Windows + OS X + Linux
  - Update Firefox to 91.12.0esr
  - [Bug tor-browser#41049](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41049): QR codes in connection settings aren't recognized by some readers in dark theme
  - [Bug tor-browser#41050](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41050): "Continue to HTTP Site" button doesn't work on IP addresses
  - [Bug tor-browser#41053](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41053): remove HTTPS-Everywhere entry from browser.uiCustomization.state pref
  - [Bug tor-browser#41054](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41054): Improve color contrast of purple elements in connection settings in dark theme
  - [Bug tor-browser#41055](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41055): Icon fix from #40834 is missing in 11.5 stable
  - [Bug tor-browser#41058](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41058): Hide `currentBridges` description when the section itself is hidden
  - [Bug tor-browser#41059](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41059): Bridge cards aren't displaying, and toggle themselves off
- Build System
  - Windows + OS X + Linux
    - Update Go to 1.17.12
    - [Bug tor-browser-build#40547](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40547): Remove container/remote_* from rbm.conf
    - [Bug tor-browser-build#40584](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40584): Update tor-browser manual to latest
