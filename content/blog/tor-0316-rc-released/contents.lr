title: Tor 0.3.1.6-rc is released!
---
pub_date: 2017-09-05
---
author: nickm
---
tags: stable release
---
categories: releases
---
_html_body:

<p>Hello again!</p>
<p>Tor 0.3.1.6-rc fixes a few small bugs and annoyances in the 0.3.1 release series, including a bug that produced weird behavior on Windows directory caches.</p>
<p>This is the first release candidate in the Tor 0.3.1 series. If we find no new bugs or regressions here, the first stable 0.3.1 release will be nearly identical to it. Please help find bugs! If we don't find any new critical problems, we'll be calling this release series "stable" soon.</p>
<p>If you build Tor from source, you can find Tor 0.3.1.6-rc at the usual place (at the Download page on our website). Otherwise, you'll probably want to wait until packages are available. There should be a new Tor Browser release later this month.</p>
<h2>Changes in version 0.3.1.6-rc - 2017-09-05</h2>
<ul>
<li>Major bugfixes (windows, directory cache):
<ul>
<li>On Windows, do not try to delete cached consensus documents and diffs before they are unmapped from memory--Windows won't allow that. Instead, allow the consensus cache directory to grow larger, to hold files that might need to stay around longer. Fixes bug <a href="https://bugs.torproject.org/22752">22752</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor features (directory authority):
<ul>
<li>Improve the message that authorities report to relays that present RSA/Ed25519 keypairs that conflict with previously pinned keys. Closes ticket <a href="https://bugs.torproject.org/22348">22348</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the August 3 2017 Maxmind GeoLite2 Country database.</li>
</ul>
</li>
<li>Minor features (testing):
<ul>
<li>Add more tests for compression backend initialization. Closes ticket <a href="https://bugs.torproject.org/22286">22286</a>.</li>
</ul>
</li>
<li>Minor bugfixes (directory cache):
<ul>
<li>Fix a memory leak when recovering space in the consensus cache. Fixes bug <a href="https://bugs.torproject.org/23139">23139</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (hidden service):
<ul>
<li>Increase the number of circuits that a service is allowed to open over a specific period of time. The value was lower than it should be (8 vs 12) in the normal case of 3 introduction points. Fixes bug <a href="https://bugs.torproject.org/22159">22159</a>; bugfix on 0.3.0.5-rc.</li>
<li>Fix a BUG warning during HSv3 descriptor decoding that could be cause by a specially crafted descriptor. Fixes bug <a href="https://bugs.torproject.org/23233">23233</a>; bugfix on 0.3.0.1-alpha. Bug found by "haxxpop".</li>
<li>Rate-limit the log messages if we exceed the maximum number of allowed intro circuits. Fixes bug <a href="https://bugs.torproject.org/22159">22159</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging, relay):
<ul>
<li>Remove a forgotten debugging message when an introduction point successfully establishes a hidden service prop224 circuit with a client.</li>
<li>Change three other log_warn() for an introduction point to protocol warnings, because they can be failure from the network and are not relevant to the operator. Fixes bug <a href="https://bugs.torproject.org/23078">23078</a>; bugfix on 0.3.0.1-alpha and 0.3.0.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay):
<ul>
<li>When a relay is not running as a directory cache, it will no longer generate compressed consensuses and consensus diff information. Previously, this was a waste of disk and CPU. Fixes bug <a href="https://bugs.torproject.org/23275">23275</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (robustness, error handling):
<ul>
<li>Improve our handling of the cases where OpenSSL encounters a memory error while encoding keys and certificates. We haven't observed these errors in the wild, but if they do happen, we now detect and respond better. Fixes bug <a href="https://bugs.torproject.org/19418">19418</a>; bugfix on all versions of Tor. Reported by Guido Vranken.</li>
</ul>
</li>
<li>Minor bugfixes (stability):
<ul>
<li>Avoid crashing on a double-free when unable to load or process an included file. Fixes bug <a href="https://bugs.torproject.org/23155">23155</a>; bugfix on 0.3.1.1-alpha. Found with the clang static analyzer.</li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Fix an undersized buffer in test-memwipe.c. Fixes bug <a href="https://bugs.torproject.org/23291">23291</a>; bugfix on 0.2.7.2-alpha. Found and patched by Ties Stuij.</li>
<li>Port the hs_ntor handshake test to work correctly with recent versions of the pysha3 module. Fixes bug <a href="https://bugs.torproject.org/23071">23071</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (Windows service):
<ul>
<li>When running as a Windows service, set the ID of the main thread correctly. Failure to do so made us fail to send log messages to the controller in 0.2.1.16-rc, slowed down controller event delivery in 0.2.7.3-rc and later, and crash with an assertion failure in 0.3.1.1-alpha. Fixes bug <a href="https://bugs.torproject.org/23081">23081</a>; bugfix on 0.2.1.6-alpha. Patch and diagnosis from "Vort".</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-271313"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271313" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>thx (not verified)</span> said:</p>
      <p class="date-time">September 09, 2017</p>
    </div>
    <a href="#comment-271313">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271313" class="permalink" rel="bookmark">will onion share &amp; ricochet…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>will onion share &amp; ricochet update their app ?<br />
ricochet cannot be updated by the user (it is built with) &amp; i do not know if onion share need this version.<br />
i would like use more both ;)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-271340"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-271340" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 13, 2017</p>
    </div>
    <a href="#comment-271340">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-271340" class="permalink" rel="bookmark">Please update the PPA…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please update the PPA repositories. I still have 2.7!</p>
</div>
  </div>
</article>
<!-- Comment END -->
