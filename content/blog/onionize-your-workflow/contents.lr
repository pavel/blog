title: Onionize your Workflow with the Onion Guide Fanzine
---
pub_date: 2021-03-27
---
author: gaba
---
tags:

anti-censorship
onion services
---
categories:

circumvention
onion services
---
summary: One way we help human rights defenders and organizations take back their right to privacy online is by helping them to use and set up onion services. Last year, thanks to the support of Digital Defenders Partnership, we wrote a series of Onion Guides intended to make it easier for our partners to correctly and safely set up their own onion services.
---
_html_body:

<p>At the Tor Project, we build technologies that allow anybody to access the Internet privately. We maintain the software that runs the Tor network as well as utilities and clients to use the network. We also collect anonymous data on the network that allows us to detect problems that may occur, and we connect with the users of the Tor network through training and feedback exercises that help to improve our tools.</p>
<div id="magicdomid7">In some places, the organizations and individuals we work with are in risk of persecution for the digital services they run. It could be reproductive rights services that are criminalized in some countries or content that is censored by an Internet provider or government. Or it could be that they need to protect their own users when accessing their content and find a way for their community to use Tor Browser for protection.</div>
<div id="magicdomid8"> </div>
<div id="magicdomid9">One way we help human rights defenders and organizations take back their right to privacy online is by helping them to use and set up onion services. Websites that are only accessible over Tor are called "onions" and end in the TLD .onion. For example, the DuckDuckGo onion is <a href="https://3g2upl4pq6kufc4m.onion" rel="noreferrer noopener">https://3g2upl4pq6kufc4m.onion</a>. You can access these websites by using Tor Browser. The addresses must be shared with you by the website host, as onions are not indexed in search engines in the typical way that other websites are.</div>
<div id="magicdomid10"> </div>
<div id="magicdomid11">Last year, thanks to the support of <a href="https://www.digitaldefenders.org/">Digital Defenders Partnership</a>, we wrote a series of Onion Guides intended to make it easier for our partners to correctly and safely set up their own onion services. To create these Onion Guides, we collected and improved existing disparate information about the benefits of onion services and how to set them up for a website. </div>
<div id="magicdomid12"> </div>
<div id="magicdomid13">During the last activity of this project, we ran a survey between December 2020 and January 2021. The participants were partner organizations and individuals who were known to use onion services and had received training from Tor in the past. All questions asked were related to the Onion Guides and onion services. Five people responded to this survey.</div>
<blockquote><div id="magicdomid16"><em>“[Tor] offers the possibility for those of us who do work for social transformation to access the Internet safely, without exposing ourselves or exposing our processes, but also, it is a tool that is there and can be even more accessible to different people in different territories</em>.” - Survey response.</div>
</blockquote>
<div id="magicdomid18">When asked if they can define onion services, all participants in this study gave different answers. Some related to specific services, like OnionShare and SecureDrop; others associated onion services to a service without metadata; only two participants answered that it is a service that can only be accessed over the Tor network.</div>
<div> </div>
<div id="magicdomid19">When asked if onion services respond to the threats they or their organizations face, most participants answered YES. One of the participants answered NO. Same for the question asking if you feel safer using onion services.</div>
<div id="magicdomid20"> </div>
<div id="magicdomid21">When asked to define the best benefit of using onion services, most participants answered (a) anonymity; followed by (b) accessing digital security guides and tools; other mentions were: (c) sharing and storing documents and sensitive information; and (d) NAT punching.</div>
<div> </div>
<div id="magicdomid22">When asked if they would recommend onion services to anyone, all survey participants answered YES, because of safety.</div>
<div class="ace-line"> </div>
<div id="magicdomid26">You can find the Onion Guide in our <a href="https://community.torproject.org/">community portal</a>, well as the section on <a href="https://community.torproject.org/onion-services/   ">Onion Services</a>, in <a href="http://3gldbgtv5e4god56.onion/static/images/outreach/print/onion-guide-fanzine-EN.pdf">English</a>, <a href="https://community.torproject.org/static/images/outreach/print/onion-guide-fanzine-ES.pdf">Spanish</a> and <a href="https://community.torproject.org/static/images/outreach/print/onion-guide-fanzine-PT_BR.pdf">Portuguese</a>. Feel free to use it to set up your own .onion site, and let us know how it works for you.</div>

---
_comments:

<a id="comment-291335"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291335" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 09, 2021</p>
    </div>
    <a href="#comment-291335">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291335" class="permalink" rel="bookmark">The fanzine recommends…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The fanzine recommends OnionShare and Ricochet Refresh. Neither has been audited. OnionShare's chat implementation is in fact completely new. If Tor Project recommends it, Tor Project should at least warn people. Otherwise, do a thorough review rather than a recommendation.</p>
<p>The fanzine and community documents for onion services would also do well to mention 1) vanity domain <a href="https://blog.torproject.org/comment/288528#comment-288528" rel="nofollow">generators</a> for v3 onion addresses, 2) OnionBalance, and 3) possibly EOTK. Newbies from the clearnet are used to memorable domain names. Larger sites demand load balancers. These are important for convincing web and network administrators to set up an onion service.</p>
<p>Read:<br />
<a href="https://blog.torproject.org/search/node?keys=vanity" rel="nofollow">https://blog.torproject.org/search/node?keys=vanity</a><br />
<a href="https://blog.torproject.org/search/node?keys=OnionBalance" rel="nofollow">https://blog.torproject.org/search/node?keys=OnionBalance</a></p>
<p>&gt; Five people responded<br />
&gt; When asked if onion services respond to the threats they or their organizations face, one of the participants answered NO. Same for the question asking if you feel safer using onion services.</p>
<p>I'm curious what that one's reasons were.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291508"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291508" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 02, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291335" class="permalink" rel="bookmark">The fanzine recommends…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291508">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291508" class="permalink" rel="bookmark">&gt; I&#039;m curious what that one…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; I'm curious what that one's reasons were.</p>
<p>I had the same thought.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291509"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291509" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 02, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291335" class="permalink" rel="bookmark">The fanzine recommends…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291509">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291509" class="permalink" rel="bookmark">&gt; The fanzine recommends…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; The fanzine recommends OnionShare and Ricochet Refresh. Neither has been audited. OnionShare's chat implementation is in fact completely new. If Tor Project recommends it, Tor Project should at least warn people. Otherwise, do a thorough review rather than a recommendation.</p>
<p>You raise an important point.  I was not aware that OnionShare has not been audited, which is troubling because I constantly recommend it.  I agree that security audits for key privacy products is essential, but I also acknowledge that these are time consuming and expensive.</p>
<p>Tor Project has had good success with asking for donations specifically for the Bug Squash campaign which as all agree is critically important but also deeply unglamorous.  I wonder how other users would respond to a similar campaign calling for donations specifically intended to audit things often used with Tor, such as OnionShare.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291447"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291447" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 28, 2021</p>
    </div>
    <a href="#comment-291447">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291447" class="permalink" rel="bookmark">If you start promoting…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you start promoting onions as a next generation to TLS, CA, DNS, etc. infrastructure, your message will be much more understandable by the broad audience.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291448"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291448" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>nice (not verified)</span> said:</p>
      <p class="date-time">March 28, 2021</p>
    </div>
    <a href="#comment-291448">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291448" class="permalink" rel="bookmark">nice</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><h1>nice</h1>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291459"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291459" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 28, 2021</p>
    </div>
    <a href="#comment-291459">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291459" class="permalink" rel="bookmark">Ransomware attacks on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ransomware attacks on healthcare sites is one of the most dangerous threats facing ordinary citizens everywhere (even ones who do not spend much time online).  And one major way intrusions into healthcare networks happen is when HIPAA entities try to share information about many or sometimes just one patient.  Even before the most recent horrors involving munged sftp type transfers, I have urged the US Congress to recommend OnionShare for small transfers, e.g. patient sends own test results to a new provider, but USG has been happy to say nothing about the fact that the standard way of transferring PHI files is unencrypted fax machine.  Outrageous!</p>
<p>Simillarly for other lockdown-enforced activities such as sharing a file with a local government agency, social services agency, political campaign, etc.</p>
<p> I hope Tor Project, EFF, Micah Lee, etc. can join me in asking Congress to fund federal programs to expand the Tor  network in order to handle a large increase in onion traffic.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291460"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291460" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 28, 2021</p>
    </div>
    <a href="#comment-291460">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291460" class="permalink" rel="bookmark">Speaking of SecureDrop, it…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Speaking of SecureDrop, it would be wonderful if Tor Project had one.  Actually you probably do, but it seems to be a secret if so.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291480"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291480" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 29, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291460" class="permalink" rel="bookmark">Speaking of SecureDrop, it…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291480">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291480" class="permalink" rel="bookmark">As far as I know, the most…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>As far as I know, the most secure and private way they have is PGP. See the <a href="https://www.torproject.org/contact/" rel="nofollow">contact page</a> under "Report a security issue." Other PGP keys are purple key-shaped icons on the <a href="https://www.torproject.org/about/people/" rel="nofollow">People page</a>. But PGP means you need an email address that will be from then onward associated with Tor Project.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291490"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291490" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 30, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291460" class="permalink" rel="bookmark">Speaking of SecureDrop, it…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291490">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291490" class="permalink" rel="bookmark">Do you mean to deliver…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do you mean to deliver documents to the Tor Project?  I've no idea, but would be surprised if they do, given its extensive procedures ( <a href="https://docs.securedrop.org/en/stable/journalist.html" rel="nofollow">https://docs.securedrop.org/en/stable/journalist.html</a> ) would seem overkill for the needs of opensource security devs, so less heavyweight alternatives probably suffice. Obviously they provide a GPG key for secure-email to tor-security list, and of course ProtonMail or similar (perhaps RiseUp) via TOR give opportunity for security and anonymity to discuss stuff, including maybe some kind of special delivery (perhaps via other <a href="https://community.torproject.org/onion-services/" rel="nofollow">https://community.torproject.org/onion-services/</a> Tools like OnionShare or GlobalLeaks, or similar). Best wishes anyway</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291491"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291491" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 30, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291460" class="permalink" rel="bookmark">Speaking of SecureDrop, it…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291491">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291491" class="permalink" rel="bookmark">Do you mean to deliver…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do you mean to deliver documents to the Tor Project?  I've no idea, but would be surprised if they do, given its extensive procedures ( <a href="https://docs.securedrop.org/en/stable/journalist.html" rel="nofollow">https://docs.securedrop.org/en/stable/journalist.html</a> ) would seem overkill for the needs of opensource security devs, so less heavyweight alternatives probably suffice. Obviously they provide a GPG key for secure-email to tor-security list, and of course ProtonMail or similar (perhaps RiseUp) via TOR give opportunity for security and anonymity to discuss stuff, including maybe some kind of special delivery (perhaps via SecureDrop or other <a href="https://community.torproject.org/onion-services/" rel="nofollow">https://community.torproject.org/onion-services/</a> Tools like OnionShare or GlobalLeaks, or similar). Best wishes anyways, and thanks again to the Tor Project team/volunteers for all they manage to do</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291625"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291625" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 22, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291460" class="permalink" rel="bookmark">Speaking of SecureDrop, it…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-291625">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291625" class="permalink" rel="bookmark">SecureDrop is for file…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>SecureDrop is for file sharing. It's designed for whistleblowers and tries to protect the anonymity of the whistleblower. Tor Project makes software. It has PGP keys and the Anonymous Ticketing Portal for reporting bugs. SecureDrop is for a different situation.</p>
<p><a href="https://www.torproject.org/contact/" rel="nofollow">https://www.torproject.org/contact/</a><br />
<a href="https://www.torproject.org/about/people/" rel="nofollow">https://www.torproject.org/about/people/</a><br />
<a href="https://blog.torproject.org/anonymous-gitlab" rel="nofollow">https://blog.torproject.org/anonymous-gitlab</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291462"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291462" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon (not verified)</span> said:</p>
      <p class="date-time">March 28, 2021</p>
    </div>
    <a href="#comment-291462">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291462" class="permalink" rel="bookmark">It&#039;s very irresponsible that…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's very irresponsible that this post introducing the concept of onions would give a V2 address as it's primary "for example" this late into the sunset of V2. Posts like this should be aiming to IMPROVE public understanding of onions, not be introducing obsolete legacy confusions to newcomers. If V2 is being phased out then STOP promoting it to people - months ago!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-291499"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291499" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 31, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291462" class="permalink" rel="bookmark">It&#039;s very irresponsible that…</a> by <span>Anon (not verified)</span></p>
    <a href="#comment-291499">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291499" class="permalink" rel="bookmark">I agree. RSA-1024 is not…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I agree. RSA-1024 is not secure by any reasonable metric in 2021. Fortunately, v2 onions will be fully discontinued this year.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291501"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291501" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 01, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291462" class="permalink" rel="bookmark">It&#039;s very irresponsible that…</a> by <span>Anon (not verified)</span></p>
    <a href="#comment-291501">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291501" class="permalink" rel="bookmark">at this point i&#039;m starting…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>at this point i'm starting to believe that onion.torproject.org is gonna list a whole bunch of unreachable onions in the future</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291549"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291549" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="Anon (BM user over tor)">Anon (BM user … (not verified)</span> said:</p>
      <p class="date-time">April 10, 2021</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-291462" class="permalink" rel="bookmark">It&#039;s very irresponsible that…</a> by <span>Anon (not verified)</span></p>
    <a href="#comment-291549">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291549" class="permalink" rel="bookmark">There are quite a few…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are quite a few privacy projects and protocols where upgrading to v3 onions is non trivial<br />
Especially those mired in the Python2 Obsolescence, with nowhere to go.</p>
<p>Bitmessage (BM)  variants ( especiially those forked from early, reliable codebases ) are one such<br />
functional example. It some respects it matter little that the v3 standard is an improvement.</p>
<p>It is going to be damaging when they stop working, or simply become vulnerable to downgrade<br />
attacks as the limited number of published onion servers goes to zero when its no longer supported<br />
over the tor network at all.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-291489"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291489" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>vtr (not verified)</span> said:</p>
      <p class="date-time">March 30, 2021</p>
    </div>
    <a href="#comment-291489">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291489" class="permalink" rel="bookmark">Thank you for the content</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for the content</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291510"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291510" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 02, 2021</p>
    </div>
    <a href="#comment-291510">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291510" class="permalink" rel="bookmark">&gt; If you start promoting…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; If you start promoting onions as a next generation to TLS, CA, DNS, etc. infrastructure, your message will be much more understandable by the broad audience.</p>
<p>That sounds good to me, but I doubt I have the technical knowledge to really understand how onions compare to the decades old TLS, CAs, DNS infrastructure.  </p>
<p>What I think I understand (someone please correct me if I am wrong) is that </p>
<p>o Surfing to onions (via Tor Browser) runs right around the deeply insecure DNS infrastructure, which in itself would appear to greatly improve privacy and cybersecurity protections.</p>
<p>o The new way of obtaining certs for onions could possibly (yes?) obstruct governments which coerce a CA into creating genuine certificates their "security services" can abuse to punt malware disguised as a "security upgrade" to journalists, dissidents, whistle-blowers, human rights researchers, offshoring researchers, environmental defenders,  activists, opposition politicians, and other people all governments/elites dislike.</p>
<p>o Many of the most horrific data-breaches occur when two (civilian) agencies of the same government try to overcome software incompatibilities with some jury-rigged unsafe file transfer operation when they try to share massive amounts of sensitive data (e.g. COVID-19 vaccination status).  But small scale breaches (especially of personal health data) may be just as harmful to the victim. Onions (via OnionShare and Tor Browser) are not yet capable of coping with large scale file transfers, but appear to be ideally suited for private and secure one-to-one transfer of sensitive files between lawyer and client, doctor and patient, two small small medical clinics.  If so, onions (and Tor Browser and OnionShare) could at one stroke help people managing transfers of sensitive information protect themselves and their clients/patients.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291519"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291519" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>v3 please (not verified)</span> said:</p>
      <p class="date-time">April 04, 2021</p>
    </div>
    <a href="#comment-291519">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291519" class="permalink" rel="bookmark">9 months ago you said …</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>9 months ago you said (please action given v2 deprecation timeline):</p>
<p>&gt; Can you please post the correct v3 addresses for all Tor Project onions?</p>
<p>The Tor Project will very very soon have most of its services on v3 addresses. The missing up to date packages for Onion Balance (with v3 support) in Debian have been uploaded and we'll soon deploy it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-291520"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-291520" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PJ-Manroe (not verified)</span> said:</p>
      <p class="date-time">April 04, 2021</p>
    </div>
    <a href="#comment-291520">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-291520" class="permalink" rel="bookmark">I&#039;ve been using Tor for a…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I've been using Tor for a little while now and really like it.  I'm still new to the .onion services and only know a few sites.  So I'm interested in learning more.  I believe privacy is a big concern.  Especially for journalists across the world.  As in the case of, Myanmar and the middle-east.  I've been to Thailand and hear they are having some troubles right now.</p>
</div>
  </div>
</article>
<!-- Comment END -->
