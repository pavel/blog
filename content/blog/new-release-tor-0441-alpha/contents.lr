title: New release: Tor 0.4.4.1-alpha
---
pub_date: 2020-06-16
---
author: nickm
---
tags: alpha release
---
categories: releases
---
_html_body:

<p>There's a new alpha release available for download. If you build Tor from source, you can download the source code for 0.4.4.1-alpha from the <a href="https://www.torproject.org/download/tor/">download page</a>. Packages should be available over the coming weeks, with a new alpha Tor Browser release by early July.</p>
<p>Remember, this is an alpha release: you should only run this if you'd like to find and report more bugs than usual.</p>
<p>This is the first alpha release in the 0.4.4.x series. It improves our guard selection algorithms, improves the amount of code that can be disabled when running without relay support, and includes numerous small bugfixes and enhancements. It also lays the ground for some IPv6 features that we'll be developing more in the next (0.4.5) series.</p>
<p>Here are the changes since 0.4.3.5.</p>
<h2>Changes in version 0.4.4.1-alpha - 2020-06-16</h2>
<ul>
<li>Major features (Proposal 310, performance + security):
<ul>
<li>Implements Proposal 310, "Bandaid on guard selection". Proposal 310 solves load-balancing issues with older versions of the guard selection algorithm, and improves its security. Under this new algorithm, a newly selected guard never becomes Primary unless all previously sampled guards are unreachable. Implements recommendation from 32088. (Proposal 310 is linked to the CLAPS project researching optimal client location-aware path selections. This project is a collaboration between the UCLouvain Crypto Group, the U.S. Naval Research Laboratory, and Princeton University.)</li>
</ul>
</li>
<li>Major features (IPv6, relay):
<ul>
<li>Consider IPv6-only EXTEND2 cells valid on relays. Log a protocol warning if the IPv4 or IPv6 address is an internal address, and internal addresses are not allowed. But continue to use the other address, if it is valid. Closes ticket <a href="https://bugs.torproject.org/33817">33817</a>.</li>
<li>If a relay can extend over IPv4 and IPv6, and both addresses are provided, it chooses between them uniformly at random. Closes ticket <a href="https://bugs.torproject.org/33817">33817</a>.</li>
<li>Re-use existing IPv6 connections for circuit extends. Closes ticket <a href="https://bugs.torproject.org/33817">33817</a>.</li>
<li>Relays may extend circuits over IPv6, if the relay has an IPv6 ORPort, and the client supplies the other relay's IPv6 ORPort in the EXTEND2 cell. IPv6 extends will be used by the relay IPv6 ORPort self-tests in 33222. Closes ticket <a href="https://bugs.torproject.org/33817">33817</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Major features (v3 onion services):
<ul>
<li>Allow v3 onion services to act as OnionBalance backend instances, by using the HiddenServiceOnionBalanceInstance torrc option. Closes ticket <a href="https://bugs.torproject.org/32709">32709</a>.</li>
</ul>
</li>
<li>Minor feature (developer tools):
<ul>
<li>Add a script to help check the alphabetical ordering of option names in the manual page. Closes ticket <a href="https://bugs.torproject.org/33339">33339</a>.</li>
</ul>
</li>
<li>Minor feature (onion service client, SOCKS5):
<ul>
<li>Add 3 new SocksPort ExtendedErrors (F2, F3, F7) that reports back new type of onion service connection failures. The semantics of these error codes are documented in proposal 309. Closes ticket <a href="https://bugs.torproject.org/32542">32542</a>.</li>
</ul>
</li>
<li>Minor feature (onion service v3):
<ul>
<li>If a service cannot upload its descriptor(s), log why at INFO level. Closes ticket <a href="https://bugs.torproject.org/33400">33400</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor feature (python scripts):
<ul>
<li>Stop assuming that /usr/bin/python exists. Instead of using a hardcoded path in scripts that still use Python 2, use /usr/bin/env, similarly to the scripts that use Python 3. Fixes bug <a href="https://bugs.torproject.org/33192">33192</a>; bugfix on 0.4.2.</li>
</ul>
</li>
<li>Minor features (client-only compilation):
<ul>
<li>Disable more code related to the ext_orport protocol when compiling without support for relay mode. Closes ticket <a href="https://bugs.torproject.org/33368">33368</a>.</li>
<li>Disable more of our self-testing code when support for relay mode is disabled. Closes ticket <a href="https://bugs.torproject.org/33370">33370</a>.</li>
</ul>
</li>
<li>Minor features (code safety):
<ul>
<li>Check for failures of tor_inet_ntop() and tor_inet_ntoa() functions in DNS and IP address processing code, and adjust codepaths to make them less likely to crash entire Tor instances. Resolves issue <a href="https://bugs.torproject.org/33788">33788</a>.</li>
</ul>
</li>
<li>Minor features (compilation size):
<ul>
<li>Most server-side DNS code is now disabled when building without support for relay mode. Closes ticket <a href="https://bugs.torproject.org/33366">33366</a>.</li>
</ul>
</li>
<li>Minor features (continuous integration):
<ul>
<li>Run unit-test and integration test (Stem, Chutney) jobs with ALL_BUGS_ARE_FATAL macro being enabled on Travis and Appveyor. Resolves ticket <a href="https://bugs.torproject.org/32143">32143</a>.</li>
</ul>
</li>
<li>Minor features (control port):
<ul>
<li>Return a descriptive error message from the 'GETINFO status/fresh- relay-descs' command on the control port. Previously, we returned a generic error of "Error generating descriptor". Closes ticket <a href="https://bugs.torproject.org/32873">32873</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (developer tooling):
<ul>
<li>Refrain from listing all .a files that are generated by the Tor build in .gitignore. Add a single wildcard *.a entry that covers all of them for present and future. Closes ticket <a href="https://bugs.torproject.org/33642">33642</a>.</li>
<li>Add a script ("git-install-tools.sh") to install git hooks and helper scripts. Closes ticket <a href="https://bugs.torproject.org/33451">33451</a>.</li>
</ul>
</li>
<li>Minor features (directory authority, shared random):
<ul>
<li>Refactor more authority-only parts of the shared-random scheduling code to reside in the dirauth module, and to be disabled when compiling with --disable-module-dirauth. Closes ticket <a href="https://bugs.torproject.org/33436">33436</a>.</li>
</ul>
</li>
<li>Minor features (directory):
<ul>
<li>Remember the number of bytes we have downloaded for each directory purpose while bootstrapping, and while fully bootstrapped. Log this information as part of the heartbeat message. Closes ticket <a href="https://bugs.torproject.org/32720">32720</a>.</li>
</ul>
</li>
<li>Minor features (IPv6 support):
<ul>
<li>Adds IPv6 support to tor_addr_is_valid(). Adds tests for the above changes and tor_addr_is_null(). Closes ticket <a href="https://bugs.torproject.org/33679">33679</a>. Patch by MrSquanchee.</li>
<li>Allow clients and relays to send dual-stack and IPv6-only EXTEND2 cells. Parse dual-stack and IPv6-only EXTEND2 cells on relays. Closes ticket <a href="https://bugs.torproject.org/33901">33901</a>.</li>
</ul>
</li>
<li>Minor features (logging):
<ul>
<li>When trying to find our own address, add debug-level logging to report the sources of candidate addresses. Closes ticket <a href="https://bugs.torproject.org/32888">32888</a>.</li>
</ul>
</li>
<li>Minor features (testing, architecture):
<ul>
<li>Our test scripts now double-check that subsystem initialization order is consistent with the inter-module dependencies established by our .may_include files. Implements ticket <a href="https://bugs.torproject.org/31634">31634</a>.</li>
<li>Initialize all subsystems at the beginning of our unit test harness, to avoid crashes due to uninitialized subsystems. Follow- up from ticket <a href="https://bugs.torproject.org/33316">33316</a>.</li>
</ul>
</li>
<li>Minor features (v3 onion services):
<ul>
<li>Add v3 onion service status to the dumpstats() call which is triggered by a SIGUSR1 signal. Previously, we only did v2 onion services. Closes ticket <a href="https://bugs.torproject.org/24844">24844</a>. Patch by Neel Chauhan.</li>
</ul>
</li>
<li>Minor features (windows):
<ul>
<li>Add support for console control signals like Ctrl+C in Windows. Closes ticket <a href="https://bugs.torproject.org/34211">34211</a>. Patch from Damon Harris (TheDcoder).</li>
</ul>
</li>
<li>Minor bugfix (onion service v3):
<ul>
<li>Prevent an assert() that would occur when cleaning the client descriptor cache, and attempting to close circuits for a non- decrypted descriptor (lacking client authorization). Fixes bug <a href="https://bugs.torproject.org/33458">33458</a>; bugfix on 0.4.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfix (refactoring):
<ul>
<li>Lift circuit_build_times_disabled() out of the circuit_expire_building() loop, to save CPU time when there are many circuits open. Fixes bug <a href="https://bugs.torproject.org/33977">33977</a>; bugfix on 0.3.5.9.</li>
</ul>
</li>
<li>Minor bugfixes (client performance):
<ul>
<li>Resume use of preemptively-built circuits when UseEntryGuards is set to 0. We accidentally disabled this feature with that config setting, leading to slower load times. Fixes bug <a href="https://bugs.torproject.org/34303">34303</a>; bugfix on 0.3.3.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (directory authorities):
<ul>
<li>Directory authorities now reject votes that arrive too late. In particular, once an authority has started fetching missing votes, it no longer accepts new votes posted by other authorities. This change helps prevent a consensus split, where only some authorities have the late vote. Fixes bug <a href="https://bugs.torproject.org/4631">4631</a>; bugfix on 0.2.0.5-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (git scripts):
<ul>
<li>Stop executing the checked-out pre-commit hook from the pre-push hook. Instead, execute the copy in the user's git directory. Fixes bug <a href="https://bugs.torproject.org/33284">33284</a>; bugfix on 0.4.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (initialization):
<ul>
<li>Initialize the subsystems in our code in an order more closely corresponding to their dependencies, so that every system is initialized before the ones that (theoretically) depend on it. Fixes bug <a href="https://bugs.torproject.org/33316">33316</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (IPv4, relay):
<ul>
<li>Check for invalid zero IPv4 addresses and ports when sending and receiving extend cells. Fixes bug <a href="https://bugs.torproject.org/33900">33900</a>; bugfix on 0.2.4.8-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (IPv6, relay):
<ul>
<li>Consider IPv6 addresses when checking if a connection is canonical. In 17604, relays assumed that a remote relay could consider an IPv6 connection canonical, but did not set the canonical flag on their side of the connection. Fixes bug <a href="https://bugs.torproject.org/33899">33899</a>; bugfix on 0.3.1.1-alpha.</li>
<li>Log IPv6 addresses on connections where this relay is the responder. Previously, responding relays would replace the remote IPv6 address with the IPv4 address from the consensus. Fixes bug <a href="https://bugs.torproject.org/33899">33899</a>; bugfix on 0.3.1.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (linux seccomp sandbox nss):
<ul>
<li>Fix a startup crash when tor is compiled with --enable-nss and sandbox support is enabled. Fixes bug <a href="https://bugs.torproject.org/34130">34130</a>; bugfix on 0.3.5.1-alpha. Patch by Daniel Pinto.</li>
</ul>
</li>
<li>Minor bugfixes (logging, testing):
<ul>
<li>Make all of tor's assertion macros support the ALL_BUGS_ARE_FATAL and DISABLE_ASSERTS_IN_UNIT_TESTS debugging modes. (IF_BUG_ONCE() used to log a non-fatal warning, regardless of the debugging mode.) Fixes bug <a href="https://bugs.torproject.org/33917">33917</a>; bugfix on 0.2.9.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logs):
<ul>
<li>Remove surprising empty line in the INFO-level log about circuit build timeout. Fixes bug <a href="https://bugs.torproject.org/33531">33531</a>; bugfix on 0.3.3.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (mainloop):
<ul>
<li>Better guard against growing a buffer past its maximum 2GB in size. Fixes bug <a href="https://bugs.torproject.org/33131">33131</a>; bugfix on 0.3.0.4-rc.</li>
</ul>
</li>
<li>Minor bugfixes (manual page):
<ul>
<li>Update the man page to reflect that MinUptimeHidServDirectoryV2 defaults to 96 hours. Fixes bug <a href="https://bugs.torproject.org/34299">34299</a>; bugfix on 0.2.6.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service v3, client):
<ul>
<li>Remove a BUG() that was causing a stacktrace when a descriptor changed at an unexpected time. Fixes bug <a href="https://bugs.torproject.org/28992">28992</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (onion service, logging):
<ul>
<li>Fix a typo in a log message PublishHidServDescriptors is set to 0. Fixes bug <a href="https://bugs.torproject.org/33779">33779</a>; bugfix on 0.3.2.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (portability):
<ul>
<li>Fix a portability error in the configure script, where we were using "==" instead of "=". Fixes bug <a href="https://bugs.torproject.org/34233">34233</a>; bugfix on 0.4.3.5.</li>
</ul>
</li>
<li>Minor bugfixes (protocol versions):
<ul>
<li>Sort tor's supported protocol version lists, as recommended by the tor directory specification. Fixes bug <a href="https://bugs.torproject.org/33285">33285</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relays):
<ul>
<li>Stop advertising incorrect IPv6 ORPorts in relay and bridge descriptors, when the IPv6 port was configured as "auto". Fixes bug <a href="https://bugs.torproject.org/32588">32588</a>; bugfix on 0.2.3.9-alpha.</li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Define and use a new constant TOR_ADDRPORT_BUF_LEN which is like TOR_ADDR_BUF_LEN but includes enough space for an IP address, brackets, separating colon, and port number. Closes ticket <a href="https://bugs.torproject.org/33956">33956</a>. Patch by Neel Chauhan.</li>
<li>Merge the orconn and ocirc events into the "core" subsystem, which manages or connections and origin circuits. Previously they were isolated in subsystems of their own.</li>
<li>Move LOG_PROTOCOL_WARN to app/config. Resolves a dependency inversion. Closes ticket <a href="https://bugs.torproject.org/33633">33633</a>.</li>
<li>Move the circuit extend code to the relay module. Split the circuit extend function into smaller functions. Closes ticket <a href="https://bugs.torproject.org/33633">33633</a>.</li>
<li>Rewrite port_parse_config() to use the default port flags from port_cfg_new(). Closes ticket <a href="https://bugs.torproject.org/32994">32994</a>. Patch by MrSquanchee.</li>
<li>Updated comments in 'scheduler.c' to reflect old code changes, and simplified the scheduler channel state change code. Closes ticket <a href="https://bugs.torproject.org/33349">33349</a>.</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Document the limitations of using %include on config files with seccomp sandbox enabled. Fixes documentation bug <a href="https://bugs.torproject.org/34133">34133</a>; bugfix on 0.3.1.1-alpha. Patch by Daniel Pinto.</li>
<li>Fix several doxygen warnings related to imbalanced groups. Closes ticket <a href="https://bugs.torproject.org/34255">34255</a>.</li>
</ul>
</li>
<li>Removed features:
<ul>
<li>Remove the ClientAutoIPv6ORPort option. This option attempted to randomly choose between IPv4 and IPv6 for client connections, and wasn't a true implementation of Happy Eyeballs. Often, this option failed on IPv4-only or IPv6-only connections. Closes ticket <a href="https://bugs.torproject.org/32905">32905</a>. Patch by Neel Chauhan.</li>
<li>Stop shipping contrib/dist/rc.subr file, as it is not being used on FreeBSD anymore. Closes issue <a href="https://bugs.torproject.org/31576">31576</a>.</li>
</ul>
</li>
<li>Testing:
<ul>
<li>Add a basic IPv6 test to "make test-network". This test only runs when the local machine has an IPv6 stack. Closes ticket <a href="https://bugs.torproject.org/33300">33300</a>.</li>
<li>Add test-network-ipv4 and test-network-ipv6 jobs to the Makefile. These jobs run the IPv4-only and dual-stack chutney flavours from test-network-all. Closes ticket <a href="https://bugs.torproject.org/33280">33280</a>.</li>
<li>Remove a redundant distcheck job. Closes ticket <a href="https://bugs.torproject.org/33194">33194</a>.</li>
<li>Run the test-network-ipv6 Makefile target in the Travis CI IPv6 chutney job. This job runs on macOS, so it's a bit slow. Closes ticket <a href="https://bugs.torproject.org/33303">33303</a>.</li>
<li>Sort the Travis jobs in order of speed. Putting the slowest jobs first takes full advantage of Travis job concurrency. Closes ticket <a href="https://bugs.torproject.org/33194">33194</a>.</li>
<li>Stop allowing the Chutney IPv6 Travis job to fail. This job was previously configured to fast_finish (which requires allow_failure), to speed up the build. Closes ticket <a href="https://bugs.torproject.org/33195">33195</a>.</li>
<li>Test v3 onion services to tor's mixed IPv4 chutney network. And add a mixed IPv6 chutney network. These networks are used in the test-network-all, test-network-ipv4, and test-network-ipv6 make targets. Closes ticket <a href="https://bugs.torproject.org/33334">33334</a>.</li>
<li>Use the "bridges+hs-v23" chutney network flavour in "make test- network". This test requires a recent version of chutney (mid- February 2020). Closes ticket <a href="https://bugs.torproject.org/28208">28208</a>.</li>
<li>When a Travis chutney job fails, use chutney's new "diagnostics.sh" tool to produce detailed diagnostic output. Closes ticket <a href="https://bugs.torproject.org/32792">32792</a>.</li>
</ul>
</li>
<li>Code simplification and refactoring (onion service):
<ul>
<li>Refactor configuration parsing to use the new config subsystem code. Closes ticket <a href="https://bugs.torproject.org/33014">33014</a>.</li>
</ul>
</li>
<li>Code simplification and refactoring (relay address):
<ul>
<li>Move a series of functions related to address resolving into their own files. Closes ticket <a href="https://bugs.torproject.org/33789">33789</a>.</li>
</ul>
</li>
<li>Documentation (manual page):
<ul>
<li>Add cross reference links and a table of contents to the HTML tor manual page. Closes ticket <a href="https://bugs.torproject.org/33369">33369</a>. Work by Swati Thacker as part of Google Season of Docs.</li>
<li>Alphabetize the Denial of Service Mitigation Options, Directory Authority Server Options, Hidden Service Options, and Testing Network Options sections of the tor(1) manual page. Closes ticket <a href="https://bugs.torproject.org/33275">33275</a>. Work by Swati Thacker as part of Google Season of Docs.</li>
<li>Refrain from mentioning nicknames in manpage section for MyFamily torrc option. Resolves issue <a href="https://bugs.torproject.org/33417">33417</a>.</li>
<li>Updated the options set by TestingTorNetwork in the manual page. Closes ticket <a href="https://bugs.torproject.org/33778">33778</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-288323"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288323" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 17, 2020</p>
    </div>
    <a href="#comment-288323">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288323" class="permalink" rel="bookmark">&gt; It also lays the ground…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; It also lays the ground for some IPv6 features that we'll be developing more in the next (0.4.5) series.</p>
<p>Looking forward to this. I'm in the process of setting up a new Linux gateway for my network, and I'm strongly considering the possibility of going IPv6-only, but I wouldn't be able to host a bridge until #23824,etc are resolved.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-288334"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-288334" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 18, 2020</p>
    </div>
    <a href="#comment-288334">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-288334" class="permalink" rel="bookmark">nickm is like an eclipse, he…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>nickm is like an eclipse, he doesn't always appear but when he does it's a treat!</p>
<p>Fantastic progress and great release! The best highlight for me is the OnionBalance one, since I'm an HS operator, but all the rest is also very good!</p>
</div>
  </div>
</article>
<!-- Comment END -->
