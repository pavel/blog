title: New Release: Tor Browser 10.5a2
---
pub_date: 2020-10-21
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.5
---
categories: applications
---
summary: Tor Browser 10.5a2 for Desktop platforms is now available from the Tor Browser Alpha download page and also from our distribution directory.
---
_html_body:

<p>Tor Browser 10.5a2 for Desktop platforms is now available from the <a href="https://www.torproject.org/download/alpha/">Tor Browser Alpha download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.5a2/">distribution directory</a>.</p>
<p><b>Note:</b> This is an alpha release, an experimental version for users who want to help us test new features. For everyone else, we recommend downloading the <a href="https://blog.torproject.org/new-release-tor-browser-1002">latest stable release</a> instead.</p>
<p>Tor Browser 10.5a2 ships with Firefox 78.4.0esr, updates NoScript to 11.1.3, and OpenSSL to 1.1.1h. This release includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020-46/">security updates</a> to Firefox.</p>
<p><strong>Note:</strong> Tor Browser 10.5 <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40089">does not support CentOS 6</a>.</p>
<p><strong>Note</strong>: We encountered updater issues for all alpha users that have been auto-updating the alpha series for months. We changed the accepted MAR channel ID to <em>torbrowser-torproject-alpha</em> as we are on an alpha channel. The assumption was that enough time passed since we changed it last time to <em>torbrowser-torproject-release,torbrowser-torproject-alpha</em> but it turns out that change did not get applied. <strong>Workaround:</strong> change the torbrowser-torproject-release in your <em>update-settings.ini</em> (in the <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/wikis/Platform-Installation">Browser's code directory</a>, which depends on you operating system) file to <em>torbrowser-torproject-alpha</em> and the update should get applied successfully. Alternatively, downloading a fresh alpha copy of Tor Browser works as well. Sorry for the inconvenience.</p>
<p><strong>Note:</strong> Now Javascript on the Safest security level is <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40082">governed by NoScript</a> again. It was set as <strong>false</strong> when on <em>Safest</em> in <a href="https://blog.torproject.org/new-release-tor-browser-95a9">9.5a9</a>. The <strong>javascript.enabled</strong> preference was reset to <strong>true</strong> beginning in <a href="https://blog.torproject.org/new-release-tor-browser-105a1">Tor Browser 10.5a1</a> for everyone using <em>Safest</em> and you must re-set it as <strong>false</strong> if that is your preference.</p>
<p>The <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master">full changelog</a> since Tor Browser 10.5a1 is:</p>
<ul>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 78.4.0esr</li>
<li>Update NoScript to 11.1.3</li>
<li>Update OpenSSL to 1.1.1h</li>
<li>Update Tor Launcher to 0.2.26
<ul>
<li>Translations update</li>
</ul>
</li>
<li><a href="https://bugs.torproject.org/31767">Bug 31767</a>: Avoid using intl.locale.requested preference directly</li>
<li><a href="https://bugs.torproject.org/33954">Bug 33954</a>: Consider different approach for Bug 2176</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40011">Bug 40011</a>: Rename tor-browser-brand.ftl to brand.ftl</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40012">Bug 40012</a>: Fix about:tor not loading some images in 82</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40013">Bug 40013</a>: End of year 2020 Fundraising campaign</li>
<li><a href="https://bugs.torproject.org/tpo/applications/torbutton/40016">Bug 40016</a>: Fix onion pattern for LTR locales</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40139">Bug 40139</a>: Update Onboarding icon for 10.0</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40148">Bug 40148</a>: Disable Picture-in-Picture until we investigate and possibly fix it</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40166">Bug 40166</a>: Disable security.certerrors.mitm.auto_enable_enterprise_roots</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40192">Bug 40192</a>: Backport Mozilla Bug 1658881</li>
<li>Translations update</li>
</ul>
</li>
<li>Windows
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40140">Bug 40140</a>: Videos stop working with Tor Browser 10.0 on Windows</li>
</ul>
</li>
<li>Build System
<ul>
<li>Windows + OS X + Linux
<ul>
<li>Update Go to 1.14.10</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40104">Bug 40104</a>: Use our TMPDIR when creating our .mar files</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40118">Bug 40118</a>: Add missing libdrm dev package to firefox container</li>
</ul>
</li>
<li>Windows
<ul>
<li><a href="https://bugs.torproject.org/34360">Bug 34360</a>: Bump binutils to 2.35.1</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40051">Bug 40051</a>: Remove SOURCE_DATE_EPOCH patch</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40131">Bug 40131</a>: Remove unused binutils patches</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-290043"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290043" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>grebs (not verified)</span> said:</p>
      <p class="date-time">October 22, 2020</p>
    </div>
    <a href="#comment-290043">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290043" class="permalink" rel="bookmark">i often invoke tor b4 doing…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i often invoke tor b4 doing work on computer but there does not appear  2b any diff. in how the comp. works....so am i connected or not?  is there a special 'google' type of search pgm that i can/should use with tor?  the search bar always shows that i'm using duck duck go.  is that normal?  i always use firefox, then tor, then an ips (ipa?).  is that procedure ok?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-290073"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290073" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 24, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-290043" class="permalink" rel="bookmark">i often invoke tor b4 doing…</a> by <span>grebs (not verified)</span></p>
    <a href="#comment-290073">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290073" class="permalink" rel="bookmark">If you use Tor Browser and…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you use Tor Browser and have not torified other applications, then only Tor Browser connects to the Tor network through the tor daemon. If you use the expert bundle, which is the tor daemon without the browser, and have not reconfigured it, then only the tor daemon connects to the Tor network.</p>
<p>It sounds like you are misunderstanding several things. Please review the <a href="https://support.torproject.org/" rel="nofollow">Support</a> website, <a href="https://tb-manual.torproject.org/" rel="nofollow">Tor Browser manual</a>, old <a href="https://2019.www.torproject.org/docs/faq.html.en" rel="nofollow">General FAQ</a>, and open the address <span class="geshifilter"><code class="php geshifilter-php">about<span style="color: #339933;">:</span>tor</code></span> and click the onion circle in the top left.</p>
<p>Google is not a search program that runs on your local device. It is a website and search engine that you access online through a web browser. You can check if a particular web browser is connected to the Tor network by visiting: <a href="https://check.torproject.org/" rel="nofollow">https://check.torproject.org/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-290164"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290164" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">November 02, 2020</p>
    </div>
    <a href="#comment-290164">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290164" class="permalink" rel="bookmark">&gt; Bug 40148: Disable Picture…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; Bug 40148: Disable Picture-in-Picture until we investigate and possibly fix it</p>
<p>To <a href="https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40147#note_2712639" rel="nofollow">Georg's comment</a>, the question is not whether Picture-in-Picture works. It does work as it does in Firefox. The question is how, if at all, to make it safe and non-fingerprintable. For instance, letterboxing is not applied to its window. Also, its window opens to a size that fits the resolution of the video, but many video players load in Auto resolution mode which dynamically adjusts the resolution based on present network speed. Those are just two issues from just casually using the UI.</p>
<p>Picture-in-picture is NOT enabled by <span class="geshifilter"><code class="php geshifilter-php">media<span style="color: #339933;">.</span>videocontrols<span style="color: #339933;">.</span>picture<span style="color: #339933;">-</span>in<span style="color: #339933;">-</span>picture<span style="color: #339933;">.</span>video<span style="color: #339933;">-</span>toggle<span style="color: #339933;">.</span>enabled</code></span>. Only the "video-toggle" is, which means only **whether the blue button is hidden** in its browser tab and right-click menu when the video is not in the picture-in-picture floating window. <span class="geshifilter"><code class="php geshifilter-php">video<span style="color: #339933;">-</span>toggle<span style="color: #339933;">.</span>enabled</code></span> does not control enabling the feature; only whether its button is displayed. Picture-in-picture is enabled by the preference, <span class="geshifilter"><code class="php geshifilter-php">media<span style="color: #339933;">.</span>videocontrols<span style="color: #339933;">.</span>picture<span style="color: #339933;">-</span>in<span style="color: #339933;">-</span>picture<span style="color: #339933;">.</span>enabled</code></span>.</p>
<p>To see them all, just open about:config, and type "videocontrols". While you're there, <span class="geshifilter"><code class="php geshifilter-php">media<span style="color: #339933;">.</span>videocontrols<span style="color: #339933;">.</span>lock<span style="color: #339933;">-</span>video<span style="color: #339933;">-</span>orientation</code></span> is not related to picture-in-picture but also sounds fingerprintable.</p>
<p>Descriptions of an older set of PiP preferences are under the "overview" heading here: <a href="https://www.ghacks.net/2019/10/29/a-look-at-firefoxs-upcoming-picture-in-picture-mode/" rel="nofollow">https://www.ghacks.net/2019/10/29/a-look-at-firefoxs-upcoming-picture-i…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
