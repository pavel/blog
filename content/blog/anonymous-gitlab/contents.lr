title: Anonymous GitLab Ticketing: An Exciting New Project at Tor
---
pub_date: 2021-02-09
---
author: MariaV
---
tags:

privacy
gitlab
anonymity
Outreachy
---
_html_body:

<p>Hi! My name is Maria Violante, and I’m one of two <a href="https://www.outreachy.org/">Outreachy</a> interns for Tor Project for Winter 2020/2021. I’m <em>thrilled </em>to share with you the results of my internship thus far: the <a href="https://anonticket.onionize.space/">Anonymous Ticket Portal</a>, which allows individuals to submit instant, anonymous tickets to participate in GitLab repos without signing up for a GitLab account or disclosing any personal data.</p>
<p>Keep reading to find out how you can participate in and benefit from the Anonymous Ticket Portal!</p>
<p><strong>Why Anonymous Ticketing?</strong></p>
<p>Currently, before making a bug report to one of Tor’s repos, users must sign up for a GitLab account via the TicketLobby (<a href="https://gitlab.onionize.space/" rel="noreferrer noopener">https://gitlab.onionize.space/</a>). Although this is the right approach for many users, it has its limitations:</p>
<ul>
<li>It’s overkill for the occasional or one-time bug reporter.</li>
</ul>
<ul>
<li>The delay between requesting a GitLab account and approval by a moderator means bug reports are lost, as people may not return to submit their bug or remember the circumstances that provoked the bug in the first place.</li>
</ul>
<ul>
<li>Many privacy-focused Tor users don’t feel comfortable providing their email for a bug report.</li>
</ul>
<p><strong>This anonymous ticketing portal is designed to circumvent these limitations, resulting in more complete, <i>private</i> bug reporting, and includes the following features:</strong></p>
<p><strong>Lightning-fast, Anonymous (and Lazy) User Interface</strong></p>
<p>Instead of username and password, the Anonymous Ticket Portal's authentication system mirrors Freedom of the Press Foundation's Secure Drop (<a href="https://securedrop.org/" rel="noreferrer noopener">https://securedrop.org/</a>), in that potential bug reporters receive a code phrase of six random words from the EFF’s New Wordlists for Random Passphrases. (<a href="https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases" rel="noreferrer noopener">https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases</a>)</p>
<p><figure role="group" class="align-center">
<img alt="An example of the code phrase creation screen. A purple bar with the Tor Logo at the top, followed by instructions to create a code phrase. At the bottom is a list of six random words in purple." src="/static/images/blog/inline-images/02 - Generate Identifier.png" />
<figcaption><em>(An example of user identifier code phrase generation.)</em></figcaption>
</figure>
</p>
<p>Once the user approves their code phrase, they are redirected to a bookmarkable landing page that allows them to browse and search projects and issues <i>or</i> instantly create their first issue/note using GitLab Flavored Markdown (<a href="https://docs.gitlab.com/ee/user/markdown.html" rel="noreferrer noopener">https://docs.gitlab.com/ee/user/markdown.html</a>) </p>
<p><figure role="group" class="align-center">
<img alt="A view of the user landing page. The top displays the code phrase that was created by the user. This is followed by a list of actions the user can take and lists of items the user has already created, including their status (pending or approved.)" src="/static/images/blog/inline-images/04 - User Landing.png" />
<figcaption><em>(A sample user landing view.)</em></figcaption>
</figure>
</p>
<p>As they navigate through the system, their user identifier code phrase is passed forward via an arg/kwarg in the URL, which is checked against an authenticator that determines it meets all the parameters for a user identifier (e.g., approved words only, right number of words, etc.).</p>
<p>Once a contribution is made, it’s saved in the database for moderation. Users can return to their landing page at any time, either via bookmark or by manually entering their code-phrase into a ‘login’ screen, and make new contributions or check the status of their pending items.</p>
<p><figure role="group" class="align-center">
<img alt="A login screen showing six fields, one for each word of the code phrase." src="/static/images/blog/inline-images/03 - Login With Previous.png" />
<figcaption><em>(The login view for returning users that do not want to use bookmarks.)</em></figcaption>
</figure>
</p>
<p><strong>Tor-Flavored, Data-Packed, Familiar Project and Issue Views:</strong>   </p>
<p>Project and issue templates are laid out to mirror a repo’s gitlab instance for a more familiar user experience, and styled via Bootstrap and the Tor Project Styleguide’s CSS files (<a href="https://styleguide.torproject.org/" rel="noreferrer noopener">https://styleguide.torproject.org/</a>) to maintain a strong visual identity and build user trust. Additionally, views display a project or issue’s GitLab milestones, labels, and notes, as well as a link directly to the relevant listing on GitLab.</p>
<p><figure role="group" class="align-center">
<img alt="A screen showing a list of projects that use the Anon-Ticket project. Each project has a link that says &quot;Link on Gitlab.&quot;" src="/static/images/blog/inline-images/05 - Sample Project List.png" />
<figcaption><em>(A project list view.)</em></figcaption>
</figure>
<figure role="group" class="align-center">
<img alt="A detail view of the snowflake project, with open issues at the top and closed issues at the bottom. A link near the top advises the user they can go back to their landing page or create a new issue." src="/static/images/blog/inline-images/06 - Project Detail View.png" />
<figcaption><em>(Example of a Project Detail View - This one for the Snowflake repo.)</em></figcaption>
</figure>
<figure role="group" class="align-center">
<img alt="An example of a closed issue view. The top shows details about the issue from gitlab, including the namespace, issue number, assignee, milestones, labels, etc. This is followed by a detail summary and a list of project notes." src="/static/images/blog/inline-images/07 - Issue Detail View.png" />
<figcaption><em>(Example of an issue detail view.)</em></figcaption>
</figure>
</p>
<p><strong>Feature-Rich Bulk Moderation:</strong></p>
<p>The Anonymous Ticket Portal leverages Django’s robust User and Group system to manage Moderator permissions. </p>
<p>Logged-in moderators have access to a feature-rich view that includes project/issue details, creation timestamps, linked User Identifier code phrases, and the option to bulk approve or reject a theoretically unlimited number of pending items at once.</p>
<p><figure role="group" class="align-center">
<img alt="A screenshot of the moderator view, showing sample notes and issues, with dropdowns for status or to update. Buttons at the bottom allow the moderators to save all changes, change password, or logout." src="/static/images/blog/inline-images/11 - Moderator View.png" />
<figcaption><em>(An example of the moderator portal - the status on many items can be set at once, or moderators can update the details of a single item.)</em></figcaption>
</figure>
</p>
<p>Additionally, each object has an “update view,” allowing moderators to tweak descriptions/content as necessary (e.g., for errors or unclear language), and add moderator-only comments that cannot be viewed by the user.</p>
<p><figure role="group" class="align-center">
<img alt="A screenshot of the view to update a moderator note, displaying a form with fields that the moderator can update, such as title, body, reviewer status. Moderators can also a mod-specific comment." src="/static/images/blog/inline-images/12 - Moderator Note Update.png" />
<figcaption><em>(A screenshot of the the moderator view for updating an issue. Note the ability to add moderator only comments.)</em></figcaption>
</figure>
</p>
<p><strong>Super-Powered SuperUser</strong></p>
<p>By leveraging the python-gitlab package and custom save definitions, a new project can be added to the Anonymous Ticket Portal using only a single piece of data--the project’s valid GitLab ID number. All details about the project (such as description, web url, name, groups, and namespace) are instantly fetched from GitLab upon project save--and can be updated by simply resaving the project.</p>
<p>Additionally, groups are created and updated programmatically via a custom BaseCommand on the command line, which increases consistency in both usage and with testing.</p>
<p><strong>Try It Out!</strong></p>
<p>A test instance of this project is currently live at <a href="https://anonticket.onionize.space/" rel="noreferrer noopener">https://anonticket.onionize.space/</a>, or you can see the repo itself at <a href="https://gitlab.torproject.org/tpo/tpa/anon_ticket" rel="noreferrer noopener">https://gitlab.torproject.org/tpo/tpa/anon_ticket</a>.</p>
<p>The following repos are currently set up to take anonymous issue reporting through the Anonymous Ticket Portal:</p>
<ul>
<li>The Tor Project / Core / Tor</li>
<li>The Tor Project / Applications / Tor Browser</li>
<li>The Tor Project / Anti-censorship / Pluggable Transports / Snowflake</li>
<li>The Tor Project / Anti-censorship / Pluggable Transports / Snowflake WebExtension</li>
<li>George Kadianakis / onionbalance</li>
<li>The Tor Project / TPA / Anonymous Ticket Portal (This repo!)</li>
</ul>
<p>Additionally, we are currently looking for volunteers to add their Tor GitLab repo as a test project for the Anonymous Ticket Portal and try out being a moderator. If you are part of the mailing list that received an email about this project, please consider volunteering your repo; your feedback will allow us to make this project as effective and user-friendly as possible for Tor users, developers, and moderators.</p>
<p>You can also (and are highly encouraged to) submit issues and notes on the project itself to GitLab via the the Anonymous Ticket Portal (<a href="https://anonticket.onionize.space/" rel="noreferrer noopener">https://anonticket.onionize.space/</a>).</p>
<p><strong>Planned Improvements</strong></p>
<p>We have a number of planned features rolling out over the next few weeks, including:</p>
<ul>
<li>Launching as an onion service.</li>
<li>The ability to create GitLab account requests with the intention of eventually replacing the TicketLobby. Users will be able to link their GitLab account request to their user identifier <i>if </i>they want the ability to check the status of their request in the web portal (thus lowering demands on moderators), but will <i>also </i>be able to create GitLab Account Requests without being logged in to the Anonymous Ticket Portal system.</li>
<li>Additional security features, such as improved rate-limiting, etc.</li>
</ul>
<p> </p>

