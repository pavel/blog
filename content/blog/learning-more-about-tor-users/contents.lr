title: Learning more about our users
---
pub_date: 2021-02-24
---
author: duncan
---
tags:

research
UX
---
categories:

research
usability
---
summary: We invite you to give your feedback in a new user survey.
---
_html_body:

<p>At the Tor Project we practice user-centered design. This means we put our users at the heart of our development process, making a conscious effort to understand the contexts in which people use our tools and paying particular attention to the bumps they encounter along the way.</p>
<p>Many digital product companies rely heavily on data gathered from invasive tracking scripts to better understand their users’ behavior, further fueling the surveillance economy. However that’s not how we do things at Tor – instead, we aim to conduct research that <a href="https://blog.torproject.org/strength-numbers-usable-tools-dont-need-be-invasive">respects the basic principles of privacy and consent</a>.</p>
<p>Prior to 2020, Tor Project contributors and local volunteers traveled to meet our users in their own communities across the world, with a <a href="https://blog.torproject.org/reaching-people-where-they-are">special emphasis on the global south</a>. Now, due to the challenges of the COVID-19 pandemic, much of this research has moved online – and we’re hoping to augment our qualitative interviews and training sessions with larger-scale quantitative research in a way that continues to respect our users’ privacy.</p>
<p><img alt="The Tor Project team and friends of Tor in Stockholm, 2019" src="/static/images/blog/inline-images/survey-blog-stockholm.jpg" /></p>
<h2>Tor Browser User Survey</h2>
<p>At our <a href="https://blog.torproject.org/reflections-our-stockholm-all-hands">Stockholm All Hands meeting in July 2019</a> we circulated a pilot paper survey among attendees (as in the kind you fill out with a pen and drop into a box) to learn more about their experience using one of our tools in particular: Tor Browser. Although the sample size was intentionally small and heavily skewed towards internal contributors and friends of Tor, it nonetheless gave us a clear indication of the pain points (i.e. frustrations) many of our users encounter while browsing.</p>
<p>Now we’re ready to open up this research to all Tor Browser users in the form of the Tor Browser User Survey – and would like to invite you to give us your feedback:</p>
<p>› <a href="https://survey.torproject.org/index.php/217469?lang=en">Launch the Tor Browser User Survey</a></p>
<p>This survey is also available as a .onion, which can be opened directly in Tor Browser, providing a greater level of security and privacy for survey participants:</p>
<p>› <a href="http://bogdyardcfurxcle.onion/index.php/217469?lang=en">Open the Tor Browser survey as a .onion</a></p>
<h2>Snowflake Client User Survey</h2>
<p>For those who aren’t familiar, <a href="https://snowflake.torproject.org/">Snowflake</a> is an in-development Pluggable Transport <a href="https://support.torproject.org/censorship/what-is-snowflake/">that’s designed to defeat internet censorship</a>, available for testing in Tor Browser Alpha. You can find out more about <a href="https://support.torproject.org/censorship/how-can-i-use-snowflake/">how to use Snowflake to bypass censorship</a> and how to <a href="https://support.torproject.org/censorship/how-to-help-running-snowflake/">install the Snowflake extension to help users in censored networks</a> on our Support portal.</p>
<p>We’ve been steadily increasing the number of users testing Snowflake over the past 6 months as we continue to scale up for a future stable release, and alongside the Tor Browser User Survey we’d like to invite Snowflake users to feed back on their browsing experience and connection quality while using it as a client:</p>
<p>› <a href="https://survey.torproject.org/index.php/491436?lang=en">Launch the Snowflake Client User Survey</a></p>
<p>As with the Tor Browser User Survey, the Snowflake Client User Survey is also available as a .onion to help safeguard your privacy while responding:</p>
<p>› <a href="http://bogdyardcfurxcle.onion/index.php/491436?lang=en">Open the Snowflake survey as a .onion</a></p>
<p><strong>Please note</strong>: for your personal safety we recommend high-risk individuals do not include any personally identifiable information. As such, certain questions have been marked as optional or include the option to respond “Prefer not to disclose” where appropriate.</p>
<h2>Get involved in open user research for Tor</h2>
<p>If you’d like to take a step further and volunteer to run an online research session in your own community we host an open call for user research at our monthly User Experience (UX) Team meetings. To get advance notice of the next UX Team meeting <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/ux">subscribe to the UX mailing list</a> and we’ll send you a reminder a few days before it happens. If you want to be extra-prepared, we recommend <a href="https://community.torproject.org/user-research/">visiting the Tor Project’s Community portal</a> to get up to speed with our previous work and research methods too.</p>

