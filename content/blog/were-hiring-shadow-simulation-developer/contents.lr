title: We're Hiring a Shadow Simulation Developer
---
pub_date: 2019-10-21
---
author: steph
---
tags:

jobs
developer
---
categories: jobs
---
summary: We are seeking an experienced programmer to help us develop cutting-edge network simulation / emulation software. This person will be responsible for the implementation, documentation, and testing of software to support research into privacy-enhancing technologies.
---
_html_body:

<p>We are seeking an experienced programmer to help us develop cutting-edge network simulation / emulation software. This person will be responsible for the implementation, documentation, and testing of software to support research into privacy-enhancing technologies. In particular, this person will be contributing to software that constructs large, realistic, high fidelity simulations of anonymity networks, allowing other researchers to run existing software (e.g., Tor) on top of a virtualized network. As such, this person should be comfortable working with established codebases (github.com/shadow) and incrementally improving them through modular design.</p>
<p>The ideal candidate will have significant practical programming skills, specifically, expertise in parallel program design and development.</p>
<p>For programming experience we seek a candidate with demonstrated ability to write correct, maintainable code in both Python and C. Experience with Rust, or a strong desire to learn to program in Rust, is preferred. In addition to high familiarity with parallel programming, the candidate will possess knowledge of the Linux operating system, networking, and algorithms at the BS level or higher. The candidate should be able to work both independently and as part of a small team, with strong communication skills and the ability to read and understand research papers and other technical documents.</p>
<p>The person in this position will work as part of a small cross-organizational development team, along with another simulation developer working in Micah Sherr's SecurityLab at Georgetown University. Rob Jansen at the U.S. Naval Research Laboratory will oversee and lead the team, however, the person in this position is integrated into the Tor Project Community through working with its Network Team.</p>
<p>The ideal candidate will be based in the Washington DC area (in which case we will try to get you a desk at Georgetown University). However, we will be considering remote candidates.</p>
<p>This position has full-time funding for three years, with the possibility to continue in another role thereafter, depending on organizational finances.</p>
<p><a href="https://www.torproject.org/about/jobs/shadow-simulation-developer/">Read the listing for more info and how to apply</a>.</p>

