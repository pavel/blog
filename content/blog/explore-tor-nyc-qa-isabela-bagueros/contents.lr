title: [Explore Tor, NYC!] Q&A with Isabela Bagueros
---
pub_date: 2018-07-30
---
author: steph
---
tags:

nyc
meetup
---
categories: community
---
summary:

This installment will feature a Q&A with our incoming Executive Director, Isabela Bagueros. We'll start with a brief introduction on Tor, then Isa will highlight a few things we've been up to and give an overview of her vision for the organization as she prepares to become the Tor Project's ED at the end of this year. 
---
_html_body:

<p>The Tor community is a global group that mostly convenes online. <a href="https://blog.torproject.org/explore-tor-new-york-city-new-meetup-starting-dec-7">Last year</a>, we opted to change that in NYC by starting regular meetups.</p>
<div id="magicdomid6"><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s">Join us for our next <i>Explore Tor, NYC! </i>meetup, happening on Thursday, August 2nd at 6:45PM at NYU Tandon in Brooklyn.</span></div>
<div> </div>
<div><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s">This installment will feature a Q&amp;A with our </span><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s"><a href="https://blog.torproject.org/announcing-tors-next-executive-director-isabela-bagueros ">incoming Executive Director, Isabela Bagueros</a>. We'll start with a brief introduction on Tor, then Isa will highlight a few things we've been up to and give an overview of her vision for the organization as she prepares to become the Tor Project's ED at the end of this year. </span></div>
<div id="magicdomid7"> </div>
<div id="magicdomid8"><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s">There will be ample time for questions afterwards, so whether you're a newcomer, are interested in one specific aspect of Tor, or are curious about Tor's future, this meetup is for you. </span></div>
<div id="magicdomid9"> </div>
<div id="magicdomid10"><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s">There is no admission fee. We respect the privacy of attendees, so there is no RSVP required, and we ask that everyone refrain from taking photographs without explicit permission of those pictured. </span></div>
<div id="magicdomid11"> </div>
<div id="magicdomid12"><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s">Join the</span><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s"> <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/regional-nyc">regional-nyc</a> mailing list to get notifications of upcoming NYC events in your inbox and stay connected with the locals online, too.</span></div>
<div id="magicdomid13"> </div>
<div id="magicdomid14"><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s"><a href="https://engineering.nyu.edu/research-innovation/makerspace ">MakerEvent Space </a>(adjacent to Makerspace) </span></div>
<div id="magicdomid16"><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s">Rogers Hall, Room 118</span></div>
<div id="magicdomid17"><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s">6 MetroTech Center </span></div>
<div id="magicdomid18"><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s">Brooklyn, NY 11201</span></div>
<div id="magicdomid19"> </div>
<div id="magicdomid20"><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s">We’ll gather at Circa Brewing Co, 141 Lawrence St, Brooklyn, NY 11201 after the event to continue our dialogue.</span></div>
<div id="magicdomid21"> </div>
<div id="magicdomid22"><span class="author-a-z90zz90z8z76zmhz65zz74zw0z122zz71zrg8s">See you there. </span></div>
<div id="magicdomid23"> </div>

---
_comments:

<a id="comment-276273"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276273" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 30, 2018</p>
    </div>
    <a href="#comment-276273">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276273" class="permalink" rel="bookmark">Can&#039;t attend (not in America…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can't attend (not in America thankfully) but Isabela is truly the best qualified person to take on Shari's leadership, wish her and the Tor Project all that is best</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276295"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276295" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Forest (not verified)</span> said:</p>
      <p class="date-time">August 02, 2018</p>
    </div>
    <a href="#comment-276295">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276295" class="permalink" rel="bookmark">OMW</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>OMW</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276382"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276382" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>xavierbad (not verified)</span> said:</p>
      <p class="date-time">August 16, 2018</p>
    </div>
    <a href="#comment-276382">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276382" class="permalink" rel="bookmark">How do I browse the dark web…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How do I browse the dark web. I'm new</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-276445"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276445" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 21, 2018</p>
    </div>
    <a href="#comment-276445">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276445" class="permalink" rel="bookmark">Would it be inappropriate…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Would it be inappropriate for those not in NYC to request a summary of what transpired?</p>
</div>
  </div>
</article>
<!-- Comment END -->
