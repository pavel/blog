title: New Alpha Release: Tor Browser 11.5a13 (Android, Windows, macOS, Linux)
---
pub_date: 2022-07-02
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5a13 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5a13 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5a13/).

Tor Browser 11.5a13 updates Firefox on Windows, macOS, and Linux to 91.10.0esr.

We use the opportunity as well to update various other components of Tor Browser:
- NoScript 11.4.6
- Tor Launcher 0.2.36
- Tor 0.4.7.8

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-21/) to Firefox.

The full changelog since [Tor Browser 11.5a12](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master) is:

- All Platforms
 - Update Tor to 0.4.7.8
 - Update NoScript to 11.4.6
 - Update translations
- Windows + OS X + Linux
  - Update Firefox to 91.10.0esr
  - Update Tor-Launcher to 0.2.36
  - [Bug tor-browser#11698](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/11698): Incorporate Tor Browser Manual pages into Tor Browser
  - [Bug tor-browser#40458](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40458): Implement about:rulesets https-everywhere replacement
  - [Bug tor-browser-build#40527](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40527): Remove https-everywhere from tor-browser alpha desktop
  - [Bug tor-browser#40971](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40971): TB Alpha desktop minor issue Help button is not working
  - [Bug tor-browser#41023](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41023): Update manual URLs
- Linux
  - [Bug tor-browser#41015](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41015): Add --name parameter to correctly setup WM_CLASS when running as native Wayland client
- Build System
  - All Platforms
    - [Bug tor-browser-build#40288](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40288): Bump mmdebstrap version to 0.8.6
    - [Bug tor-browser-build#40426](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40426): Update Ubuntu base image to 22.04
    - [Bug tor-browser-build#40497](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40497): Check that directory does not exist before starting macOS signing
    - [Bug tor-browser-build#40508](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40508): ChangeLog in master is missing 11.0.x changelog stanzas
    - [Bug tor-browser-build#40516](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40516): Remove aguestuser from tb_builders and torbutton.gpg
    - [Bug tor-browser-build#40519](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40519): Add Alexis' latest PGP key to https-everywhere key ring
    - [Bug tor-browser-build#40523](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40523): Add tor-announce to Release Prep template
  - Android
    - Update Go to 1.18.3
  - Windows + OS X + Linux
    - Update Go to 1.17.10
    - [Bug tor-browser-build#34451](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/34451): Include Tor Browser Manual in packages during build
    - [Bug tor-browser-build#40525](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40525): Update the mozconfig for tor-browser-91.9-11.5-2