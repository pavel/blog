title: Tor 0.3.4.4-rc is released!
---
pub_date: 2018-07-09
---
author: nickm
---
_html_body:

<p>Hi!  There's a new release candidate available for download.  If you build Tor from source, you can download the source code for 0.3.4.4-rc from the usual place on the website.  Packages should be available over the coming weeks, with a new alpha Tor Browser release likely some time in <s>Muly</s>July.</p>
<p>Remember, this is a release candidate: you should only run this if you'd like to find and report more bugs than usual.</p>
<p> </p>
<p>Tor 0.3.4.4-rc fixes several small compilation, portability, and correctness issues in previous versions of Tor. This version is a release candidate: if no serious bugs are found, we expect that the stable 0.3.4 release will be (almost) the same as this release.</p>
<h2>Changes in version 0.3.4.4-rc - 2018-07-09</h2>
<ul>
<li>Minor features (compilation):
<ul>
<li>When building Tor, prefer to use Python 3 over Python 2, and more recent (contemplated) versions over older ones. Closes ticket <a href="https://bugs.torproject.org/26372">26372</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the July 3 2018 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/26674">26674</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (Rust, portability):
<ul>
<li>Rust cross-compilation is now supported. Closes ticket <a href="https://bugs.torproject.org/25895">25895</a>.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Fix a compilation warning on some versions of GCC when building code that calls routerinfo_get_my_routerinfo() twice, assuming that the second call will succeed if the first one did. Fixes bug <a href="https://bugs.torproject.org/26269">26269</a>; bugfix on 0.2.8.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (control port):
<ul>
<li>Report the port correctly when a port is configured to bind to "auto". Fixes bug <a href="https://bugs.torproject.org/26568">26568</a>; bugfix on 0.3.4.1-alpha.</li>
<li>Handle the HSADDRESS= argument to the HSPOST command properly. (Previously, this argument was misparsed and thus ignored.) Fixes bug <a href="https://bugs.torproject.org/26523">26523</a>; bugfix on 0.3.3.1-alpha. Patch by "akwizgran".</li>
</ul>
</li>
<li>Minor bugfixes (correctness, flow control):
<ul>
<li>Upon receiving a stream-level SENDME cell, verify that our window has not grown too large. Fixes bug <a href="https://bugs.torproject.org/26214">26214</a>; bugfix on svn r54 (pre-0.0.1).</li>
</ul>
</li>
<li>Minor bugfixes (memory, correctness):
<ul>
<li>Fix a number of small memory leaks identified by coverity. Fixes bug <a href="https://bugs.torproject.org/26467">26467</a>; bugfix on numerous Tor versions.</li>
</ul>
</li>
<li>Minor bugfixes (testing, compatibility):
<ul>
<li>When running the hs_ntor_ref.py test, make sure only to pass strings (rather than "bytes" objects) to the Python subprocess module. Python 3 on Windows seems to require this. Fixes bug <a href="https://bugs.torproject.org/26535">26535</a>; bugfix on 0.3.1.1-alpha.</li>
<li>When running the ntor_ref.py test, make sure only to pass strings (rather than "bytes" objects) to the Python subprocess module. Python 3 on Windows seems to require this. Fixes bug <a href="https://bugs.torproject.org/26535">26535</a>; bugfix on 0.2.5.5-alpha.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-276102"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276102" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">July 09, 2018</p>
    </div>
    <a href="#comment-276102">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276102" class="permalink" rel="bookmark">...Browser release likely…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>...Browser release likely some time in <b>Muly</b></p></blockquote>
<p>Should be  July</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-276161"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-276161" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">July 18, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-276102" class="permalink" rel="bookmark">...Browser release likely…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-276161">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-276161" class="permalink" rel="bookmark">You are right.
Let&#039;s hope…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You are right.</p>
<p>Let's hope it sticks to July!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
