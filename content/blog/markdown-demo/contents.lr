title: Markdown Demo
---
_discoverable: no
---
pub_date: 2021-11-01
---
author: lavamind
---
image: /static/images/blog/ferris-and-onions_0.jpg
---
summary:
Testing the markdown.
---
body:

Lektor uses [mistune](https://github.com/lepture/mistune) (version 0.8 as of now) to parse markdown to HTML.

# h1 Heading 8-)
## h2 Heading
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading

## Horizontal Rules

___

----

***


## Emphasis

**This is bold text**

__This is bold text__

*This is italic text*

_This is italic text_

~~Strikethrough~~


## Blockquotes


> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.


## Lists

Unordered

+ Create a list by starting a line with `+`, `-`, or `*`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!

Ordered

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa


1. You can use sequential numbers...
1. ...or keep all the numbers as `1.`

## Code

Inline `code`

Indented code

    // Some comments
    line 1 of code
    line 2 of code
    line 3 of code


Block code "fences"

```
Sample text here...
```

Code block with long lines

```
|2019-11-16|2019-11-23|ir||Internet blackout in Iran.|[report](https://ooni.org/post/2019-iran-internet-blackout/) [NTC thread](https://ntc.party/t/a-new-kind-of-censoship-in-iran/237)||
|2017-06-07|2017-12-14|br|meek|Sustained increase of meek users in Brazil, similar to the one that took place between 2016-07-21 and 2017-03-03.|[graph](https://metrics.torproject.org/userstats-bridge-combined.html?start=2016-06-01&end=2018-01-15&country=br)|X|
```

Syntax highlighting

``` js
var foo = function (bar) {
  return bar++;
};

console.log(foo(5));
```

## Tables

| Option | Description |
| ------ | ----------- |
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

Right aligned columns

| Option | Description |
| ------:| -----------:|
| data   | path to data files to supply the data that will be passed into templates. |
| engine | engine to be used for processing templates. Handlebars is the default. |
| ext    | extension to be used for dest files. |

Multiline cells

| Option | Description |
| ------:| -----------:|
| data   | path to data files to supply<br>the data that will be passed into templates. |
| engine | engine to be used for processing templates.<br>Handlebars is the default. |
| ext    | extension to be used for dest files. |


## Links

[link text](http://dev.nodeca.com)

[link with title](http://nodeca.github.io/pica/demo/ "title text!")

Autoconverted link https://github.com/nodeca/pica (enable linkify to see)

## Images

Using markdown (original size, not possible to resize)

![Tor metrics](../static/images/blog/inline-images/userstats-relay-country-lk-2018-02-01-2018-04-15-off.png)

Using HTML and resized

<img alt="Alttext" src="../static/images/blog/inline-images/userstats-relay-country-lk-2018-02-01-2018-04-15-off.png" height="240" />

Like links, Images also have a footnote style syntax

![Donate button][donate]

With a reference later in the document defining the URL location:

`[donate]: ../static/images/blog/inline-images/tor-donate-green-button2020_0.png  "Donate button"`

[donate]: ../static/images/blog/inline-images/tor-donate-green-button2020_0.png  "Donate button"
