title: Vidalia 0.2.4 Released
---
pub_date: 2009-09-07
---
author: phobos
---
tags:

polipo
drag and drop OS X install
msi installer
vidalia release
updated packages
---
categories: releases
---
_html_body:

<p>Vidalia 0.2.4 is released.  The OS X -alpha bundles are updated to fix a bug in the default "bootstrap" vidalia.conf file that pointed to a non-existent Polipo configuration file, causing Polipo to fail on startup.</p>

<p>The Changelog for this release is:</p>

<ul>
<li>Split the message log into "Basic" and "Advanced" views. The<br />
    "Advanced" view contains standard log messages from Tor, while the new<br />
    experimental "Basic" view displays status events received from Tor.<br />
    (Ticket #265)</li>
<li>Apply an application-global stylesheet on OS X that forces all tree<br />
    widgets in Vidalia to use the 12pt font recommended by Apple's human<br />
    interface guidelines.</li>
<li>Add an OSX_FORCE_32BIT CMake option that can be used to force a 32-bit<br />
    build on Mac OS X versions that default to 64-bit builds (e.g., Snow<br />
    Leopard), if only 32-bit versions of the Qt libraries are available.</li>
<li>Fix a bug introduced in 0.2.3 that prevented Vidalia from correctly<br />
    responding to ADDRMAP events from Tor. The result was that users would<br />
    sometimes see IP addresses in the connection list shown under the network<br />
    map rather than hostnames.</li>
<li>Fix a bug in the default "bootstrap" vidalia.conf file included in the<br />
    OS X drag-and-drop bundles that pointed to a non-existent Polipo<br />
    configuration file, causing Polipo to fail on startup.</li>
</ul>

---
_comments:

<a id="comment-2458"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2458" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 08, 2009</p>
    </div>
    <a href="#comment-2458">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2458" class="permalink" rel="bookmark">after upgrading to snow</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>after upgrading to snow leopard, the torbutton for firefox doesnt seem to connect to Vidalia properly.  vidalia itself still works, but firefox does not work with vidalia anymore, any solutions?  Thanks a lot.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2459"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2459" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 08, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2458" class="permalink" rel="bookmark">after upgrading to snow</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2459">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2459" class="permalink" rel="bookmark">re: snow leopard</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="http://blog.torproject.org/blog/major-changes-os-x-vidalia-bundle-0221alpha" rel="nofollow">http://blog.torproject.org/blog/major-changes-os-x-vidalia-bundle-0221a…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2468"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2468" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>david (not verified)</span> said:</p>
      <p class="date-time">September 10, 2009</p>
    </div>
    <a href="#comment-2468">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2468" class="permalink" rel="bookmark">firefox fix</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks phobos for the link.  I will give it a try.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2551"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2551" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 18, 2009</p>
    </div>
    <a href="#comment-2551">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2551" class="permalink" rel="bookmark">Will the documentation for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Will the documentation for Mac be updated to reflect Polipo, rather than Privoxy, any time soon?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2577"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2577" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">September 20, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2551" class="permalink" rel="bookmark">Will the documentation for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2577">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2577" class="permalink" rel="bookmark">re: Will the documentation for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, it's on my todo list this week for the -alpha users.  The -stable is still privoxy.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2582"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2582" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>behrouz (not verified)</span> said:</p>
      <p class="date-time">September 21, 2009</p>
    </div>
    <a href="#comment-2582">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2582" class="permalink" rel="bookmark">hello       
please send me</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hello       </p>
<p>please send me vidalia tor last version  </p>
<p>tanks  behrouz iran</p>
</div>
  </div>
</article>
<!-- Comment END -->
