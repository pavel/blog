title: Using Tor hidden services for good
---
pub_date: 2012-01-08
---
author: phobos
---
tags:

hidden services
arab spring
positive uses
globaleaks
controversial content hosting
resilience
---
categories:

community
onion services
---
_html_body:

<p>Getting good stories for Tor successes is tricky, because if Tor is doing its job, nobody notices. I know a lot of people who have really interesting Tor success stories and have no interest in telling the world who they are and how they managed (until that moment when everybody is reading about them, that is) to stay safe.</p>

<p>Still, there are a bunch of other stories out there that haven't been documented as well. For example, I really like Nasser's story about his experiences in Mauritania:<br />
<a href="http://www.technologyreview.com/computing/22427/page4/" rel="nofollow">http://www.technologyreview.com/computing/22427/page4/</a></p>

<p>Hidden services have gotten less broad attention from the Tor user base, since most people who install Tor have a website in mind like twitter or indymedia that they want to visit safely. Some good use cases that we've seen for hidden services in particular include:</p>

<p>- I know people (for example, in countries that have been undergoing revolutions lately) who run popular blogs but their blogs kept getting knocked offline by state-sponsored jerks. The common blogging software they used (like Wordpress) couldn't stand up to the ddos attacks and breakins. The solution was to split the blog into a public side, which is static html and has no logins, and a private side for posting, which is only reachable over a Tor hidden service. Now their blog works again and they're reaching their audiences. And as a bonus, the nice fellow hosting the private side for them doesn't need to let people know where it is, and even if somebody figures it out, the nice fellow hosting it doesn't have any IP addresses to hand over or lose.</p>

<p>- Whistleblowing websites want to provide documents from a platform that is hard for upset corporations or governments to censor. See e.g. <a href="http://globaleaks.org/" rel="nofollow">http://globaleaks.org/</a></p>

<p>- Google for 'indymedia fbi seize'. When Indymedia offers a hidden service version of their website, censoring organizations don't know which data centers to bully into handing over the hardware.</p>

<p>- Data retention laws in Europe (and soon in the US too at this rate) threaten to make centralized chat networks vulnerable to social network analysis (step one, collect all the data; step two, get broken into by corporations, criminals, external governments, you name it; step three comes identity theft, stalking, targeted scam jobs, etc etc). What if you had a chat network where all the users were on hidden services by default? Now there's no easy central point to learn who's talking to who and when. Building one and making it usable turns out to be hard. But good thing we have this versatile tool here as a building block.</p>

<p>That's a start. It is certainly the case that we (Tor) spend most of our time making the technology better, and not so much of our time figuring out how to market it and change the world's perception on whether being safe online is worthwhile. Please help. :)</p>

<p>You might also like<br />
<a href="https://torproject.org/about/torusers.html.en" rel="nofollow">https://torproject.org/about/torusers.html.en</a><br />
<a href="https://blog.torproject.org/blog/we-need-your-good-tor-stories" rel="nofollow">https://blog.torproject.org/blog/we-need-your-good-tor-stories</a></p>

<p>This blog post was adapted from an email to tor-talk by Roger. See the original email at <a href="https://lists.torproject.org/pipermail/tor-talk/2011-November/021997.html" rel="nofollow">https://lists.torproject.org/pipermail/tor-talk/2011-November/021997.ht…</a></p>

---
_comments:

<a id="comment-13418"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13418" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 08, 2012</p>
    </div>
    <a href="#comment-13418">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13418" class="permalink" rel="bookmark">pls start a forum soon!!!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>pls start a forum soon!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13419"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13419" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 08, 2012</p>
    </div>
    <a href="#comment-13419">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13419" class="permalink" rel="bookmark">First, thank you all for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>First, thank you all for providing one of best proxy services TOR project that i am using for months.<br />
Second, I as one of users of this program have some suggestions:<br />
1. Its boring to download the whole program files each time a tiny update is added, particularly for those who use low speed internet access, downloading a 20 mg file just for some kb updates is panic. Best update technic is automatic update through the program itself without need of manually download a big file and update should be just for the new files updated not the whole program.<br />
2. The browser integrated is not powerful and famous as those Firefox, Chrome. Users should have an option to use other browsers as well. But TOR pops up Aurora when it stats and remain active until Aurora is open. If user shift to another browser and closes Aurora, then, TOR is shut down.<br />
Since I know TOR has been developed to facilitate users, so it would be better users can use their favorite browsers with TOR without encountering problem.<br />
Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-13526"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13526" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 15, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-13419" class="permalink" rel="bookmark">First, thank you all for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-13526">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13526" class="permalink" rel="bookmark">Aurora is an unbranded</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Aurora is an unbranded Firefox. The instance in the browser bundle also has some additional security patches. You can use Chrome/Chromium with Tor, just point Chrome's proxy settings at the SOCKS port of the running tor instance.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-13422"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13422" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 08, 2012</p>
    </div>
    <a href="#comment-13422">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13422" class="permalink" rel="bookmark">Dear Tor Users and Hidden</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><strong>Dear Tor Users and Hidden Service Fans :)</strong></p>
<p>Please consider using onioncat ( <a href="http://www.cypherpunk.at/onioncat" rel="nofollow">http://www.cypherpunk.at/onioncat</a> ).<br />
Onioncat provides a transparent IPv6 tunnel over Hidden Services, you could think<br />
of it as a point-to-multipoint VPN.</p>
<p>It is <strong>easy to set up</strong> ( follow the instructions on our website ) and you'll get a unique IPv6 address that is only valid within<br />
onioncat. All traffic you exchange over it will never leave the tor network as the packets are only routed between hidden services.</p>
<p>You may provide Web Services, eMail, DNS (although we are working on a better solution in respect to the latency) or whatever you want so that we all together grow an ecosystem of location hidden and easy to use services.</p>
<p>Get in touch with us if there are any questions on how to use onioncat,<br />
you'll find our details on the website mentioned above.</p>
<p>all the best,</p>
<p>creo</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-13438"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13438" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 08, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-13422" class="permalink" rel="bookmark">Dear Tor Users and Hidden</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-13438">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13438" class="permalink" rel="bookmark">Excellent work Creo &amp; team!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Excellent work Creo &amp; team! :)  Thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-13437"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13437" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 08, 2012</p>
    </div>
    <a href="#comment-13437">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13437" class="permalink" rel="bookmark">We have published this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We have published this article on our website in the hope to expand the chances you may get assistance.  You correctly identify the reason it's hard to get a 'real story that matters'; I could tell you many more than one, but people would have to be dead first.</p>
<p>To explain that in these paranoid times - I owe legal professional privilege and cannot even start to describe the circumstances, without it becoming obvious to whom!  Now I've told you that much, I cannot even tell you the website, either! LOL</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13447"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13447" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 09, 2012</p>
    </div>
    <a href="#comment-13447">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13447" class="permalink" rel="bookmark">&quot;Your comment has been</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Your comment has been queued for moderation by site administrators and will be published after approval.".....</p>
<p>Please do set up a forum!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13448"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13448" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 09, 2012</p>
    </div>
    <a href="#comment-13448">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13448" class="permalink" rel="bookmark">Why&#039;s the firefox browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why's the firefox browser not starting up when I click the Start Tor Browser... Vidalia says "connected to the tor network" but the browser does not pop up as usual?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13472"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13472" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 10, 2012</p>
    </div>
    <a href="#comment-13472">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13472" class="permalink" rel="bookmark">I agree with other posters</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I agree with other posters here that we need a forum for tor talk</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-13498"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13498" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 11, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-13472" class="permalink" rel="bookmark">I agree with other posters</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-13498">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13498" class="permalink" rel="bookmark">I agree too. I think there</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I agree too. I think there is no problem to add forum on Drupal, but acctualy there is a problem, who will manage and administrate it 24x7</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-13524"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13524" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 15, 2012</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-13498" class="permalink" rel="bookmark">I agree too. I think there</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-13524">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13524" class="permalink" rel="bookmark">no need to administrate it</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>no need to administrate it 24/7. Who administrates the blog? The same people who administrate the blog should be able to administrate the forum. The forum can have the same functions enabled: requiring anonymous posts to be verified before they are posted.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-13486"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13486" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 10, 2012</p>
    </div>
    <a href="#comment-13486">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13486" class="permalink" rel="bookmark">i downloaded new Tor Browser</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i downloaded new Tor Browser Bundles ,when i attempt to run it,an alert message occur:"Vidalia detected that the tor software exited unexpectedly.please check the message log for recent warning or error messages".i changed different version Tor browser bundles,but still can't resolve.so,can you help me ?thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13610"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13610" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 22, 2012</p>
    </div>
    <a href="#comment-13610">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13610" class="permalink" rel="bookmark">I believe that beside good</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I believe that beside good stories Tor developers need to offer to the people:<br />
1)forum, for discussion and technical help, million times I wanted to ask something and I must ask in some comment at topic of blog which has nothing with my question. You don't need crowd of people to administrate forum, better any (slow) help than no help. Now: Tor has no technical help section and that's big failure for any software. If you want people to use your software, you must have technical help. Good stories are not enough.<br />
2)better technical explanations/documentations, just read your instructions for tor hidden service, your explanation will be understood only by computer professionals, if ordinary person follow your instructions, he will never succeed to setup/install tor hidden service. </p>
<p>Beside this, tor developers need to make better advertising of tor (especially Tor Relays), so, we can get bigger Tor network. There are freelancers websites where people from India can be paid some small money to make articles about tor all over internet. I don't have debit/credit card so I can not employ people, and I am without job, so, I can't pay people to advertise Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13614"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13614" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 22, 2012</p>
    </div>
    <a href="#comment-13614">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13614" class="permalink" rel="bookmark">hello 
im unable to to view</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hello </p>
<p>im unable to to view chat when using  tor ,can any one suggest a solution please</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-13707"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-13707" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 27, 2012</p>
    </div>
    <a href="#comment-13707">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-13707" class="permalink" rel="bookmark">A forum is a very good idea</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>A forum is a very good idea I think. I was wondering if using print pdf addon with tor exposes my identity ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-14951"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-14951" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 10, 2012</p>
    </div>
    <a href="#comment-14951">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-14951" class="permalink" rel="bookmark">When downloading files, is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When downloading files, is that as safe as in will you still be anonymous while downloading files?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15473"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15473" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 08, 2012</p>
    </div>
    <a href="#comment-15473">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15473" class="permalink" rel="bookmark">The story about the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The story about the half-public, half-hidden blog was interesting, but without a technical explanation of how it was done and what software was used, the average "civilian," even someone with a general knowledge of computers and UNIX, can't easily duplicate it. And you don't really want to go asking others for help or assistance in setting it up, since that would compromise the whole idea of keeping parts of it secret.</p>
<p>So yes, there is a need for a forum or question and answer site or Wiki for people interested in using Tor to get some technical help.</p>
<p>I'd like to ask questions like:</p>
<p>-- Can you put hidden services on the same server as open services? Do you need to install two HTTP servers? Do you need two IP addresses?</p>
<p>-- Are hidden service URLs one-to-one to dotted-quad IP addresses? Or can you have more than one dot-onion URL per dotted-quad IP address if you specify it in the httpd.conf file? (If you can't separate this stuff out, you'd have to put all your hidden services under a single dot-onion URL, which would reveal that they are operated by the same person, not so good.)</p>
<p>The technical explanations of Tor hidden services on this website are inadequate. Fair enough. You guys are understaffed and very busy and doing a great job. So that is exactly why we need a forum. If someone else wants to set it up somewhere, let us know here in the comments!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15638"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15638" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 18, 2012</p>
    </div>
    <a href="#comment-15638">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15638" class="permalink" rel="bookmark">why not combine both between</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>why not combine both between tor+onion cat integrated inside tor bundle. that way can make us the user a lot of save from headache. now about globaleak.org is it truly anonymously for I had a greatest secret about to be unfold. This scandal is huge and ugly.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-15913"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-15913" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 02, 2012</p>
    </div>
    <a href="#comment-15913">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-15913" class="permalink" rel="bookmark">I want contact to some</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I want contact to some website with single IP in a cretain country evermore.<br />
How can I configure my sightly IP?<br />
Thank you</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-16276"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-16276" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 20, 2012</p>
    </div>
    <a href="#comment-16276">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-16276" class="permalink" rel="bookmark">I&#039;m not sure if this is the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm not sure if this is the correct place to post this question, but judging by some of the website links called when I visit certain websites, the site seems to know my geographical location. The IP address allocated by Tor is not in my country, and yet I receive ads specific to my country. Is there something I'm missing?</p>
<p>Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
