title: Tor 0.2.2.2-alpha released
---
pub_date: 2009-10-10
---
author: phobos
---
tags:

bug fixes
alpha release
performance improvements
---
categories: releases
---
_html_body:

<p>On September 21st, we released Tor version 0.2.2.2-alpha.</p>

<p><strong>Major features:</strong></p>

<ul>
<li>Tor now tracks how long it takes to build client-side circuits<br />
      over time, and adapts its timeout to local network performance.<br />
      Since a circuit that takes a long time to build will also provide<br />
      bad performance, we get significant latency improvements by<br />
      discarding the slowest 20% of circuits. Specifically, Tor creates<br />
      circuits more aggressively than usual until it has enough data<br />
      points for a good timeout estimate. Implements proposal 151.<br />
      We are especially looking for reports (good and bad) from users with<br />
      both EDGE and broadband connections that can move from broadband<br />
      to EDGE and find out if the build-time data in the .tor/state gets<br />
      reset without loss of Tor usability. You should also see a notice<br />
      log message telling you that Tor has reset its timeout.</li>
<li>Directory authorities can now vote on arbitary integer values as<br />
      part of the consensus process. This is designed to help set<br />
      network-wide parameters. Implements proposal 167.</li>
<li>Tor now reads the "circwindow" parameter out of the consensus,<br />
      and uses that value for its circuit package window rather than the<br />
      default of 1000 cells. Begins the implementation of proposal 168.</li>
</ul>

<p><strong>Major bugfixes:</strong></p>

<ul>
<li>Fix a remotely triggerable memory leak when a consensus document<br />
      contains more than one signature from the same voter. Bugfix on<br />
      0.2.0.3-alpha.</li>
</ul>

<p><strong>Minor bugfixes:</strong></p>

<ul>
<li>Fix an extremely rare infinite recursion bug that could occur if<br />
      we tried to log a message after shutting down the log subsystem.<br />
      Found by Matt Edman. Bugfix on 0.2.0.16-alpha.</li>
<li>Fix parsing for memory or time units given without a space between<br />
      the number and the unit. Bugfix on 0.2.2.1-alpha; fixes bug 1076.</li>
<li>A networkstatus vote must contain exactly one signature. Spec<br />
      conformance issue. Bugfix on 0.2.0.3-alpha.</li>
<li>Fix an obscure bug where hidden services on 64-bit big-endian<br />
      systems might mis-read the timestamp in v3 introduce cells, and<br />
      refuse to connect back to the client. Discovered by "rotor".<br />
      Bugfix on 0.2.1.6-alpha.</li>
<li>We were triggering a CLOCK_SKEW controller status event whenever<br />
      we connect via the v2 connection protocol to any relay that has<br />
      a wrong clock. Instead, we should only inform the controller when<br />
      it's a trusted authority that claims our clock is wrong. Bugfix<br />
      on 0.2.0.20-rc; starts to fix bug 1074. Reported by SwissTorExit.</li>
<li>We were telling the controller about CHECKING_REACHABILITY and<br />
      REACHABILITY_FAILED status events whenever we launch a testing<br />
      circuit or notice that one has failed. Instead, only tell the<br />
      controller when we want to inform the user of overall success or<br />
      overall failure. Bugfix on 0.1.2.6-alpha. Fixes bug 1075. Reported<br />
      by SwissTorExit.</li>
<li>Don't warn when we're using a circuit that ends with a node<br />
      excluded in ExcludeExitNodes, but the circuit is not used to access<br />
      the outside world. This should help fix bug 1090, but more problems<br />
      remain. Bugfix on 0.2.1.6-alpha.</li>
<li>Work around a small memory leak in some versions of OpenSSL that<br />
      stopped the memory used by the hostname TLS extension from being<br />
      freed.</li>
<li>Make our 'torify' script more portable; if we have only one of<br />
      'torsocks' or 'tsocks' installed, don't complain to the user;<br />
      and explain our warning about tsocks better.</li>
</ul>

<p><strong>Minor features:</strong></p>

<ul>
<li>Add a "getinfo status/accepted-server-descriptor" controller<br />
      command, which is the recommended way for controllers to learn<br />
      whether our server descriptor has been successfully received by at<br />
      least on directory authority. Un-recommend good-server-descriptor<br />
      getinfo and status events until we have a better design for them.</li>
<li>Update to the "September 4 2009" ip-to-country file.</li>
</ul>

---
_comments:

<a id="comment-2867"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2867" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 18, 2009</p>
    </div>
    <a href="#comment-2867">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2867" class="permalink" rel="bookmark">IT DOESNT work!with ff 3.5.3 fer.or 3.5.5 us vers.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>u know what would be in-GENIOUS!!!???? putting a SIMPLE,for newbies&amp;Dummies UNDERSTANDABLE,step-by-step introduction to your site even BETTER WITZ screenshots!!<br />
'cause when I tried to instal,act.,I installed TORbutton,my WHOLE i-net connection crushed!!!!!no browser would work !!!C'm on...Is it SO much more work to embedd an Step-by step introductionj,because i'm feeling alone&amp;hopeless noone who could help,only faq's that do not even scratch my problem....!!!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2868"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2868" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 18, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2867" class="permalink" rel="bookmark">IT DOESNT work!with ff 3.5.3 fer.or 3.5.5 us vers.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2868">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2868" class="permalink" rel="bookmark">If you don&#039;t have any basic</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you don't have any basic computer knowledge you shouldn't even attempt to use something as sophisticated as a proxy, that being said, TORButton works fine in the most recent FF version</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-2872"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2872" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">October 19, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2867" class="permalink" rel="bookmark">IT DOESNT work!with ff 3.5.3 fer.or 3.5.5 us vers.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2872">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2872" class="permalink" rel="bookmark">re: firefox 3.5.3 or 3.5.5</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Technicall, torbutton installs and works with FF 3.5.x.  You need to install Tor first, otherwise no, it's not going to work.  Torbutton requires Tor, it says so right on the mozilla add-ons page. </p>
<p>The issue with Firefox 3.5.x is that torbutton does not protect your anonymity as well as with 3.0.x.  There are a number of leaks with torbutton and firefox 3.5.x we're working to address.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
