title: Tails 1.1.1 is out
---
pub_date: 2014-09-02
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>Tails, The Amnesic Incognito Live System, version 1.1.1, is out.</p>

<p>All users must <a href="https://tails.boum.org/doc/first_steps/upgrade/" rel="nofollow">upgrade</a> as soon as possible: this release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_1.1/" rel="nofollow">numerous security issues</a>.</p>

<p><a href="https://tails.boum.org/download/" rel="nofollow">Download it now.</a></p>

<p><strong>Changes</strong></p>

<p>Notable user-visible changes include:</p>

<ul>
<li>Security fixes
<ul>
<li>Upgrade the web browser to 24.8.0esr-0+tails1~bpo70+1 (Firefox 24.8.0esr + Iceweasel patches + Torbrowser patches).</li>
<li>Add an I2P boot parameter. Without adding "i2p" to the kernel command line, I2P will not be accessible for the Live user. I2P was also upgraded to 0.9.14.1-1~deb7u+1, and stricter firewall rules are applied to it, among other security enhancements.</li>
<li>Upgrade Tor to 0.2.4.23-2~d70.wheezy+1 (fixes CVE-2014-5117).</li>
<li>Upgrade Linux to 3.14.15-2 (fixes CVE-2014-3534, CVE-2014-4667 and CVE-2014-4943).</li>
<li>Prevent dhclient from sending the hostname over the network (<a href="https://labs.riseup.net/code/issues/7688" rel="nofollow">ticket #7688</a>).</li>
<li>Override the hostname provided by the DHCP server (<a href="https://labs.riseup.net/code/issues/7769" rel="nofollow">ticket #7769</a>).</li>
</ul>
</li>
<li>Bugfixes
<ul>
<li>Don't ship OpenJDK 6: I2P prefers v7, and we don't need both (<a href="https://labs.riseup.net/code/issues/7807" rel="nofollow">ticket #7807</a>).</li>
<li>Prevent Tails Installer from updating the system partition properties on MBR partitions (<a href="https://labs.riseup.net/code/issues/7716" rel="nofollow">ticket #7716</a>).</li>
</ul>
</li>
<li>Minor improvements
<ul>
<li>Upgrade to Torbutton 1.6.12.1.</li>
<li>Install gnome-user-guide (<a href="https://labs.riseup.net/code/issues/7618" rel="nofollow">ticket #7618</a>).</li>
<li>Install cups-pk-helper (<a href="https://labs.riseup.net/code/issues/7636" rel="nofollow">ticket #7636</a>).</li>
<li>Update the SquashFS sort file, which should speed up boot from DVD (<a href="https://labs.riseup.net/code/issues/6372" rel="nofollow">ticket #6372</a>).</li>
<li>Compress the SquashFS more aggressively (<a href="https://labs.riseup.net/code/issues/7706" rel="nofollow">ticket #7706</a>) which should make the Tails ISO image smaller.</li>
</ul>
</li>
</ul>

<p>See the <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">online Changelog</a> for technical details.</p>

<p><strong>Known issues</strong></p>

<p><a href="https://tails.boum.org/support/known_issues/" rel="nofollow">Longstanding</a> known issues.</p>

<p><strong>I want to try it or to upgrade!</strong></p>

<p>Go to the <a href="https://tails.boum.org/download/" rel="nofollow">download</a> page.</p>

<p><strong>What's coming up?</strong></p>

<p>The next Tails release is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for October 14.</p>

<p>Have a look to our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>Do you want to help? There are many ways <a href="https://tails.boum.org/contribute/" rel="nofollow"><strong>you</strong> can contribute to Tails</a>. If you want to help, come talk to us!</p>

<p><strong>Support and feedback</strong></p>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

