title: Projects from Google Summer of Code 2022
---
author: gaba
---
pub_date: 2022-05-29
---
categories:
community
network
announcements
---
summary:

Tor Project is participating once again in Google Summer of Code in 2022. Starting in June we will have two OONI projects and one project for helping the Tor's network health. 

---
body:


We are happy that the Tor Project is again participating in [Google Summer of Code](https://opensource.googleblog.com/2022/05/GSoC-2022-Accepted-Contributor-Announced.html). Mentorship programs are very important for open source projects as they help us have one more way of bringing new contributors to our projects.  Starting in June this year we will have the [following mentors and their projects](https://gitlab.torproject.org/tpo/team/-/wikis/GSoC) working with a contributor from Google Summer of Code:

# [Tor Weather: Improving the Tor network](https://gitlab.torproject.org/tpo/team/-/wikis/GSoC#1-tor-weather) by Sarthik Gupta 

This project will bring back Tor Weather. It is an email notification service that relay operators can subscribe to and choose which notifications they want to receive about their relay.

- Mentors: Georg and Hiro
- Project: https://gitlab.torproject.org/tpo/team/-/wikis/GSoC#1-tor-weather

# [OONI Probe Network Experiments: Detecting TLS Middleboxes and Port-based Blocking](https://gitlab.torproject.org/tpo/team/-/wikis/GSoC#2-ooni-probe-network-experiments)

This project is about researching and developing new network experiments to be integrated inside the OONI Probe CLI. The contributor will write network measurements code and unit/integration tests for OONI's measurement engine.

- Mentor: Simone
- Project: https://gitlab.torproject.org/tpo/team/-/wikis/GSoC#2-ooni-probe-network-experiments

# [Tor Project: OONI Explorer & Design System Improvements](https://gitlab.torproject.org/tpo/team/-/wikis/GSoC#4-ooni-explorer-improvements)

This project will improve OONI Explorer. Specifically improvements on the OONI Explorer country pages to enrich them with data coming from new experiments and make use of new backend API calls to display more rich information about blocking. Moreover this work will entail modernizing the code base, improving the user interface, fixing usability issues, and writing more unit/integration tests.

- Mentor: Arturo
- Project: https://gitlab.torproject.org/tpo/team/-/wikis/GSoC#4-ooni-explorer-improvements

The contributors will start working with [the Tor Project](https://www.torproject.org) and [OONI](https://ooni.org) in the next few weeks. 
