title: Visual overview of the Tor network
---
pub_date: 2013-01-28
---
author: karsten
---
tags:

support
network
overview
introduction
---
categories:

community
network
---
_html_body:

<p>If you were to give a non-technical person a brief overview of the Tor network, how would you begin?  And if you had a picture or diagram to assist you, how would that look like?</p>

<p>We're looking for better visualizations of the Tor network as introductory material.  Most people already know EFF's visualizations from <a href="https://www.torproject.org/about/overview.html.en#thesolution" rel="nofollow">Tor's Overview page</a>.  Recently, an <a href="https://lists.torproject.org/pipermail/tor-talk/2013-January/027105.html" rel="nofollow">Italian hack meeting</a> came up with a fun picture of how to imagine a Tor circuit.  A discussion among Tor developers brought up an ugly, but potentially useful <a href="https://people.torproject.org/~karsten/tor-network-road-traffic.jpg" rel="nofollow">analogy with road traffic</a>.</p>

<p>Want to help make these visualizations better or suggest your own?  Prettier drawings that we can actually show to the world are as useful as content-wise improvements what to add or leave out from these visualizations.  Simply leave your ideas or links in the comments.  Thanks!</p>

---
_comments:

<a id="comment-18595"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-18595" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 28, 2013</p>
    </div>
    <a href="#comment-18595">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-18595" class="permalink" rel="bookmark">With visualizations it&#039;s</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>With visualizations it's always important to have in mind what aspect of reality you want to visualize since you can never capture all of it. What's the aim of the visualization?<br />
The first visualization takes the perspective of a user asking "Which way will my bits go?" and focuses on the anonymity functionality of Tor, leaving out censorship circumvention . The second tries to give an overview of all the different components involved in the tor network, with respect to both anonymity preservation and censorship circumvention.<br />
I haven't been participating in the italian hack meeting and don't know about the discussions leading to the first visualization but I'm pretty sure that educating end users was an important aspect. The second visualization sprang from a discussion where I tried to understand all the parts of the Tor machinery and how they interact, and consequently it's more of a technical overview.</p>
<p>I think the first/italian/users visualization looks very neat but it suffers a bit from the kind of icons chosen: only Alice should use a PC, the relays in the internet should be servers like Jane, Bob and Dave. Jane, Bob and Dave shouldn't have names, since they are not end users like Alice. Picture 2 attempts to illustrate the "Alice communicates with Bob" usecase but it doesn't get it right. Usually they don't communicate directly but via some server - a mail server, a web server etc. Alice communicating with Bob via a forum site could look like this: Bob would have a PC too and they would both hop through the Tor network to reach that server running the forum.<br />
Then I'd like "the internet" to be a 4x4 matrix, but still only 5 of them Tor nodes to make it look more real and the Tor nodes a little more distributed and the hops a little more back and forth.</p>
<p>The weak spot of the traffic visualization is the relay. One traffic crossing doesn't evoke "shaking off your persecutors" but I see how it is the one relay between guard/bridge and exit node. Maybe if it was a city with skyscrapers and many crossings… but then how do you picture it differently from "The Internet" to the right? Another more fundamental problem: if the Internet is the roads than strictly speaking there can be only buildings at crossings and only one of them. That's more like a rural situation, which in turn doesn't evoke "the Internet". Or maybe you leave your car at an entrance node (park+ride), take the subway wearing sunglasses (to wipe your traces) and then exit in the skyscraperish hear of town?<br />
This is tricky but you just can't get everything right. The picture is already pretty good as an illustration to an introductory text. It definitely helps building a mental model. The aim shouldn't be to have a perfect visual metaphor that get's everything right but to:<br />
1) not evoke wrong analogies<br />
2) mention everything important<br />
3) evoke good analogies<br />
(in that order)</p>
<p>Cheers,<br />
Thomas</p>
</div>
  </div>
</article>
<!-- Comment END -->
