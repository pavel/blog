title: New Project Manager and Director of Communications for Tor
---
pub_date: 2015-05-12
---
author: ailanthus
---
tags:

organization
strategy
communications
---
_html_body:

<p>Tor has hired two new people— a Project Manager and a Director of Communications—to help the group stay on track, build its user base, and explain its work to the world.</p>

<p><strong>Isabela Bagueros </strong> is the new project manager at Tor. She is joining Tor to coordinate its development teams and help them define their roadmaps, keep track of priorities, and ensure that Tor is always thinking “user first” while building things.</p>

<p>Isabela is from Brazil, where in the late 1990s she started to play with free software; in the early 2000s, she joined the information democratization movement that was growing quickly with the increase of Internet access around the world.</p>

<p>Isabela has volunteered with Indymedia, SFCCP (San Francisco Community Colocation Project), and other free software/hacker collectives around the world. She worked for Brazil’s Federal Government at the Ministry of Communications digital inclusion program, and later coordinated the project to migrate Presidential Palace IT infrastructure to free software. Before joining Tor, she was a Product Manager at Twitter, where she worked for over four years on the Internationalization and Growth teams, respectively.</p>

<p>Bagueros says that she has been a Tor user “since I can't remember” and she strongly believes in the right to privacy and keeping the Internet free, as in “Liberdade.”</p>

<p>Said Tor Project interim Executive Director Roger Dingledine, “Isabela’s background in the free software community has let her get up to speed on our work really quickly, as well as adapt to our communications and development styles."</p>

<p>“We have many different projects going on at once, and we rely on Isabela to help prioritize and schedule them so we can keep our funders and other communities involved and informed about our progress. Not only do we value her organizational prowess, but she also has a background in helping to make technology more usable by ordinary people, so we're excited to have her play a larger role in getting Tor to a wider audience,” said Dingledine.</p>

<p><strong>Kate Krauss</strong> is Tor’s first Director of Communications, where she is sharing news about Tor’s unique technical projects with the outside world.</p>

<p>Kate will also be reaching out to groups of human rights activists to teach them about Tor, and is studying efforts to restrict privacy in countries across the globe. She also hopes to launch Tor Journalist Camp, where journalists who cover Tor can learn about the technical workings of the Tor Network, Tor hidden services, and Tor’s many other projects—and the ideas about privacy that underpin them.</p>

<p>Kate was an early member of the activist group ACT UP, where she led a California statewide coalition that doubled funding for an AIDS medication fund and spurred the reorganization of the state’s HIV funding priorities. One of the first US activists to embrace international AIDS advocacy, she was a key US strategist behind the campaign to get AIDS drugs into African countries in the late 1990s.</p>

<p>As director of the small advocacy group the AIDS Policy Project, Kate organized successful campaigns that freed a number of human rights defenders in China.  Her work also helped secure some $90 million in aid for China's HIV/AIDS programs from the Global Fund to Fight AIDS, TB, and Malaria. Later, at Physicians for Human Rights, her media work supported the successful campaign to reauthorize the $48 billion President’s Emergency Plan for AIDS Relief.</p>

<p>Kate began her anti-censorship career in an anonymous art collective covered in ARTFORUM, ARTNews, and Newsweek, as Girl #1. She became interested in information security issues while helping Chinese human rights defenders who were being surveilled.</p>

<p>She has placed front-page articles in the New York Times, the Washington Post, the Wall Street Journal, and other major outlets and has written opinion pieces for the Washington Post, the International Herald Tribune, and other newspapers.</p>

<p>Said Dingledine, “There are so many journalists out there who are excited about Tor but don't know where to start. Having Kate helps us keep them informed and coordinated. As Tor continues to go mainstream, her communication skills are critical to helping us get there. Tor’s wide diversity of users--from civic-minded individuals and ordinary consumers to activists, journalists, and companies—is part of its security. Kate is critical to helping us reach all of these audiences at once.”</p>

---
_comments:

<a id="comment-93234"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93234" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 12, 2015</p>
    </div>
    <a href="#comment-93234">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93234" class="permalink" rel="bookmark">&quot;technology more usable by</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"technology more usable by ordinary people". so i'm ordinary if i don't use a computer?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-93275"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93275" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 12, 2015</p>
    </div>
    <a href="#comment-93275">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93275" class="permalink" rel="bookmark">Speaking as one user who has</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Speaking as one user who has been calling for the creation of just these positions, this is awesome news!  And it is very encouraging that Tor is able to hire people with such impressive backgrounds!  This sends the right message at the right time: Tor is a core part of the internet, and won't be easily killed off by our enemies.</p>
<p>&gt; Kate will also be reaching out to groups of human rights activists to teach them about Tor, and is studying efforts to restrict privacy in countries across the globe. She also hopes to launch Tor Journalist Camp, where journalists who cover Tor can learn about the technical workings of the Tor Network, Tor hidden services, and Tor’s many other projects—and the ideas about privacy that underpin them.</p>
<p>I was just about to post a request that Tor Project collaborate more closely with Citizen Lab.  Tor Journalist Camp would be the perfect place for workshops run by both organizations.  How the bad guys work their evil (Citizen Lab) and how the good guys can try to thwart them (Tor).  Someone like Julia Angwin (Pro Publica) would make a suitable keynote speaker.</p>
<p>Thinking along the same lines, I urge the Project to consider offering practical cybersec training to any or all of these groups</p>
<p>o IT people who work for small health organizations and NGOs,</p>
<p>o jurists (especially those who work on international war crimes cases),</p>
<p>o legislative staff who need to do OSI safely.</p>
<p>Tall order, I know.  And training the last group in particular could make Tor appear to be more politically involved than everyone would feel comfortable with, so this suggestion may be worth debating a bit.</p>
<p>Possible unintended consequences of training legislative staff in the USA in particular include:</p>
<p>o what if they use their new skills for oppo research on someone running against their boss?</p>
<p>o in the USA, FBI will scream that they can't watch for evidence of public corruption if they can't read every email sent by every staffer (to which we can reply that while public corruption is a serious concern, illegal FBI practices such as "parallel construction" and extrajudicial "asset forfeiture" suggest that FBI is itself too corrupt to be entrusted with investigations into public corruption).</p>
<p>&gt; Kate began her anti-censorship career in an anonymous art collective covered in ARTFORUM, ARTNews, and Newsweek, as Girl #1. </p>
<p>I was just about to renew my request that Tor Project reach out to artists when I noticed this.  This is just the kind of background I think is needed!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-93284"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93284" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 13, 2015</p>
    </div>
    <a href="#comment-93284">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93284" class="permalink" rel="bookmark">In this &quot;macho&quot; world of IT,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>In this "macho" world of IT, diversity is key and this will not only<br />
enrich the existing tor structure but also be an example that there<br />
are women with rich backgrounds in the free software world.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-93327"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93327" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 13, 2015</p>
    </div>
    <a href="#comment-93327">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93327" class="permalink" rel="bookmark">this is exciting news!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>this is exciting news!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-93459"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93459" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 13, 2015</p>
    </div>
    <a href="#comment-93459">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93459" class="permalink" rel="bookmark">&quot;Tor continues to go</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Tor continues to go mainstream"</p>
<p>wow!<br />
Full ACK, good PR is needed more than ever.<br />
It is a safe prediction that smear &amp; criminalization campaigns will continue to get nastier.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-93472"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93472" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 14, 2015</p>
    </div>
    <a href="#comment-93472">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93472" class="permalink" rel="bookmark">Wonderful, but far too late.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wonderful, but far too late. The Tor community is in a bubble. Outside of that bubble, people have been successfully persuaded that online anonymity is a very bad thing and think that the government should put a stop to it. It's only a question of time now.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-93506"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93506" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 14, 2015</p>
    </div>
    <a href="#comment-93506">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93506" class="permalink" rel="bookmark">&gt; In this &quot;macho&quot; world of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; In this "macho" world of IT, diversity is key and this will not only<br />
enrich the existing tor structure but also be an example that there<br />
are women with rich backgrounds in the free software world.</p>
<p>I care even more about ties to respectable journalism outlets like First Look and politically not impotent NGOs like Wikimedia than about gender, but we certainly need a more balanced sex ratio in IT, so the fact the new hires happen to be women is also a good thing.  Another reason this helps is that Tor should (IMO) seek more small donations from ordinary citizens all over the world, and if half the population sees evidence their gender is well represented, they may be more inclined to help out.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-93741"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93741" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 17, 2015</p>
    </div>
    <a href="#comment-93741">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93741" class="permalink" rel="bookmark">Kate, please publicize and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Kate, please publicize and try to explain to reporters the Tor context for this horrid development:</p>
<p><a href="http://arstechnica.com/tech-policy/2015/05/uk-government-quietly-rewrites-hacking-laws-to-grant-gchq-immunity/" rel="nofollow">http://arstechnica.com/tech-policy/2015/05/uk-government-quietly-rewrit…</a></p>
<p>&gt; The UK government has quietly passed new legislation that exempts GCHQ, police, and other intelligence officers from prosecution for hacking into computers and mobile phones.  While major or controversial legislative changes usually go through normal parliamentary process (i.e. democratic debate) before being passed into law, in this case an amendment to the Computer Misuse Act was snuck in under the radar as secondary legislation. According to Privacy International, "It appears no regulators, commissioners responsible for overseeing the intelligence agencies, the Information Commissioner's Office, industry, NGOs or the public were notified or consulted about the proposed legislative changes... There was no public debate."</p>
<p>Tor users: alarum!  Because WE are high on the list of the prospective targets.</p>
<p>Please stress that similar immunity for warrantless FBI intrusions into our computers (the euphemism used is "remote search of electronic media") will result from a proposed change in Rule 41(b) of the US Code of Criminal Procedure</p>
<p><a href="https://www.aclu.org/sites/default/files/field_document/aclu_comments_on_rule_41.pdf" rel="nofollow">https://www.aclu.org/sites/default/files/field_document/aclu_comments_o…</a></p>
<p>if the US Congress does not intervene before Jan 2016.   As with the GCHQ immunization, the change to Rule 41(b) is being implemented without any real public discussion, much less legislative debate.</p>
<p>Please look for previous discussions in this blog of "remote searches" conducted by the Dutch national police, which eventually admitted were executed without any legal authority under Dutch law.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-93746"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93746" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 17, 2015</p>
    </div>
    <a href="#comment-93746">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93746" class="permalink" rel="bookmark">I urge Kate to make a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I urge Kate to make a practice of daily reading, using Tor Browser of course, of such essential blogs and news outlets as</p>
<p>* arstechnica.com</p>
<p>* theregister.co.uk</p>
<p>* firstlook.org/theintercept</p>
<p>* techdirt.com</p>
<p>* publicintelligence.net</p>
<p>* cryptome.org</p>
<p>* aclu.org Blog of Rights</p>
<p>* eff.org Deeplinks blog</p>
<p>* emptywheel.net</p>
<p>* hrw.org</p>
<p>* rsf.org</p>
<p>* amnesty.org</p>
<p>Please also follow closely the continuing publication of Snowden leaked documents.  You probably already know this link: </p>
<p><a href="https://www.eff.org/nsa-spying/nsadocs" rel="nofollow">https://www.eff.org/nsa-spying/nsadocs</a></p>
<p>Please remain alert for early warnings of administrative/legislative manueverings which are used by the Enemies of Tor to secretly establish "legally authorized" [sic]</p>
<p>* state-sponsored censorship</p>
<p>* municipal, provincial or national communications kill switches</p>
<p>i.e. the very things Tor is primarily intended to prevent, and please alert Tor users to important issues as they arise.</p>
<p>While FVEY are not the only enemies of the Tor Project and everything for which it stands, the USA is arguably the pre-eminent enemy because of</p>
<p>* NSA's global dragnet,</p>
<p>* rampant US military adventurism,</p>
<p>* extensive torture and political assassination by US agents,</p>
<p>* the USG's explicit disavowal of any notion that anyone outside US territory enjoys any civil rights whatever,</p>
<p>* the fact that the Project and most of the developers (and many key civil rights and human rights organizations) are based in the USA.</p>
<p>For this reason I wish to stress that Marcy Wheeler's emptywheel blog is (if read closely over time) an outstanding source of information on USG machinations.</p>
<p>Everyone here is no doubt familiar (by reputation, at least) with China's Great Firewall, but many Tor users are also personally familiar with USG attempts to disrupt our (legal) activism activities or even to censor our access to sites such as Wikileaks.</p>
<p>It is important to avoid simple characterizations of certain countries or industries as "friendly" to Tor users.   Looking more closely often reveals a nasty side to seemingly innocuous organizations.  For example,  NTR Foundation is a nonprofit funded by NTR plc, a US energy company which is involved in beneficial sounding "green business" such as harvesting wind power in Tornado Alley.</p>
<p>But look at this exchange which occurred in Mar 2015 at a Council on Foreign Relations (cfr.org) shindig, in which CIA Director John Brennan took questions from an audience of lobbyists, one of whom appeared to suggest that the USG should be doing more to destroy networks (Brazil's Petrobas, perhaps?) which NTR views as "opposed" to some "US national interest":</p>
<p>&gt;    QUESTION: Thank you. Paula DiPerna, NTR Foundation. This is probably an unpopular suggestion, but is it feasible or how feasible would it be to do a little selective Internet disruption in the areas concerned, a la a blockade, digital blockade, and then an international fund to indemnify business loss?</p>
<p>&gt;   BRENNAN: OK. First of all, as we all know, the worldwide web, the Internet, is a very large enterprise. And trying to stop things from coming out, there are political issues, there are legal issues here in the United States as far as freedom of speech is concerned. But even given that consideration, doing it technically and preventing some things from surfacing is really quite challenging. And we see that a number of these organizations have been able to immediately post what they’re doing in Twitter. And the ability to stop some things from getting out is really quite challenging. As far as, you know, indemnification of various companies on some of these issues, there has been unfortunately a very, very long, multi-year effort on the part of the Congress to try to pass some cybersecurity legislation that addressed some of these issues. There has been passage in the Senate.  I think it’s overdue. We need to update our legal structures as well as our policy structures to deal with the cyber threats we face.</p>
<p>Would I be merely tilting at windmills were I to suggest that this exchange shows that NTR may not be quite so nice as its website makes out?  </p>
<p>To repeat: one must always be most suspicious precisely of those unexamined assumptions about who/what is or is not "beneficial" to mankind which make our lives dramatically easier, because these are precisely the places where governments and the corporations they serve may be manipulating our actions and beliefs to suit their hidden agenda.</p>
<p>Two phenomena visible here which arise again and again:</p>
<p>* many "democratic" governments (including FVEY, India,...) currently favor disguising the kind of censorship and reprisal one would expect from regimes like Putin's Russia under the euphemism of "fighting cybercrime" (making the world safe for Standard Oil and SONY) under the rubric of "fighting cybercrime" [sic] as well as "fighting terrorism" [sic]</p>
<p>* industry lobbyists are much more interested in winning indemnification than they are in protecting civil liberties, privacy, or the democratic process.</p>
<p>For the original context (public statements by Brennan which are likely to include significant dissimulation) please see:</p>
<p><a href="https://www.emptywheel.net" rel="nofollow">https://www.emptywheel.net</a><br />
Wyden et al: Spot the Lie in Brennan’s CFR Speech Contest!<br />
emptywheel<br />
9 May 2015</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-93939"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93939" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 22, 2015</p>
    </div>
    <a href="#comment-93939">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93939" class="permalink" rel="bookmark">What about a regularly</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What about a regularly updated collection of news articles bearing on Tor? </p>
<p>Here is a recent example:</p>
<p><a href="http://www.theregister.co.uk/2015/05/22/new_relay_selection_for_tor_to_spoil_spooks_fun/" rel="nofollow">http://www.theregister.co.uk/2015/05/22/new_relay_selection_for_tor_to_…</a><br />
New relay selection fix for Tor to spoil spooks' fun (eventually)<br />
Quick, before Skynet takes control of the Five Eyes<br />
22 May 2015</p>
<p>&gt; Research by American and Israeli academics has lead to the development of Astoria, a new Tor client specifically designed to spoil spooks' traffic analysis of the surveillance-dodging network. Astoria all-but decimates the number of vulnerable connections on the Tor network, bring the figure from 58 per cent of total users to 5.8 per cent, the researchers claim.</p>
</div>
  </div>
</article>
<!-- Comment END -->
