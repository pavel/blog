title: Torbutton has retired
---
pub_date: 2023-10-18
---
author: pierov
---
categories:

applications
---
summary: Tor Browser 13.0 has just been released, and it is the first version not to have any remaining of the Torbutton code.
---
body:
Once upon a time, the Tor Browser Bundle was an actual bundle. It included Firefox, the Tor daemon, and Torbutton, the extension to turn on and off the Tor mode in the browser.

This [toggle model was not great](https://blog.torproject.org/toggle-or-not-toggle-end-torbutton/) and extremely confusing to some users. This and other problems led to the creation of Tor Browser: [this article](https://blog.torproject.org/tor-browser-advancing-privacy-innovation/) contains more details about this story.

From a technical point of view, Torbutton did not really go away. The visible button disappeared, but much of the related code remained.

Part of the state isolation code was not necessary anymore because Tor Browser always runs in private browsing mode or was dropped over the years thanks to Firefox improvements and the [Tor Uplift initiative](https://wiki.mozilla.org/Security/Tor_Uplift). However, the circuit display, the first-party domain circuit isolation, and other parts of the existing code were still needed. As a result, Torbutton continued to live for many years as a Tor Browser-only built-in extension on its [separate repository](https://gitlab.torproject.org/tpo/applications/torbutton) and included in the browser with git submodules (even though the browser was non-functional without it). New patches and functionalities were written in the Firefox code that constitutes Tor Browser, and the Torbutton code was changed only to fix existing bugs or to keep it working in new versions of Firefox.

Similarly, [Tor Launcher](https://gitlab.torproject.org/tpo/applications/tor-launcher) was the extension that showed the connection window before opening the browser.

However, we wanted to merge the codebases to more easily maintain and improve them and create a better UX. So, version by version, we replaced or refactored and integrated the various components.

[Tor Browser 8.5](https://blog.torproject.org/new-release-tor-browser-85/) turned Torbutton's security slider into the _security level_ accessible through about:preferences. [Version 10.5](https://blog.torproject.org/improving-ux-connecting-to-tor-105/) moved the connection workflow _inside_ the browser. However, it still used Tor Launcher as a backend. We completely [refactored and merged it](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40933) with the rest of the browser code in version 12.0. In the same version, we did the same for the [new identity functionality](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40926) and the [security level backend](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40925). In [our previous major, 12.5](https://blog.torproject.org/new-release-tor-browser-125/), we reimplemented the circuit display and the download warning.

For the just-released 13.0, we decided to accelerate the pace and remove all the legacy code we still had from Torbutton.

One of the main reasons is that the Tor daemon will eventually be replaced with [Arti](https://arti.torproject.org), the new implementation written in Rust. Its control interface is more modern and incompatible with the legacy implementation. However, we expect to support both configurations for a while.

So, we decided to create an abstraction focused on the browser's needs and a first concrete implementation for the control port protocol understood by C-tor. The transition is intended to be seamless, and users should not even notice it in our ambitions 😁️. So far, we have not received feedback about regressions due to the change.

A few references to the Torbutton name are still in 13.0: traditionally, the two extensions also included our translation files. Replacing them is the last step to remove these final references. We plan to do so with the migration to Fluent, the most recent and preferred format used by Firefox, but something for 13.5.

In the meantime, we are eager to hear your feedback! Please let us know if you find any problems in [our forum](https://forum.torproject.org/) or in our [bug tracker](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues).
