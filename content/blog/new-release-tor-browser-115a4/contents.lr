title: New Alpha Release: Tor Browser 11.5a4 (Windows/macOS/Linux)
---
pub_date: 2022-02-18
---
author: boklm
---
categories:

applications
releases
---
summary: Tor Browser 11.5a4 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5a4 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5a4/).

This version includes important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2022-05/) to Firefox.

The full changelog since [Tor Browser 11.5a3](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=master) is:

- Windows + OS X + Linux
  - Update Firefox to 91.6.0esr
  - Update NoScript to 11.2.19
  - Tor Launcher 0.2.33
  - [Bug tor-browser#40562](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40562): Reorganize patchset
  - [Bug tor-browser#40598](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40598): Remove legacy settings read from TorSettings module
  - [Bug tor-browser#40679](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40679): Missing features on first-time launch in esr91
  - Added extensions.torlauncher.launch_delay debug pref to simulate slow tor daemon launch [tor-launcher]
  - [Bug tor-browser#40752](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40752): Misleading UX when about:tor as New Tab
  - [Bug tor-browser#40775](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40775): about:ion should no tbe labeled as a Tor Browser page
  - [Bug tor-browser#40793](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40793): moved Tor configuration options from old-configure.in to moz.configure
  - [Bug tor-browser#40795](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40795): Revert Deutsche Welle v2 redirect
- Build System
  - Windows + OS X + Linux
    - Update Go to 1.17.6
    - [Bug tor-browser-build#40416](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40416): Pick up obfsproxy 0.0.12
