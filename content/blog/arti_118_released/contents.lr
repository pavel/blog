title: Arti 1.1.8 is released: Onion service infrastructure
---
author: nickm
---
pub_date: 2023-09-05
---
categories: announcements
---
summary:

Arti 1.1.8 is released and ready for download.
---
body:

Arti is our ongoing project to create a next-generation Tor client in
Rust.   Now we're announcing the latest release, Arti 1.1.8.

This release continues our work on support for onion services in Arti.
It includes backend support for nearly all of the functionality needed
to launch and publish an onion service and accept incoming requests
from onion service clients.  This functionality is not yet usable,
however: we still need to connect it all together, test and debug it,
and provide high-level APIs to allow the user to actually turn it on.

With this release, there have been
many smaller and less visible changes as well;
for those, please see the [CHANGELOG].

For more information on using Arti, see our top-level [README], and the
documentation for the [`arti` binary].

Thanks to everybody who's contributed to this release, including
Emil Engler, Jim Newsome, Micah Elizabeth Scott, Saksham Mittal,
and Trinity Pointard.

Also, our deep thanks to [Zcash Community Grants] and our [other sponsors]
for funding the development of Arti!

[CHANGELOG]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/CHANGELOG.md#arti-118-5-september-2023
[README]: https://gitlab.torproject.org/tpo/core/arti/-/blob/main/README.md
[`arti` binary]: https://gitlab.torproject.org/tpo/core/arti/-/tree/main/crates/arti
[Zcash Community Grants]: https://zcashcommunitygrants.org/
[other sponsors]: https://www.torproject.org/about/sponsors/
