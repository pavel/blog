title: New Alpha Release: Tor Browser 13.0a5 (Android, Windows, macOS, Linux)
---
pub_date: 2023-09-24
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0a5 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0a5 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0a5/).

This release updates Firefox to 115.3.0esr, including bug fixes, stability improvements and important security updates. Android-specific security updates from Firefox 118 are not yet available, but will be part of the next alpha release scheduled for next week.

## Major Changes

This is our fifth alpha release in the 13.0 series which represents a transition from Firefox 102-esr to Firefox 115-esr. This builds on a year's worth of upstream Firefox changes, so alpha-testers should expect to run into issues. If you find any issues, please report them on our [gitlab](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/new) or on the [Tor Project forum](https://forum.torproject.org/c/feedback/tor-browser-alpha-feedback/6).

We are in the middle of our annual esr transition audit, where we review Mozilla's year's worth of work with an eye for privacy and security issues that would negatively affect Tor Browser users. This will be completed before we transition the 13.0 alpha series to stable. At-risk users should remain on the 102-esr based 12.5 stable series which will continue to receive security updates until 13.0 alpha is promoted to stable.

### Build Output Naming Updates

As a reminder from the [13.0a3](https://blog.torproject.org/new-alpha-release-tor-browser-130a3) release post, we have made the naming scheme for all of our build outputs mutually consistent. If you are a downstream packager or in some other way download Tor Browser artifacts in scripts or automation, you will have a bit more work to do beyond bumping the version number once the 13.0 alpha stabilizes. All of our current build outputs can be found in the [distribution directory](https://www.torproject.org/dist/torbrowser/13.0a4/)

## Known Issues

### All Platforms

The Snowflake pluggable-transport is [no longer working](https://lists.torproject.org/pipermail/anti-censorship-team/2023-September/000314.html) for some users due to cdn.sstatic.net resolving to a Cloudflare IP rather than a Fastly one. As a result, the domain fronting functionality required by the Snowflake pluggable-transport no longer works and users will not be able to use it to connect to tor on versions of Tor Browser older than 13.0a5.

For now, this can be worked around by using custom Snowflake bridge lines with an updated fronting domain. Directions on how to do this can be found on this post on the tor forum:

- https://forum.torproject.org/t/temporary-fix-for-moat-and-connection-assist/9385

This issue is being tracked [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42120) and is fixed in 13.0a5 and will also be fixed in the next stable release. Users who rely on Snowflake for Tor connectivity will not be able to bootstrap and update their Tor Browser instance without the aforementioned manual workaround.

### Desktop

The automatic censorship circumvention system is also currently failing due to the same domain-fronting issue affecting snowflake. As a workaround, users can set the `extensions.torlauncher.bridgedb_front` preference to `foursquare.com` in Tor Browser's [about:config](about:config) page.

This issue is being tracked [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42126) and will be fixed in the next stable and alpha releases.

## Full changelog

The full changelog since [Tor Browser 13.0a4](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - Updated OpenSSL to 3.0.11
  - Updated tor to 0.4.8.6
  - [Bug tor-browser#40938](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40938): Migrate remaining torbutton functionality to tor-browser
  - [Bug tor-browser#41327](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41327): Disable UrlbarProviderInterventions
  - [Bug tor-browser#42026](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42026): Disable cookie banner service and UI.
  - [Bug tor-browser#42094](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42094): Disable media.aboutwebrtc.hist.enabled as security in-depth
  - [Bug tor-browser#42112](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42112): Rebase alpha to 115.3.0esr
  - [Bug tor-browser#42120](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42120): Use foursquare as domain front for snowflake
- Windows + macOS + Linux
  - Updated Firefox to 115.3.0esr
  - [Bug tor-browser-build#40893](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40893): Update (Noto) fonts for 13.0
  - [Bug tor-browser#41639](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41639): Fix the wordmark (title and background) of the "About Tor Browser" window
  - [Bug tor-browser#41822](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41822): The default browser button came back on 115
  - [Bug tor-browser#41864](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41864): TOR_CONTROL_HOST and TOR_SOCKS_HOST do not work as expected when the browser launches tor
  - [Bug tor-browser#41903](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41903): The info icon on the language change prompt is not shown
  - [Bug tor-browser#41957](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41957): Revert to Fx's default identity block style for internal pages
  - [Bug tor-browser#41958](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41958): Console error when closing tor browser with about:preferences open
  - [Bug tor-browser#41986](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41986): Fix the control port password handling
  - [Bug tor-browser#42037](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42037): Disable about:firefoxview
  - [Bug tor-browser#42073](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42073): Add simplified onion pattern to the new homepage
  - [Bug tor-browser#42079](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42079): TorConnect: handle switching from Bootstrapped to Configuring state
  - [Bug tor-browser#42083](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42083): RemoteSecuritySettings.init throws error in console
  - [Bug tor-browser#42091](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42091): Onion authorization prompt overflows
  - [Bug tor-browser#42092](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42092): Onion services key table display problems.
  - [Bug tor-browser#42102](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42102): TorProcess says the SOCKS port is not valid even though it is
  - [Bug tor-browser#42110](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42110): Add a utility module for shared UI methods needed for several tor browser components
- Windows
  - [Bug tor-browser#41798](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41798): Stop building private_browsing.exe on Windows
- macOS
  - [Bug tor-browser#42078](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42078): Implement MacOS application icons
- Linux
  - [Bug tor-browser#42088](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42088): Implement Linux application icons
- Android
  - Updated GeckoView to 115.3.0esr
  - [Bug tor-browser#42089](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42089): Disable Mozilla 1805450
  - [Bug tor-browser#42122](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42122): Fix crash due to bad android deprecation and APIs
- Build System
  - Windows + macOS + Linux
    - [Bug tor-browser-build#40956](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40956): Allow testing the updater also with release and alpha channel
  - Windows
    - [Bug tor-browser#41995](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41995): Generated headers on Windows aren't reproducible
