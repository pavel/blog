title: May 2009 Progress Report
---
pub_date: 2009-06-10
---
author: phobos
---
tags:

progress report
anonymity advocacy
translations
proposals
metrics
---
categories:

advocacy
localization
metrics
reports
---
_html_body:

<p><strong>New releases</strong><br />
On May 25, we released Tor 0.2.1.15-rc.<br />
On May 17, we released Tor VM 0.0.2.<br />
On May 25, we released Vidalia 0.1.13 containing</p>

<ul>
<li>Remove an old warning on the relay settings page that running a bridge<br />
    relay requires Tor 0.2.0.8-alpha or newer. </li>
<li>Add a workaround for a bug that prevented Vidalia's tray icon from<br />
    getting added to the system notification area on Gnome when Vidalia was<br />
    run on system startup. Patch by Steve Tyree. (Ticket #247) </li>
<li>Fix a bug that prevented the control panel from displaying when<br />
    running on the Enlightenment window manager. Patch by Steve Tyree. </li>
<li>Rename the CMake variables used to store the location of Qt's lupdate<br />
    and lrelease executables. Recent versions of CMake decided to use the<br />
    same variable name, which was stomping on mine, resulting in the wrong<br />
    lupdate and lrelease executables being used. </li>
<li>Use the TorProcess subclass of QProcess for launching Tor when hashing<br />
    a control password so we can take advantage of its PATH+=:/usr/sbin<br />
    trick on Debian there too. </li>
<li>If a RouterDescriptor object is empty, don't try to display it in the<br />
    router descriptor details viewer. (Ticket #479)</li>
<li>Wait until Vidalia has registered for log events via the control port<br />
    before ignoring Tor's output on stdout. Previously we would start<br />
    ignoring Tor's stdout after successfully authenticating, but before<br />
    registering for log events which, in some cases, could lead to<br />
    messages not appearing in the message log. </li>
<li>Update many translations and remove others than are no longer<br />
    up-to-date enough to be useful.</li>
</ul>

<p>On May 25th, we released Tor Browser Bundle 1.2.0 containing</p>

<ul>
<li>Switch to launching Firefox directly from Vidalia to<br />
       allow multiple instances of Firefox </li>
<li>Update Firefox to 3.0.10 </li>
<li>Update to Qt 4.5.1</li>
<li>Update Firefox prefs.js to stop scanning for plugins </li>
<li>Update libevent to 1.4.11</li>
<li>Include the Tor geoip database</li>
<li>Update Vidalia to 0.1.13</li>
<li>Update Tor to 0.2.1.15-rc</li>
</ul>

<p><strong>Design, develop, and implement enhancements that make Tor a better<br />
tool for users in censored countries.</strong></p>

<p>Matt added "fetch bridges" features to Vidalia 0.2.x.  This provides a link to automatically request bridges from <a href="https://bridges.torproject.org" rel="nofollow">https://bridges.torproject.org</a> for users.</p>

<p><strong>Architecture and technical design docs for Tor enhancements<br />
related to blocking-resistance.</strong><br />
Proposal 160 aims to let authorities modify the bandwidth they put in<br />
the consensus for each relay. This step will allow us to adjust the<br />
weights we advertise for clients, once the measurements from TorFlow<br />
start giving us better weights.<br />
<a href="https://git.torproject.org/checkout/tor/master/doc/spec/proposals/160-bandwidth-offset.txt" rel="nofollow">https://git.torproject.org/checkout/tor/master/doc/spec/proposals/160-b…</a></p>

<p>Proposal 161 describes how node bandwidth ratios are<br />
   computed and how they can be produced in parallel.<br />
<a href="https://git.torproject.org/checkout/tor/master/doc/spec/proposals/161-computing-bandwidth-adjustments.txt" rel="nofollow">https://git.torproject.org/checkout/tor/master/doc/spec/proposals/161-c…</a></p>

<p>Proposal 162 describes "consensus flavors": the size of the networkstatus<br />
consensus is critical, since every user fetches it every few hours. So<br />
we need a way to add new fields -- and remove old fields -- in a way<br />
that lets old clients continue to use the consensus. The current plan<br />
is to build and distribute several different versions at once, so each<br />
client can fetch the one with the format they expect.<br />
<a href="https://git.torproject.org/checkout/tor/master/doc/spec/proposals/162-consensus-flavors.txt" rel="nofollow">https://git.torproject.org/checkout/tor/master/doc/spec/proposals/162-c…</a></p>

<p>Proposal 163 starts to consider the problem of clients using relays as<br />
single-hop proxies. If many clients start doing this (say, to improve<br />
their own performance), it puts additional risk on the relays, since now<br />
an attacker can expect to discover both client origins and destinations<br />
by attacking the relay. Our current strategy for forcing clients to use<br />
more than one hop is quite fragile, and it looks like we will soon need<br />
more robust strategies.<br />
<a href="https://git.torproject.org/checkout/tor/master/doc/spec/proposals/163-detecting-clients.txt" rel="nofollow">https://git.torproject.org/checkout/tor/master/doc/spec/proposals/163-d…</a></p>

<p>Proposal 164 suggests ways to make it easier for relay operators to<br />
discover why they are not listed in the networkstatus consensus. We have<br />
a handle of people each week ask us on IRC why their relay isn't listed,<br />
and currently the only way to answer is to have a competent directory<br />
authority operator go dig around in various files in his datadirectory.<br />
<a href="https://git.torproject.org/checkout/tor/master/doc/spec/proposals/164-reporting-server-status.txt" rel="nofollow">https://git.torproject.org/checkout/tor/master/doc/spec/proposals/164-r…</a></p>

<p>Proposal 165 focuses on simplifying the steps required to add a new<br />
directory authority. The current approach requires manual work from every<br />
directory authority operator within a space of several hours. As the<br />
number of authorities grows, this synchronization is becoming impractical<br />
-- and that's causing us to leave the number of authorities small, which<br />
makes us vulnerable to other attacks. Once this proposal is finalized<br />
and deployed, we'll hopefully be able to add new authorities more<br />
smoothly.<br />
<a href="https://git.torproject.org/checkout/tor/master/doc/spec/proposals/165-simple-robust-voting.txt" rel="nofollow">https://git.torproject.org/checkout/tor/master/doc/spec/proposals/165-s…</a></p>

<p><strong>Grow the Tor network and user base. Outreach.</strong><br />
Jacob attended CONFidence in Krakow, Poland as a keynote speaker.  <a href="http://2009.confidence.org.pl/" rel="nofollow">http://2009.confidence.org.pl/</a></p>

<p>Andrew and Jacob attended the Soul of a New Machine conference in Berkeley, CA.  <a href="http://hrc.berkeley.edu/events/newmachineconference/" rel="nofollow">http://hrc.berkeley.edu/events/newmachineconference/</a></p>

<p>Roger and Andrew attended the 7th Annual Chinese Internet Research Conference in Philadelphia, PA. <a href="http://www.global.asc.upenn.edu/index.php?page=167" rel="nofollow">http://www.global.asc.upenn.edu/index.php?page=167</a></p>

<p>Karsten attended SIGINT 09 in Cologne.</p>

<p>Mike gave a presentation on TorFlow at CodeCon.</p>

<p>Roger met with Nick, Paul Syverson and Aaron Johnson at Yale to work more on Paul's research question: if we trust some Tor relays differently than others, how should we select our paths to be safe, and how do we analyze how safe the paths are?</p>

<p>Roger did a talk for about 15 OSI people in Budapest, Hungary.</p>

<p><strong>Preconfigured privacy (circumvention) bundles for USB or LiveCD</strong><br />
The two large changes were the ability to run multiple instances of Firefox at once, such that a user's personal firefox shouldn't share data with the firefox from our bundle.  The other change is the ability to stop TBB firefox from scanning the system for potential plugins, like Windows Media, Java, etc.  </p>

<p>Started work on a hardened branch of Incognito live CD to help protect users from possible bugs in the programs running.</p>

<p><strong>Scalability, load balancing, directory overhead, efficiency.</strong></p>

<p>We documented the metrics we collect to help us determine the best ways to scale the Tor network.  <a href="http://blog.torproject.org/blog/performance-measurements-and-blockingresistance-analysis-tor-network" rel="nofollow">http://blog.torproject.org/blog/performance-measurements-and-blockingre…</a>  A number of nodes are now collecting this information to assist our network-wide measurements.</p>

<p>Much progress on torctl and torflow tools being used to measure real and potential performance of nodes in the public tor network.  </p>

<p>Mike wrote proposal 161 describing how node bandwidth ratios are<br />
   computed and how they can be produced in parallel.  The proposal can be found at <a href="http://git.torproject.org/checkout/tor/master/doc/spec/proposals/161-computing-bandwidth-adjustments.txt" rel="nofollow">http://git.torproject.org/checkout/tor/master/doc/spec/proposals/161-co…</a></p>

<p>Karsten finished a first patch to dump statistics about local queues to disk every 15 minutes. A first impression of how these data could be evaluated can be found in <a href="http://freehaven.net/~karsten/volatile/bufferstats-2009-05-25.pdf" rel="nofollow">http://freehaven.net/~karsten/volatile/bufferstats-2009-05-25.pdf</a>. The goal is to see if our buffer allocation algorithms are sufficient or need tweaking.</p>

<p><strong>More reliable (e.g. split) download mechanism.</strong><br />
Developed the ability to split Apple OS X bundles into 1.44MB chunks.  The functionality is native to OS X versions 10.4 and newer.  It will not work in versions 10.3.9 or earlier releases.</p>

<p><strong>Translation work, ultimately a browser-based approach</strong><br />
11 Polish updates<br />
4 German updates<br />
Portugese torbutton updates<br />
Danish torbutton updates<br />
Romanian torbutton updates<br />
11 Italian updates<br />
3 Chinese updates</p>

---
_comments:

<a id="comment-1532"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1532" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 13, 2009</p>
    </div>
    <a href="#comment-1532">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1532" class="permalink" rel="bookmark">exit policy of tor nodes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i use tor for my emails. as you all know this is done by configuring thunderbird to forward its connections through tor/privoxy running on the local host. recently it came to my attention that all the tor nodes i have tried have enabled as the exit policy<br />
reject: *.465<br />
which is the port through which one can connect to smtp.gmail.com. thus, in my case i can download my emails through tor, but cannot send them.</p>
<p>i am probably being silly, but is there some rational explanation for this change?<br />
e.g., some e-mail providers began to reject traffic from tor servers?<br />
some arm twisting?<br />
it seems to me that sending email through tor is so slow that no sane spammer would use it for distribution, but than what do i know.</p>
<p>thanks,<br />
anonymous</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1538"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1538" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">June 16, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1532" class="permalink" rel="bookmark">exit policy of tor nodes</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1538">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1538" class="permalink" rel="bookmark">re: exit policy of tor nodes</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you're trying to select nodes, that's probably a losing plan.  Better to let the Tor software figure out which nodes allow exit on that port.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1535"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1535" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://digitallife.germanblogs.de/">David (not verified)</a> said:</p>
      <p class="date-time">June 15, 2009</p>
    </div>
    <a href="#comment-1535">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1535" class="permalink" rel="bookmark">Tor speedbrake ?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I used Tor for a long time, could be possible that TOR reduce my DSL Speed ?<br />
Or what was the Problem ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1539"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1539" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">June 16, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1535" class="permalink" rel="bookmark">Tor speedbrake ?</a> by <a rel="nofollow" href="http://digitallife.germanblogs.de/">David (not verified)</a></p>
    <a href="#comment-1539">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1539" class="permalink" rel="bookmark">re: Tor speedbrake ?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Unless your DSL modem can't handle the number of tcp connections, there's no way Tor would change your DSL speed as set by your provider.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1559"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1559" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 18, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1535" class="permalink" rel="bookmark">Tor speedbrake ?</a> by <a rel="nofollow" href="http://digitallife.germanblogs.de/">David (not verified)</a></p>
    <a href="#comment-1559">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1559" class="permalink" rel="bookmark">Speedrestrictions have been enforced</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You are right, for some time now, every new releases has made Tor slower and slower for you if you had it setup properly from the start.</p>
<p>The only way to get a decent speed now would be to use an old outdated, possibly insecure version of Tor that would not be as restricted as the up to date versions now are,<br />
But even then you won't get the speeds you got 6 or 12 months ago since Tornodes running newer versions also are responsible for speed restrictions applied to your client.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1537"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1537" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2009</p>
    </div>
    <a href="#comment-1537">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1537" class="permalink" rel="bookmark">Vidalia and ssleay32.dll</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I had the same crashes that is being described here: <a href="https://trac.vidalia-project.net/ticket/395" rel="nofollow">https://trac.vidalia-project.net/ticket/395</a><br />
so I tried the suggested fix provided by edmanm and renamed ssleay32.dll before starting tor browser bundle, and everything seems to work ok. My question is, does this pose any risk to my privacy, and are there any better solutions?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1540"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1540" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">June 16, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1537" class="permalink" rel="bookmark">Vidalia and ssleay32.dll</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1540">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1540" class="permalink" rel="bookmark">re: Vidalia and ssleay32.dll</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Which version of TBB has this error?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-1541"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1541" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 16, 2009</p>
    </div>
    <a href="#comment-1541">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1541" class="permalink" rel="bookmark">re: Vidalia and ssleay32.dll</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Both 1.2.0 and 1.2.1-dev.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-1543"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1543" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 17, 2009</p>
    </div>
    <a href="#comment-1543">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1543" class="permalink" rel="bookmark">ssleay32.dll</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I have the same problem with vidalia (TBB version 1.2.0) that is being described above. I'm using a public computer and I can't rename or replace ssleay32.dll (don't have access to it), so those fixes doesn't work for me. Is there another solution? There should be, considering this is what TBB is made for and there seem to be other ppl with this problem...</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1562"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1562" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">June 19, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1543" class="permalink" rel="bookmark">ssleay32.dll</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1562">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1562" class="permalink" rel="bookmark">re: ssl errors</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>these should be fixed in the 1.2.1-rc posted last night.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1697"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1697" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 29, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to phobos</p>
    <a href="#comment-1697">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1697" class="permalink" rel="bookmark">That&#039;s awesome, thanks a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That's awesome, thanks a lot! Finally vidalia is working.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-1560"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1560" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">June 18, 2009</p>
    </div>
    <a href="#comment-1560">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1560" class="permalink" rel="bookmark">Tor VM 0.0.2</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"On May 17, we released Tor VM 0.0.2."</p>
<p>Interresting, were can it be downloaded ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-1561"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-1561" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">June 19, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-1560" class="permalink" rel="bookmark">Tor VM 0.0.2</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-1561">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-1561" class="permalink" rel="bookmark">https://torproject.org/torvm/</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://torproject.org/torvm/" rel="nofollow">https://torproject.org/torvm/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
