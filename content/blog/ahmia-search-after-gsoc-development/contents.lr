title: Ahmia search after GSoC development
---
pub_date: 2014-09-08
---
author: juha
---
tags:

hidden services
GSoC 2014
ahmia.fi
ahmia
search engine
onion
crawling
---
categories: onion services
---
_html_body:

<p>The Google Summer of Code (GSoC) was an excellent opportunity to improve on the Ahmia search engine. With Google's stipend and friendly mentoring from The Tor Project, I was able to concentrate on development of my search engine project. Thank you all!</p>

<p><a href="https://trac.torproject.org/projects/tor/wiki/doc/gsoc" rel="nofollow">GSoC 2014</a> is over, but I am sticking around to continue developing and maintaining Ahmia.</p>

<p>Here is the current status of ahmia after GSoC development:</p>

<h2>Introduction</h2>

<p>Ahmia is <a href="https://github.com/juhanurmi/ahmia/" rel="nofollow">open-source search engine software</a> for Tor hidden service websites. You can test the running search engine at <a href="https://ahmia.fi/search/" rel="nofollow">ahmia.fi</a>.</p>

<p>Building a search engine for anonymous web sites running inside the Tor network is an interesting problem. Tor enables web servers to <a href="https://www.torproject.org/docs/hidden-services" rel="nofollow">hide their location</a> and Tor users can connect to these authenticated hidden services while the server and the user both stay anonymous. However, finding web content is hard without a good search engine and therefore a search engine is needed for the Tor network.</p>

<p>Web search engines are needed to navigate and search the web. There were no search engines for searching hidden service web content, so I decided to build a search engine specially for Tor. I registered ahmia.fi and started development on it as a side project in 2010.</p>

<p>This development involved programming and testing web crawlers, thinking of ways to find hidden service addresses (since the protocol does not allow enumeration), learning about the Tor community, and implementing a filtering policy. Moreover, I implemented <a href="https://ahmia.fi/documentation/" rel="nofollow">an API</a> that empowers other Tor services that publish content to integrate with Ahmia.</p>

<p>As a result, Ahmia is a working search engine that indexes, searches and catalogs content published on Tor Hidden Services. Furthermore, it is an environment to share meaningful statistics, insights and news about the Tor network itself.</p>

<h2>Interesting Summer of Code</h2>

<p>One of my best memories from the summer is the Tor Project's Summer 2014 Developers meeting that was hosted by Mozilla in Paris, France. I have always admired the people who are working on the Tor Project.</p>

<p>I also loved the coding itself. Finally I had time to improve the Ahmia search engine and its many features. <a href="https://github.com/juhanurmi/ahmia/commits/master" rel="nofollow">I did a lot of work</a> and liked it.</p>

<p>Some journalist were very interested in my work: Carola Frediani asked if I could analyze the content of hidden services. I coded a script that fetches every front page's HTML, I gathered all the keywords, headers and description texts and made a simple word cloud visualization.</p>

<p><img src="https://ahmia.fi/static/visuals/content.png" alt="Hidden website content visualization." /></p>

<p>It is a simple way to glance what is published on the hidden websites.</p>

<p>Carola found this data useful and used it in her presentation at <a href="http://www.sotn.it" rel="nofollow">www.sotn.it</a> on June 11th.</p>

<h2>Technical design of ahmia</h2>

<p>The Ahmia web service is written using the Django web framework. As a result, the server-side language is Python. On the client-side, most of the pages are plain HTML. There are some pages that require JavaScript, but the search itself works without client-side JavaScript.</p>

<p>The components of Ahmia are:</p>

<ul>
<li>Django front-end site</li>
<li>PostgreSQL database for the site</li>
<li>Custom scripts to download data about hidden services</li>
<li>Django-Haystack connection to Solr database</li>
<li>Apache Solr for the crawled data</li>
<li>OnionBot crawler that gathers data to Solr database</li>
</ul>

<p><img src="https://raw.githubusercontent.com/juhanurmi/ahmia/master/technical_architecture.png" alt="Technical architecture." /></p>

<p><a href="https://github.com/juhanurmi/ahmia/blob/master/README.md" rel="nofollow">See installation and developing tutorial</a></p>

<h2>Search</h2>

<p>The full-text search is implemented using Django-Haystack. The search is using crawled website data that is saved to Apache Solr.</p>

<h2>OnionDir</h2>

<p><a href="https://ahmia.fi/address/" rel="nofollow">OnionDir</a> is a list of known online hidden service addresses. A separate script gathers this list and fetches information fields from the HTML (title, keywords, description etc.). Furthermore, users can freely edit these fields.</p>

<p>We've also started a convention where hidden service admins can add a file to their website, called <a href="https://ahmia.fi/documentation/descriptionProposal/" rel="nofollow">description.json</a>, to offer an official description of their site in Ahmia.</p>

<p>As a result, this information is shown in <a href="https://ahmia.fi/address/#/search=Description%20downloaded%20directly%20from%20the%20hidden%20service." rel="nofollow">the OnionDir page</a> and over 80 domains are already using this method.</p>

<h2>Statistics</h2>

<p>We are gathering statistics from hidden services. As a result, we can represent and share <a href="https://ahmia.fi/documentation/" rel="nofollow">meaningful data about hidden services</a> and <a href="https://ahmia.fi/stats/viewer" rel="nofollow">visualize it</a>.</p>

<p>We are gathering three types of popularity data:</p>

<ol>
<li>Tor2web nodes share their visiting statistics to Ahmia</li>
<li>Number of public WWW backlinks to hidden services</li>
<li>Number of clicks in the search results</li>
</ol>

<p>The click counter tells the total number of clicks on a search result in ahmia.fi</p>

<h2>Filtering</h2>

<p>We have decided to filter any sites related to child porn from our search results. Ahmia is removing everything related to these websites. These websites may not be actual child porn sites. They are rather sites where users can post content (forums, file and image uploads etc.) and as the result there have been, momentarily at least, some suspicious content that has not been moderated in a reasonable period of time. Ahmia.fi does not have the time to monitor these sites carefully and we are banning sites from our public index if we see any evidence of child abuse. Of course, the ban is removed if the site itself contacts us and we review the website to be OK.</p>

<p>In practice, Ahmia calculates the MD5 sums of the banned domains for use as a filtering policy. Moreover, we are sharing this list and Tor2web nodes can use the list to filter out pages.</p>

<p>At the moment, there seems to be 1228 hidden website domains online and 7 of them has been filtered because they are possibly sharing child porn content.</p>

<h2>OnionBot</h2>

<p><a href="https://github.com/juhanurmi/ahmia/tree/master/onionbot" rel="nofollow">OnionBot</a> is a crawler for hidden service websites based on the Scrapy framework. It crawls the Tor network and passes data to the search database. OnionBot requires the Tor software (using Tor2web mode) and Polipo. The results are saved to Apache Solr.</p>

<h2>Apache Solr</h2>

<p>Apache Solr is a popular, open source enterprise search platform. Its major features include powerful full-text search, hit highlighting, faceted search, and near real-time indexing.</p>

<p>The <a href="https://github.com/juhanurmi/ahmia/blob/master/solr/schema.xml" rel="nofollow">schema.xml</a> file contains all of the details about which fields your documents can contain, and how those fields should be dealt with when adding documents to the index, or when querying those fields.</p>

<h2>Security measures for privacy</h2>

<p>In the software</p>

<ul>
<li>We do not log any IP addresses, see <a href="https://github.com/juhanurmi/ahmia/tree/master/apache2" rel="nofollow">Apache configuration</a>
</li>
<li>We are gathering real-time clicks, however, this data is not shown accurately</li>
</ul>

<p>In the host ahmia.fi</p>

<ul>
<li>Backend servers are run separately and they do not have any knowledge about the end-users</li>
<li>All servers are hosted in countries with strong privacy laws. For example, Finland and the Netherlands</li>
<li>Communication between servers is encrypted</li>
<li>Only a few trustworthy people know the locations of the back-end servers and are able to access them</li>
</ul>

<h2>Future work</h2>

<p>GSoC 2014 was fun and productive!</p>

<p>  There is a lot more to do. However, I do not have time to do everything myself. Of course, I am coding when I have time and maintaining the search engine.</p>

<p>  In addition, I am going to write a scientific article about the implementation.</p>

<p>  Is there anyone who would be interested in developing Ahmia.fi?</p>

<p>  Is anyone familiar with Solr and would know how to tweak it for full text search?</p>

<p>  Furthermore, any kind of help would be most welcome. There are always Linux admin duties, HTML/CSS design, bug fixing, Django development, etc...</p>

<p><b>For further information, please don't hesitate to contact me by e-mail: <a href="mailto:juha.nurmi@ahmia.fi" rel="nofollow">juha.nurmi@ahmia.fi</a></b></p>

---
_comments:

<a id="comment-74660"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74660" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 09, 2014</p>
    </div>
    <a href="#comment-74660">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74660" class="permalink" rel="bookmark">Will your crawler parse and</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Will your crawler parse and honour "robots.txt" from hidden services for general and/or specific interdictions, if any ?<br />
If your crawler robot is honouring targetted interdiction, how' s the robot name to be spelled in the "robots.txt" ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-74664"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74664" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  juha
  </article>
    <div class="comment-header">
      <p class="comment__submitted">juha said:</p>
      <p class="date-time">September 09, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-74660" class="permalink" rel="bookmark">Will your crawler parse and</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-74664">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74664" class="permalink" rel="bookmark">Yes Ahmia&#039;s OnionBot</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes Ahmia's OnionBot software is honouring robots.txt.</p>
<p>OnionBot is based on Scrapy web crawling framework[1]. I have enabled so called RobotsTxtMiddleware[2].</p>
<p>The documentation does not say much else.</p>
<p>[1] <a href="http://doc.scrapy.org/en/latest/intro/overview.html" rel="nofollow">http://doc.scrapy.org/en/latest/intro/overview.html</a><br />
[2] <a href="http://doc.scrapy.org/en/latest/topics/downloader-middleware.html#topics-dlmw-robots" rel="nofollow">http://doc.scrapy.org/en/latest/topics/downloader-middleware.html#topic…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-74670"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74670" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 09, 2014</p>
    </div>
    <a href="#comment-74670">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74670" class="permalink" rel="bookmark">1. What PT does orbot</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>1. What PT does orbot support?<br />
2. if I direct my dns settings to 127.0.0.1:5400 while tor is running will my system use a torified dns?<br />
3.if I install standalone tor on my computer (via terminal/cmd) can I use it?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-74673"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74673" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">September 09, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-74670" class="permalink" rel="bookmark">1. What PT does orbot</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-74673">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74673" class="permalink" rel="bookmark">This sure doesn&#039;t sound like</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This sure doesn't sound like an ahmia question.</p>
<p>I recommend asking the Guardian people your Orbot questions.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-74680"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74680" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 10, 2014</p>
    </div>
    <a href="#comment-74680">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74680" class="permalink" rel="bookmark">Hi,
https://ahmia.fi does</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi,</p>
<p><a href="https://ahmia.fi" rel="nofollow">https://ahmia.fi</a> does not support forward secrecy :(</p>
<p><a href="https://www.ssllabs.com/ssltest/analyze.html?d=ahmia.fi" rel="nofollow">https://www.ssllabs.com/ssltest/analyze.html?d=ahmia.fi</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-74700"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74700" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  juha
  </article>
    <div class="comment-header">
      <p class="comment__submitted">juha said:</p>
      <p class="date-time">September 11, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-74680" class="permalink" rel="bookmark">Hi,
https://ahmia.fi does</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-74700">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74700" class="permalink" rel="bookmark">Support to forward secrecy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Support to forward secrecy added :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-74682"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74682" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 10, 2014</p>
    </div>
    <a href="#comment-74682">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74682" class="permalink" rel="bookmark">I understand the importance</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I understand the importance of Tor, but I also see that using it, as opposed to other browser set-ups, instantly cuts out about 60% of the current Internet. Search engines on web sites that use a Google plug-in search engine suddenly are inaccessible. Comment sections become inaccessible. Videos and audio files become inaccessible. Video and audio web sites become inaccessible. Retail sites become inaccessible. The Internet is suddenly devolved back to 1992 or perhaps 1986. There comes a point at which anonymity begins to blur with the tinfoil hat brigade. It is right that no government, corporation or neighbor should be able to access our private information. It is also right that very little that you do is important enough for anyone else's scrutiny. I value my privacy, but I'm the only one. Not because I have so much at stake but because no one else cares, nor should. Right now, Tor is too anonymous. It stops being useful as a general web browser. Using Tor is not going to force the rest of the world to concede to our desires for anonymity. They're too busy selling smart phones and pet meds to the 99% who don't care about their privacy. Unless you can change the behavior of enough Internet users to have an economic impact on the perpetrators, privacy is a non-issue. That makes Tor just an annoying curiosity, like Dungeons &amp; Dragons or Warhammer 40,000.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-74809"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74809" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 12, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-74682" class="permalink" rel="bookmark">I understand the importance</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-74809">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74809" class="permalink" rel="bookmark">Would anyone but a troll</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Would anyone but a troll start their speech claiming<br />
     "I understand the importance of Tor",<br />
and then nowhere in their entire  _l_o_n_g_  unbroken paragraph make any attempt to show they might "understand the importance of Tor"??<br />
:-)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-74995"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74995" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 15, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-74682" class="permalink" rel="bookmark">I understand the importance</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-74995">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74995" class="permalink" rel="bookmark">This issue was discussed in</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This issue was discussed in a recent blog post, see <a href="https://blog.torproject.org/blog/call-arms-helping-internet-services-accept-anonymous-users" rel="nofollow">https://blog.torproject.org/blog/call-arms-helping-internet-services-ac…</a> for more information on how you can get involved in solving int.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-74690"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74690" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 10, 2014</p>
    </div>
    <a href="#comment-74690">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74690" class="permalink" rel="bookmark">ahmia.fi site trying extra</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>ahmia.fi site trying extra canvas data when I click a link on the Statistics Viewer page?  NOT COOL.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-74696"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74696" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  juha
  </article>
    <div class="comment-header">
      <p class="comment__submitted">juha said:</p>
      <p class="date-time">September 11, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-74690" class="permalink" rel="bookmark">ahmia.fi site trying extra</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-74696">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74696" class="permalink" rel="bookmark">Yes, there are pages that</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, there are pages that are using JavaScipt and even canvas. These pages are showing some stats visualizations. </p>
<p>I have made sure that the main features of Ahmia are working without JavaScript.</p>
<p>Unfortunately, it is the easiest way for me to make the visualizations with JavaScript. That's why it is used in some pages.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-74708"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74708" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 11, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to juha</p>
    <a href="#comment-74708">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74708" class="permalink" rel="bookmark">understood and you are not</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>understood and you are not alone in these respects (e.g. Atlas uses javascript), but my opinion is that applications specifically designed for tbb users should always aim for full functionality without the use of browser features that increase a user's attack surface.  </p>
<p>not saying your implementation is any sort of threat per se, but affiliated projects should not be in the business of prompting people to make themselves more vulnerable to use your site and have to remember to change things back again once they're off-site. this is especially true for a search engine, where clicking through to other sites is what people are going to be doing. i think it encourages user behaviors that aren't good for their security.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-74756"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74756" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 12, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to juha</p>
    <a href="#comment-74756">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74756" class="permalink" rel="bookmark">Do &quot;the main features of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do "the main features of Ahmia" include the abuse notifier button?  (See my other comment; I am not the person who commented about canvas, but I will agree, NOT COOL.)</p>
<p>I think this is the first example I have seen first-hand that <strong>JavaScript helps CP spread</strong> (because dedicated privacy activists with a strong security posture will not be able to use the abuse button).  I always joked that "JavaScript kicks puppies" and the like, but this is worse and it is not a joke.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-74691"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74691" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 10, 2014</p>
    </div>
    <a href="#comment-74691">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74691" class="permalink" rel="bookmark">Why didn&#039;t you opt for a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why didn't you opt for a decentralized design like yacy.net?  It seems like it would be easier to build and a great way to offer mutual aid and solidarity to an open source project already focused on decentralized search..</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-74697"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74697" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  juha
  </article>
    <div class="comment-header">
      <p class="comment__submitted">juha said:</p>
      <p class="date-time">September 11, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-74691" class="permalink" rel="bookmark">Why didn&#039;t you opt for a</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-74697">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74697" class="permalink" rel="bookmark">That was my plan[1] before I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That was my plan[1] before I decided to code my own OnionBot software. The idea is great and fascinating. However, there are few bugs or features in YaCy software that should be fixed first.</p>
<p>[1] <a href="https://github.com/juhanurmi/ahmia/issues/14" rel="nofollow">https://github.com/juhanurmi/ahmia/issues/14</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-74707"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74707" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 11, 2014</p>
    </div>
    <a href="#comment-74707">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74707" class="permalink" rel="bookmark">Sorry to say, but services</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry to say, but services like CloudFlare are killing Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-74709"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74709" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 11, 2014</p>
    </div>
    <a href="#comment-74709">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74709" class="permalink" rel="bookmark">what was broken with YaCY?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>what was broken with YaCY? the link provided doesn't really explain your thought process</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-74714"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74714" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  juha
  </article>
    <div class="comment-header">
      <p class="comment__submitted">juha said:</p>
      <p class="date-time">September 11, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-74709" class="permalink" rel="bookmark">what was broken with YaCY?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-74714">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74714" class="permalink" rel="bookmark">One of the issues was</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>One of the issues was <a href="http://forum.yacy-websuche.de/viewtopic.php?f=8&amp;t=4845" rel="nofollow">http://forum.yacy-websuche.de/viewtopic.php?f=8&amp;t=4845</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-74761"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74761" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 12, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to juha</p>
    <a href="#comment-74761">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74761" class="permalink" rel="bookmark">interesting--thanks!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>interesting--thanks!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-74724"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74724" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 11, 2014</p>
    </div>
    <a href="#comment-74724">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74724" class="permalink" rel="bookmark">hi,everyone
Tor browser can</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi,everyone<br />
Tor browser can successfully connect  to the tor network , unfortunately tor Browser occasionally fails to open, why?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-74744"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-74744" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 12, 2014</p>
    </div>
    <a href="#comment-74744">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-74744" class="permalink" rel="bookmark">use nginx ;)</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>use nginx ;)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-75018"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-75018" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 15, 2014</p>
    </div>
    <a href="#comment-75018">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-75018" class="permalink" rel="bookmark">Am I correct that your word</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Am I correct that your word cloud visualization also omits any references to CP?</p>
<p>Isn't that sort of lying to yourself? I don't think leaving out one of the biggest parts of hidden services is a very scientific approach just because we may not like to acknowledge it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-75025"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-75025" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  juha
  </article>
    <div class="comment-header">
      <p class="comment__submitted">juha said:</p>
      <p class="date-time">September 15, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-75018" class="permalink" rel="bookmark">Am I correct that your word</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-75025">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-75025" class="permalink" rel="bookmark">There were 7-9 hidden</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There were 7-9 hidden services filtered when the content analysis was made. This probably didn't have any effect to the results because there are almost 1300 hidden service websites.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-75380"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-75380" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 19, 2014</p>
    </div>
    <a href="#comment-75380">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-75380" class="permalink" rel="bookmark">If you discriminate against</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you discriminate against pedophiles by filtering out their sites, thus censoring the hidden web, what makes you any better than those who would censor the entire internet to discriminate against any minority or prevent people knowing about any number of things? Pedophilia is not the only thing people find objectionable, nor is it the only thing associated with abuse. </p>
<p>Sure, you may find CP to be awful stuff, but hiding it does nothing to prevent child abuse, nor does it help the abused in any meaningful way. It only is a way of "sweeping it under the rug", so that people can feel better in their ignorance. It also endangers freedom of information for all of us. See <a href="https://falkvinge.net/2012/09/07/three-reasons-child-porn-must-be-re-legalized-in-the-coming-decade/" rel="nofollow">https://falkvinge.net/2012/09/07/three-reasons-child-porn-must-be-re-le…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-76355"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-76355" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">October 04, 2014</p>
    </div>
    <a href="#comment-76355">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-76355" class="permalink" rel="bookmark">Two questions I never really</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Two questions I never really expect to be answered:</p>
<p>Why doesn't Ahmia have a .onion url?</p>
<p>Ahmia's main page says as of 4 Oct that there are 1270 hidden services, but according to skunksworkedp2cg.onion there are 1479 hidden sites, and assuming that they also respect robots.txt, while you claim to be only censoring 12 ATM, that leaves almost 200 hidden sites unaccounted for, how do you explain this?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-77346"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-77346" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  juha
  </article>
    <div class="comment-header">
      <p class="comment__submitted">juha said:</p>
      <p class="date-time">October 24, 2014</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-76355" class="permalink" rel="bookmark">Two questions I never really</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-77346">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-77346" class="permalink" rel="bookmark">1) Ahmia has an onion URL:</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>1) Ahmia has an onion URL: <a href="http://msydqstlz2kzerdg.onion/" rel="nofollow">http://msydqstlz2kzerdg.onion/</a></p>
<p>2) Ahmia marks each hidden service offline that haven't been answering to HTTP requests within a week.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
