title: Tor 0.2.9.6-rc is released
---
pub_date: 2016-12-02
---
author: nickm
---
tags:

tor
release
release candidate
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.9.6-rc fixes a few remaining bugs found in the previous alpha version. We hope that it will be ready to become stable soon, and we encourage everyone to test this release. If no showstopper bugs are found here, the next 0.2.9 release will be stable.</p>

<p>You can download the source from the usual place on the website. Packages should be available over the next several days, including an alpha TorBrowser release around December 14. Remember to check the signatures!</p>

<p>Please note: This is a release candidate. I think it's pretty stable, but bugs can always remain. If you want a stable experience, please stick to the stable releases.</p>

<p>Below are the changes since 0.2.9.5-alpha.</p>

<h2>Changes in version 0.2.9.6-rc - 2016-12-02</h2>

<ul>
<li>Major bugfixes (relay, resolver, logging):
<ul>
<li>For relays that don't know their own address, avoid attempting a local hostname resolve for each descriptor we download. This will cut down on the number of "Success: chose address 'x.x.x.x'" log lines, and also avoid confusing clock jumps if the resolver is slow. Fixes bugs 20423 and 20610; bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (client, fascistfirewall):
<ul>
<li>Avoid spurious warnings when ReachableAddresses or FascistFirewall is set. Fixes bug <a href="https://bugs.torproject.org/20306" rel="nofollow">20306</a>; bugfix on 0.2.8.2-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (hidden services):
<ul>
<li>Stop ignoring the anonymity status of saved keys for hidden services and single onion services when first starting tor. Instead, refuse to start tor if any hidden service key has been used in a different hidden service anonymity mode. Fixes bug <a href="https://bugs.torproject.org/20638" rel="nofollow">20638</a>; bugfix on 17178 in 0.2.9.3-alpha; reported by ahf.
  </li>
</ul>
</li>
<li>Minor bugfixes (portability):
<ul>
<li>Work around a bug in the OSX 10.12 SDK that would prevent us from successfully targeting earlier versions of OSX. Resolves ticket <a href="https://bugs.torproject.org/20235" rel="nofollow">20235</a>.
  </li>
<li>Run correctly when built on Windows build environments that require _vcsprintf(). Fixes bug <a href="https://bugs.torproject.org/20560" rel="nofollow">20560</a>; bugfix on 0.2.2.11-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (single onion services, Tor2web):
<ul>
<li>Stop complaining about long-term one-hop circuits deliberately created by single onion services and Tor2web. These log messages are intended to diagnose issue 8387, which relates to circuits hanging around forever for no reason. Fixes bug <a href="https://bugs.torproject.org/20613" rel="nofollow">20613</a>; bugfix on 0.2.9.1-alpha. Reported by "pastly".
  </li>
</ul>
</li>
<li>Minor bugfixes (unit tests):
<ul>
<li>Stop spurious failures in the local interface address discovery unit tests. Fixes bug <a href="https://bugs.torproject.org/20634" rel="nofollow">20634</a>; bugfix on 0.2.8.1-alpha; patch by Neel Chauhan.
  </li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Correct the minimum bandwidth value in torrc.sample, and queue a corresponding change for torrc.minimal. Closes ticket <a href="https://bugs.torproject.org/20085" rel="nofollow">20085</a>.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-224282"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224282" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2016</p>
    </div>
    <a href="#comment-224282">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224282" class="permalink" rel="bookmark">When will Tor Browser 64-bit</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>When will Tor Browser 64-bit be released for Windows?</p>
<p>32-bit version is too vulnerable against malware.. even open-source banking trojan zeus supports man in the browser for Tor Browser.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224363"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224363" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 08, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224282" class="permalink" rel="bookmark">When will Tor Browser 64-bit</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224363">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224363" class="permalink" rel="bookmark">Mozilla started a while back</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Mozilla started a while back to ship 64bit versions of Firefox and we followed that experiment closely. We thought it was premature to offer builds based on ESR 45, though, and looking at the crashes that occurred with 64bit Firefox builds it seems that was not a bad idea.</p>
<p>We will reevaluate that decision with the switch to Firefox ESR 52 which will happen in the first and second quarter in 2017. The current plan is to have 64 bit Tor Browser bundles for Windows ready in the 7.5 alpha series, fix issues we find and provide 64bit stable bundles for Windows user starting with Tor Browser 7.5.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
