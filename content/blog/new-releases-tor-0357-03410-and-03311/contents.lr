title: New Releases: Tor 0.3.5.7, 0.3.4.10, and 0.3.3.11
---
pub_date: 2019-01-08
---
author: nickm
---
tags: releases
---
categories: releases
---
summary: Tor 0.3.5.7 is the first stable release in its series; it includes compilation and portability fixes, and a fix for a severe problem affecting directory caches. Tor 0.3.4.10 and 0.3.3.11 are also released today; please see the official announcements for those releases if you are tracking older stable versions.
---
_html_body:

<p>Tor 0.3.5.7 is the first stable release in its series; it includes compilation and portability fixes, and a fix for a severe problem affecting directory caches. <a href="https://lists.torproject.org/pipermail/tor-announce/2019-January/000171.html">Tor 0.3.4.10 and 0.3.3.11</a> are also released today; please see the official announcements for those releases if you are tracking older stable versions.</p>
<p>The Tor 0.3.5 series includes several new features and performance improvements, including client authorization for v3 onion services, cleanups to bootstrap reporting, support for improved bandwidth- measurement tools, experimental support for NSS in place of OpenSSL, and much more. It also begins a full reorganization of Tor's code layout, for improved modularity and maintainability in the future. Finally, there is the usual set of performance improvements and bugfixes that we try to do in every release series.</p>
<p>There are a couple of changes in the 0.3.5 that may affect compatibility. First, the default version for newly created onion services is now v3. Use the HiddenServiceVersion option if you want to override this. Second, some log messages related to bootstrapping have changed; if you use stem, you may need to update to the latest version so it will recognize them.</p>
<p>We have designated 0.3.5 as a "long-term support" (LTS) series: we will continue to patch major bugs in typical configurations of 0.3.5 until at least 1 Feb 2022. (We do not plan to provide long-term support for embedding, Rust support, NSS support, running a directory authority, or unsupported platforms. For these, you will need to stick with the latest stable release.)</p>
<p>Below are the changes since 0.3.5.6-rc. For a complete list of changes since 0.3.4.9, see the ReleaseNotes file.</p>
<h2>Changes in version 0.3.5.7 - 2019-01-07</h2>
<ul>
<li>Major bugfixes (relay, directory):
<ul>
<li>Always reactivate linked connections in the main loop so long as any linked connection has been active. Previously, connections serving directory information wouldn't get reactivated after the first chunk of data was sent (usually 32KB), which would prevent clients from bootstrapping. Fixes bug <a href="https://bugs.torproject.org/28912">28912</a>; bugfix on 0.3.4.1-alpha. Patch by "cypherpunks3".</li>
</ul>
</li>
<li>Minor features (compilation):
<ul>
<li>When possible, place our warning flags in a separate file, to avoid flooding verbose build logs. Closes ticket <a href="https://bugs.torproject.org/28924">28924</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the January 3 2019 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/29012">29012</a>.</li>
</ul>
</li>
<li>Minor features (OpenSSL bug workaround):
<ul>
<li>Work around a bug in OpenSSL 1.1.1a, which prevented the TLS 1.3 key export function from handling long labels. When this bug is detected, Tor will disable TLS 1.3. We recommend upgrading to a version of OpenSSL without this bug when it becomes available. Closes ticket <a href="https://bugs.torproject.org/28973">28973</a>.</li>
</ul>
</li>
<li>Minor features (performance):
<ul>
<li>Remove about 96% of the work from the function that we run at startup to test our curve25519_basepoint implementation. Since this function has yet to find an actual failure, we now only run it for 8 iterations instead of 200. Based on our profile information, this change should save around 8% of our startup time on typical desktops, and may have a similar effect on other platforms. Closes ticket <a href="https://bugs.torproject.org/28838">28838</a>.</li>
<li>Stop re-validating our hardcoded Diffie-Hellman parameters on every startup. Doing this wasted time and cycles, especially on low-powered devices. Closes ticket <a href="https://bugs.torproject.org/28851">28851</a>.</li>
</ul>
</li>
<li>Minor bugfixes (compilation):
<ul>
<li>Fix compilation for Android by adding a missing header to freespace.c. Fixes bug <a href="https://bugs.torproject.org/28974">28974</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (correctness):
<ul>
<li>Fix an unreached code path where we checked the value of "hostname" inside send_resolved_hostname_cell(). Previously, we used it before checking it; now we check it first. Fixes bug <a href="https://bugs.torproject.org/28879">28879</a>; bugfix on 0.1.2.7-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Make sure that test_rebind.py actually obeys its timeout, even when it receives a large number of log messages. Fixes bug <a href="https://bugs.torproject.org/28883">28883</a>; bugfix on 0.3.5.4-alpha.</li>
<li>Stop running stem's unit tests as part of "make test-stem", but continue to run stem's unit and online tests during "make test- stem-full". Fixes bug <a href="https://bugs.torproject.org/28568">28568</a>; bugfix on 0.2.6.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (windows services):
<ul>
<li>Make Tor start correctly as an NT service again: previously it was broken by refactoring. Fixes bug <a href="https://bugs.torproject.org/28612">28612</a>; bugfix on 0.3.5.3-alpha.</li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>When parsing a port configuration, make it more obvious to static analyzer tools that we always initialize the address. Closes ticket <a href="https://bugs.torproject.org/28881">28881</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-279288"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279288" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 07, 2019</p>
    </div>
    <a href="#comment-279288">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279288" class="permalink" rel="bookmark">First, the default version…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>First, the default version for newly created onion services is now v3.</p></blockquote>
<p>Now we can call v3 onion services stable and worth deploying! Congratulations!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279290"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279290" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 08, 2019</p>
    </div>
    <a href="#comment-279290">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279290" class="permalink" rel="bookmark">Changehttps://gitweb…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Change<br />
<a href="https://gitweb.torproject.org/tor.git/plain/ChangeLog" rel="nofollow">https://gitweb.torproject.org/tor.git/plain/ChangeLog</a><br />
to<br />
<a href="https://gitweb.torproject.org/tor.git/plain/ChangeLog?id=tor-0.3.5.7" rel="nofollow">https://gitweb.torproject.org/tor.git/plain/ChangeLog?id=tor-0.3.5.7</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-279306"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279306" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>John (not verified)</span> said:</p>
      <p class="date-time">January 09, 2019</p>
    </div>
    <a href="#comment-279306">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279306" class="permalink" rel="bookmark">It would be nice to include…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It would be nice to include a GUI (Graphical User Interface) in the Windows version (0.3.5.x), since us (Windows Users) like graphical everything possible (otherwise we would be using other OS's).</p>
<p>Should be easy to configure all in the same graphical Window for example or easy menus, and have options to download updates/ upgrades automatically (on by default) and/ or manually.</p>
<p>Everything should include deep explanations with pictures so that users understand exactly what is/ will happen.</p>
<p>Should include tools to check if Ports are correctly open.</p>
<p>Should include tool to check if Windows Time is correct and offer to correct right there, and maybe allow user to have it update it every day or so (should work even in special Internet connections like satellite where NTP normally is blocked).</p>
<p>If possible should show visually how much bandwidth it used, how was the traffic over time, so that users understand it better.</p>
<p>Users like things to detect has much parameters as possible, so make sure the GUI does as much detection as possible to make it easy, but allow expert mode to set/ correct parameters.</p>
<p>These should allow user to use it more... to host Onion web sites (with the appropriated web server software), to create Onion Bridges, to create Onion Relays, or to create Onion Exit's.</p>
<p>Since there are a ton of Windows users out there I think these will help make them use it more.<br />
Should also be included with TorBrowser bundle to make them test and maybe use it.</p>
<p>So all of these to make onion network grow.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279405"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279405" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Ryan (not verified)</span> said:</p>
      <p class="date-time">January 16, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279306" class="permalink" rel="bookmark">It would be nice to include…</a> by <span>John (not verified)</span></p>
    <a href="#comment-279405">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279405" class="permalink" rel="bookmark">I am in complete agreement…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am in complete agreement. Maybe the Tor Team should revive Vidalia.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-279389"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279389" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>ern (not verified)</span> said:</p>
      <p class="date-time">January 14, 2019</p>
    </div>
    <a href="#comment-279389">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279389" class="permalink" rel="bookmark">$ gpg --verify tor-0.3.5.7…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>$ gpg --verify tor-0.3.5.7.tar.gz.asc<br />
gpg: assuming signed data in 'tor-0.3.5.7.tar.gz'<br />
gpg: Signature made Mon 07 Jan 2019 02:37:18 PM MST<br />
gpg:                using RSA key 6AFEE6D49E92B601</p>
<p>But a search for 6AFEE6D49E92B601 at<br />
<a href="https://www.torproject.org/docs/signing-keys.html.en" rel="nofollow">https://www.torproject.org/docs/signing-keys.html.en</a><br />
finds nothing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279395"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279395" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">January 15, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279389" class="permalink" rel="bookmark">$ gpg --verify tor-0.3.5.7…</a> by <span>ern (not verified)</span></p>
    <a href="#comment-279395">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279395" class="permalink" rel="bookmark">It&#039;s a subkey of 4096R…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's a subkey of 4096R/FE43009C4607B1FB.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-279420"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279420" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 18, 2019</p>
    </div>
    <a href="#comment-279420">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279420" class="permalink" rel="bookmark">see the ReleaseNotes file.
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>see the ReleaseNotes file.</p></blockquote>
<p>It would help if the post links to it: <a href="https://gitweb.torproject.org/tor.git/log/ReleaseNotes" rel="nofollow">https://gitweb.torproject.org/tor.git/log/ReleaseNotes</a></p>
<p>In the <a href="https://gitweb.torproject.org/tor.git/commit/ReleaseNotes?id=ba5889011853bc1d86892b5379a87766b0380be5" rel="nofollow">ReleaseNotes</a> and <a href="https://gitweb.torproject.org/tor.git/commit/ChangeLog?id=ba5889011853bc1d86892b5379a87766b0380be5" rel="nofollow">ChangeLog</a>, the year is wrong:</p>
<p>Changes in version 0.3.3.11 - 2018-01-07<br />
Changes in version 0.3.4.10 - 2018-01-07</p>
<p>And part of the first paragraph for 0.3.3.11 is missing. A sentence begins mid-sentence.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-279427"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-279427" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  nickm
  </article>
    <div class="comment-header">
      <p class="comment__submitted">nickm said:</p>
      <p class="date-time">January 18, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-279420" class="permalink" rel="bookmark">see the ReleaseNotes file.
…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-279427">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-279427" class="permalink" rel="bookmark">Thanks; I&#039;ve fixed the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks; I've fixed the release dates.</p>
<p>For the links, I'll have to tweak the script that generates the HTML on these announcements.  Making a TODO item there.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
