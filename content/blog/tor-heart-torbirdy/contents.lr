title: Tor at the Heart: TorBirdy
---
pub_date: 2016-12-01
---
author: ssteele
---
tags:

TorBirdy
heart of Internet freedom
---
_html_body:

<p><i>During the month of December, we're highlighting other organizations and projects that rely on Tor, build on Tor, or are accomplishing their missions better because Tor exists. Check out our blog each day to learn about our fellow travelers. And please support the Tor Project! We're at the heart of Internet freedom.</i><br />
<a href="https://torproject.org/donate/donate-blog1" rel="nofollow">Donate today!</a></p>

<p><b>TorBirdy</b></p>

<p>TorBirdy automatically connects you through the Tor network whenever you log into Thunderbird email. TorBirdy also enhances the privacy settings of Thunderbird and configures it for use over the Tor network. This makes it so your location is anonymous when you check and send your email, making it more difficult for companies or governments to assemble a profile of your online activity.</p>

<p>Under normal circumstances, your email provider can see your IP address whenever you check your email. In addition, your IP address is imprinted within the header of the message whenever you send an email, so the email recipient can see it. TorBirdy reroutes your email through the Tor network, effectively bouncing it around different computers across the globe before delivering it. Your email provider and the recipient of the email will see your IP address as being from a random location rather than your actual location. If you set up an email account over Tor and check your email using TorBirdy, your email can't be related back to you, greatly increasing your anonymity when it comes to using email.</p>

<p>TorBirdy is an extension for ​Mozilla Thunderbird that is still in beta, but it is already available in 27 languages. You can <a href="https://dist.torproject.org/torbirdy/" rel="nofollow">download it</a> from the Tor Project's website. Tails also ships with TorBirdy.</p>

---
_comments:

<a id="comment-223610"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223610" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 01, 2016</p>
    </div>
    <a href="#comment-223610">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223610" class="permalink" rel="bookmark">Great extension! :)
Haven&#039;t</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great extension! :)</p>
<p>Haven't tested it yet but does it have a similar feature like "Privacy and Security Settings" as in Tor browser? Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-223648"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223648" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 01, 2016</p>
    </div>
    <a href="#comment-223648">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223648" class="permalink" rel="bookmark">hi why isn&#039;t</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hi why isn't 0x4e2c6e8793298290 available for download on the main site?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223761"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223761" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223648" class="permalink" rel="bookmark">hi why isn&#039;t</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223761">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223761" class="permalink" rel="bookmark">What main site do you  mean?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What main site do you  mean? Keys are listed on our site: <a href="https://www.torproject.org/docs/signing-keys.html.en" rel="nofollow">https://www.torproject.org/docs/signing-keys.html.en</a> (and that one is there as well). To import the key you can do something like gpg --recv 0x4e2c6e8793298290.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-223686"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223686" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 01, 2016</p>
    </div>
    <a href="#comment-223686">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223686" class="permalink" rel="bookmark">Need some help guys... how</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Need some help guys... how can i use this tor significantly?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-225398"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225398" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 15, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223686" class="permalink" rel="bookmark">Need some help guys... how</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-225398">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225398" class="permalink" rel="bookmark">I interpret the question to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I interpret the question to mean: how can I get started using Tor to improve my cybersecurity against corporate snoopers or even against intelligence services foreign or domestic?</p>
<p>Two easy ways to use Tor for something useful---even essential--- which would be dangerously insecure if you are not using Tor:</p>
<p>o download and use Tor Browser from torproject.org; this provides a standalone package enabling you to browse the web using Tor, essentially the same way you already use a non-Torified webbrowser,</p>
<p>o download and use Tails from tails.boum.org; this provides a complex Linux OS (closely based on Debian) which prevents traces of computer activity such as writing a whistleblowing letter from being written to your hard drive, which could get you fired or worse, and enables many other things described in their documentation page; Tails also comes with the latest version of Tor Browser so you can surf the web, use email, use Office type programs, and you can use mat to remove metadata from image files, etc, etc; see also the "Tor at Heart" post in this blog on Tails.</p>
<p>Hope this helps!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-223742"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223742" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 01, 2016</p>
    </div>
    <a href="#comment-223742">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223742" class="permalink" rel="bookmark">Some ISPs downgrade STARTLS.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Some ISPs downgrade STARTLS. Any possibility of something like HSTS or "HTTPS Only" addons to protect concerned citizens from brutal dictators?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223797"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223797" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 02, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223742" class="permalink" rel="bookmark">Some ISPs downgrade STARTLS.</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223797">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223797" class="permalink" rel="bookmark">i. You can enforce a</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>i. You can enforce a stronger encryption (resp. prevent a downgrade) to your email provider but the from than your email provider the email provider of your contact a free to negotiate an outdated cipher</p>
<p>ii. torbirdy enforces tls 1.1 (the last rfc recommends ciphers that just available in tls 1.2, tls 1.1 still better than nothing)</p>
<p>iii. you can use torbirdy and it's setting without using Tor! --&gt;<br />
{{{open torbirdy}}} --&gt; choose {{{Transparent Torification}}}</p>
<p>or </p>
<p>iii.set it manually in torbirdy's {{{about:config}}} (that works for tormessenger, too, in contrast to tbb tracking you in that much of an issue cos you already have an account, anyway),see <a href="https://trac.torproject.org/projects/tor/ticket/20751" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/20751</a>,</p>
<p>{{{<br />
 security.tls.version.min = 3 enforce tls v 1.2<br />
security.ssl3.* false<br />
security.ssl3.ecdhe_rsa_aes_128_gcm_sha256 true<br />
security.ssl3.ecdhe_ecdsa_aes_128_gcm_sha256 true</p>
<p>prevent insecure recognition<br />
security.ssl.require_safe_negotiation true<br />
security.ssl.treat_unsafe_negotiation_as_broken true</p>
<p>strict key pinning [1]<br />
security.cert_pinning.enforcement_level 2<br />
}}}</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-223882"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223882" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 03, 2016</p>
    </div>
    <a href="#comment-223882">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223882" class="permalink" rel="bookmark">If you&#039;re still looking for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you're still looking for projects to feature for <i>Tor at the Heart</i>, Tokumei is a big fan: <a href="https://tokumei.co/" rel="nofollow">https://tokumei.co/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-223896"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-223896" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 03, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-223882" class="permalink" rel="bookmark">If you&#039;re still looking for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-223896">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-223896" class="permalink" rel="bookmark">Neat. Can you summarize what</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Neat. Can you summarize what it is, and how it makes use of Tor?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224326"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224326" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-224326">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224326" class="permalink" rel="bookmark">Tokumei is a free,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tokumei is a free, anonymous, self-hosted microblogging platform. The hosting docs encourage users to run Tokumei sites as Tor hidden services and the process is scripted and documented. Public Tokumei sites are indexed with a preference for hidden services and guides for end users to install a Tor browser and connect.</p>
<p><a href="https://tokumei.co/hosting/public-tor" rel="nofollow">https://tokumei.co/hosting/public-tor</a><br />
<a href="https://github.com/tokumeico/tokumei-www/blob/master/pubtor.sh#L79-L86" rel="nofollow">https://github.com/tokumeico/tokumei-www/blob/master/pubtor.sh#L79-L86</a><br />
<a href="https://tokumei.co/sites/examplesite/" rel="nofollow">https://tokumei.co/sites/examplesite/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div>
