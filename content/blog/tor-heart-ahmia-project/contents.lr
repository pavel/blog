title: Tor at the Heart: The Ahmia project
---
pub_date: 2016-12-05
---
author: asn
---
tags:

ahmia
onion services
heart of Internet freedom
---
categories: onion services
---
_html_body:

<p><i>During the month of December, we're highlighting other organizations and projects that rely on Tor, build on Tor, or are accomplishing their missions better because Tor exists. Check out our blog each day to learn about our fellow travelers. And please support the Tor Project! We're at the heart of Internet freedom.</i><br />
<a href="https://torproject.org/donate/donate-blog5" rel="nofollow">Donate today</a>!</p>

<p><a href="https://ahmia.fi" rel="nofollow"><br />
<img src="https://extra.torproject.org/blog/2016-12-05-tor-heart-ahmia-project/ahmialogo.png" /><br />
</a></p>

<p><b>The Ahmia project</b></p>

<p>Onion services are <a href="https://metrics.torproject.org/hidserv-dir-onions-seen.html" rel="nofollow">used by thousands of people every day</a>, yet they remain as elusive as ever. There is no central repository of onion sites, and there are no great ways to find the content you are looking for. We feel that this "foggy situation" severely impacts the user experience of onion services and hence also impedes their deployment and <i>acceptance by the general public</i>. It's easy to dismiss the onionspace as smelly if you only read media articles about the onion sites that stink the most.</p>

<p><i>How is one supposed to navigate in the onionspace if there is no map?</i></p>

<p>On the "normal Internet," people are used to using search engines to find the content they are looking for: blogs, shops, educational resources, cat pictures. Search engines act as streetlights on the dark alleyways of the Internet; allowing people to navigate and visit the places they want.</p>

<p>However in the onionspace, search engines are not well established, and finding the right content is much harder. For years people have resorted to various <a href="https://www.reddit.com/r/onions/" rel="nofollow">DIY solutions</a> for listing and finding onion addresses, but none of those solutions is particularly pleasant or complete.</p>

<p>Imagine Alice wants to start a blog about her cats on the onionspace. There is no good place for Alice to list her onion address so that other people can find it. Without a good search engine, it's hard for other cat fans to find her website and start building a community.</p>

<p><i>How is one supposed to catch 'em all if we don't know how many there are?</i></p>

<p>Hence, there is no better time to introduce <a href="https://ahmia.fi/" rel="nofollow">Ahmia</a>! Ahmia is a <b>search engine for onion sites</b>. The Ahmia project has been around for years, and it's been collecting public onion addresses and indexing them so that users can search for the content they are looking for.</p>

<p>Ahmia's indexing technology is improving, and the quality of the search results has gotten much better over the past year. Ahmia also provides <a href="https://ahmia.fi/add/" rel="nofollow"> an easy way</a> for onion service operators to register their own onion sites with the search engine. Ahmia's onion site is <a href="http://msydqstlz2kzerdg.onion/" rel="nofollow">here</a>.</p>

<p><a href="https://github.com/juhanurmi" rel="nofollow">Juha Nurmi</a>, the lead Ahmia developer, is still actively involved with the project, however writing a <b>low-budget search engine</b> is not an easy job! Crawling the Internet requires heavy infrastructure and is technically complicated. Discovering onion links means searching in the deepest corners of both the normal Internet and the onionspace. Ahmia is always looking for more volunteers and sources of funding! Two years ago, Tor supported Ahmia by working together in <a href="https://blog.torproject.org/blog/ahmia-search-after-gsoc-development" rel="nofollow">Google Summer of Code 2014</a>.</p>

<p> <i>How is one supposed to walk around if the fog machine is on?</i></p>

<p>Finally and closing with a healthy dose of <i>paranoia</i>, we need to remember that <b>centralized search engines</b> might be a temporary solution for now, but they are never the end goal. Centralized services <a href="https://en.wikipedia.org/wiki/Censorship_by_Google" rel="nofollow">should be avoided</a> in high-security systems like anonymity networks, and we should always strive to build <a href="https://en.wikipedia.org/wiki/Distributed_search_engine" rel="nofollow">decentralized systems</a> and <a href="https://lists.torproject.org/pipermail/tor-dev/2016-October/011514.html" rel="nofollow">to research alternative ways to make anonymity systems more usable</a>. There is lots of work to be done.<i></i></p>

<p><a href="https://torproject.org/donate/donate-blog5" rel="nofollow">Donate</a> and get involved! </p>

<p>Thank you for reading and enjoy Monday!</p>

---
_comments:

<a id="comment-224013"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224013" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 05, 2016</p>
    </div>
    <a href="#comment-224013">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224013" class="permalink" rel="bookmark">Thanks for this blog post</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for this blog post George! :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224018"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224018" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 05, 2016</p>
    </div>
    <a href="#comment-224018">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224018" class="permalink" rel="bookmark">Does Ahmia have an onion</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does Ahmia have an onion link itself?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224020"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224020" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  asn
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">George Kadianakis</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/33">George Kadianakis</a> said:</p>
      <p class="date-time">December 05, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224018" class="permalink" rel="bookmark">Does Ahmia have an onion</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224020">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224020" class="permalink" rel="bookmark">It does!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It does! <a href="http://msydqstlz2kzerdg.onion/" rel="nofollow">http://msydqstlz2kzerdg.onion/</a><br />
Enjoy!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224104"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224104" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224020" class="permalink" rel="bookmark">It does!</a> by <a class="tor" title="View user profile." href="/user/33">George Kadianakis</a></p>
    <a href="#comment-224104">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224104" class="permalink" rel="bookmark">No it doesn&#039;t.  That link</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No it doesn't.  That link times out.  Worthless.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224253"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224253" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224104" class="permalink" rel="bookmark">No it doesn&#039;t.  That link</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224253">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224253" class="permalink" rel="bookmark">&quot;Ahmia is a one-person</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Ahmia is a one-person project right now, so some patience and understanding is needed. Thanks :)"</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-224019"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224019" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 05, 2016</p>
    </div>
    <a href="#comment-224019">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224019" class="permalink" rel="bookmark">Ahmia has even an i2p</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ahmia has even an i2p search! Check: ahmia.fi/i2p/</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224106"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224106" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224019" class="permalink" rel="bookmark">Ahmia has even an i2p</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224106">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224106" class="permalink" rel="bookmark">No it doesn&#039;t.  That link</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No it doesn't.  That link returns a "502 Bad Gateway<br />
nginx/1.6.2" message.</p>
<p>Please do not post useless advice.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224252"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224252" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224106" class="permalink" rel="bookmark">No it doesn&#039;t.  That link</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224252">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224252" class="permalink" rel="bookmark">&quot;Ahmia is a one-person</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Ahmia is a one-person project right now, so some patience and understanding is needed. Thanks :)"</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-224036"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224036" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 05, 2016</p>
    </div>
    <a href="#comment-224036">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224036" class="permalink" rel="bookmark">I guess a peer-to-peer</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I guess a peer-to-peer search engine like YaCy is still a long way out? In the shorter term, I wonder if free (untrusted) federation would be possible among multiple operators running the software. I'll have to read more about it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224037"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224037" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  asn
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">George Kadianakis</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/33">George Kadianakis</a> said:</p>
      <p class="date-time">December 05, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224036" class="permalink" rel="bookmark">I guess a peer-to-peer</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224037">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224037" class="permalink" rel="bookmark">I don&#039;t know much about</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't know much about distributed search engines but YaCy indeed seems to be a long way out. It's underdeveloped and has had fundamental issues over the years.</p>
<p>We should look on whether there has been any research on this area in the past years, but it's definitely a very hard problem to solve (depending on the desired security properties).</p>
<p>Secure naming systems are other better-understood systems that might help with improving the UX of onion services. Also, onion services have long-term identity keys that could be used to provide some sort of authenticity even if the search engine is a centralized entity.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224207"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224207" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224037" class="permalink" rel="bookmark">I don&#039;t know much about</a> by <a class="tor" title="View user profile." href="/user/33">George Kadianakis</a></p>
    <a href="#comment-224207">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224207" class="permalink" rel="bookmark">Secure naming was a hard</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Secure naming was a hard problem too until the relatively recent discovery of the blockchain (which interestingly solved quite a lot of other formerly-hard problems, too), and Namecoin even has provisions for associating .bit names with .onion and .i2p addresses. I'm really unpleasantly surprised that Namecoin still hasn't gained much traction, even in the deep web.</p>
<p>That said, I don't see how secure naming is a replacement for a search engine from a usability perspective. They're both useful, but they both solve different problems. I've never used or heard of Ahmia before this blog post, but onion search engines, even if centralized, are a good thing, and I guess for now we'll have to rely on diversity and numbers for availability and censorship-resistance in leu of a federated network of search engines or a peer-to-peer approach.</p>
<p>As for .onions providing authenticity, they do in the same sense that conventional clearnet sites do in relation to search engines. In other words, they authenticate the content, but only after the search has lead to it. I interpret the advantages of a secure distributed search engine as decentralization, censorship-resistance, and maybe privacy, but I suppose we would have to come up with a clear and specific threat model before tackling the problem.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-224039"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224039" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 05, 2016</p>
    </div>
    <a href="#comment-224039">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224039" class="permalink" rel="bookmark">There are much better search</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>There are much better search engines out there. Just saying.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224075"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224075" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224039" class="permalink" rel="bookmark">There are much better search</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224075">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224075" class="permalink" rel="bookmark">Would you care to enlighten</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Would you care to enlighten us? Without a link to some of these alternative .onion search engines your comment doesn't really add anything.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224151"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224151" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224075" class="permalink" rel="bookmark">Would you care to enlighten</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224151">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224151" class="permalink" rel="bookmark">No, this is not time for it.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No, this is not time for it. This is a donation drive.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-224081"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224081" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
    <a href="#comment-224081">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224081" class="permalink" rel="bookmark">Just a question (no malice</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just a question (no malice whatsoever) : does Ahmia honor "robots.txt", their general and/or specific prohibitions ? If it does, what user-agent(s) does it use or recognize as its own ?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224087"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224087" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  pastly
  </article>
    <div class="comment-header">
      <p class="comment__submitted">pastly said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224081" class="permalink" rel="bookmark">Just a question (no malice</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224087">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224087" class="permalink" rel="bookmark">Hey good question!
I looked</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hey good question!</p>
<p>I looked it up, and it looks like they do respect robots.txt. I didn't quickly find what user agent they use, though.</p>
<p><a href="https://ahmia.fi/documentation/indexing/" rel="nofollow">https://ahmia.fi/documentation/indexing/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-224103"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224103" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
    <a href="#comment-224103">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224103" class="permalink" rel="bookmark">Ahmia link in your article</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ahmia link in your article returns "502 Bad Gateway<br />
nginx/1.6.2" message.</p>
<p>Useless.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224108"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224108" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  asn
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">George Kadianakis</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/33">George Kadianakis</a> said:</p>
      <p class="date-time">December 06, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224103" class="permalink" rel="bookmark">Ahmia link in your article</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224108">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224108" class="permalink" rel="bookmark">Sorry for the bad</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry for the bad experience. I notified the operator.</p>
<p>Ahmia is a one-person project right now, so some patience and understanding is needed. Thanks :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224187"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224187" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224103" class="permalink" rel="bookmark">Ahmia link in your article</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224187">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224187" class="permalink" rel="bookmark">Keep in mind there&#039;s a good</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Keep in mind there's a good possibility the site is under high load due to this blog post. I'm sure they're working on it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224293"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224293" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  asn
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">George Kadianakis</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/33">George Kadianakis</a> said:</p>
      <p class="date-time">December 07, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224187" class="permalink" rel="bookmark">Keep in mind there&#039;s a good</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224293">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224293" class="permalink" rel="bookmark">Also see</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Also see <a href="https://lists.torproject.org/pipermail/tor-talk/2016-December/042667.html" rel="nofollow">https://lists.torproject.org/pipermail/tor-talk/2016-December/042667.ht…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div><a id="comment-230613"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-230613" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 11, 2017</p>
    </div>
    <a href="#comment-230613">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-230613" class="permalink" rel="bookmark">Does ahmia censor any search</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does ahmia censor any search results based on any country's laws or opinions, or is it entirely based on freedom of speech?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-230730"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-230730" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 12, 2017</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-230613" class="permalink" rel="bookmark">Does ahmia censor any search</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-230730">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-230730" class="permalink" rel="bookmark">I believe the answer is no,</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I believe the answer is no, they don't do what they do based on laws from a particular country.</p>
<p>But they do choose not to list certain onion addresses, based on their own decisions about what they want their site to be.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
