title: Tor Weekly News — June 3rd, 2015
---
pub_date: 2015-06-03
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the hundredth issue of Tor Weekly News, the weekly newsletter that covers what’s happening in the Tor community.</p>

<h1>United Nations Special Rapporteur endorses Tor</h1>

<p>David Kaye, the United Nations Special Rapporteur on freedom of opinion and expression, last week issued his <a href="http://www.ohchr.org/EN/Issues/FreedomOpinion/Pages/CallForSubmission.aspx" rel="nofollow">first report</a> to the UN Human Rights Council addressing the relationship between secure online communication, the freedom of expression, and regulation of these by States and governments. The report draws on submissions by UN member states as well as advocacy groups and non-governmental organizations, including the Tor Project.</p>

<p>Recognizing that “the ability to search the web, develop ideas and communicate securely may be the only way in which many can explore basic aspects of identity, such as one’s gender, religion, ethnicity, national origin or sexuality”, and that the right to free expression must be protected “especially in situations where it is not only the State creating limitations but also society that does not tolerate unconventional opinions or expression”, the report concludes by encouraging “States, civil society organizations, and corporations” to ”engage in a campaign to bring encryption by design and default to users around the world”.</p>

<p>There can be few clearer endorsements of the Tor Project’s work than this; that it comes in a report to the UNHRC only adds to its significance. David Kaye also identified Tor by name as an example of essential anonymity software in an <a href="http://www.washingtonpost.com/blogs/the-switch/wp/2015/05/28/un-report-encryption-is-important-to-human-rights-and-backdoors-undermine-it" rel="nofollow">interview with the Washington Post</a> following the release of his report.</p>

<p>You can read Tor Project board member Wendy Seltzer’s <a href="https://blog.torproject.org/blog/un-special-rapporteur-anonymity-gateway-free-expression" rel="nofollow">analysis</a> of the report on the Tor blog. Thanks to everyone who contributed to the report in its research phase, and to David Kaye for so eloquently making the case for anonymity online.</p>

<h1>Monthly status reports for May 2015</h1>

<p>The wave of regular monthly reports from Tor project members for the month of May has begun. <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000830.html" rel="nofollow">Philipp Winter</a> released his report first (for work on Sybil attack detection), followed by reports from <a href="https://lists.torproject.org/pipermail/tor-reports/2015-May/000831.html" rel="nofollow">Damian Johnson</a> (hacking on Stem and Nyx), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000833.html" rel="nofollow">Karsten Loesing</a> (for work on Sponsors O &amp; R, Onionoo, Metrics, and CollecTor), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000834.html" rel="nofollow">Georg Koppen</a> (developing Tor Browser), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000835.html" rel="nofollow">Jacob Appelbaum</a> (working on outreach), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000836.html" rel="nofollow">Tom Ritter</a> (for work on Tor Browser and Tor network tools), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000837.html" rel="nofollow">Griffin Boyce</a> (reporting on several coding projects and research), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000838.html" rel="nofollow">Nick Mathewson</a> (leading core Tor development, as well as organizational work), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000840.html" rel="nofollow">David Goulet</a> (on onion service research, code review, and Torsocks development), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000841.html" rel="nofollow">George Kadianakis</a> (conducting onion service and security research), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000842.html" rel="nofollow">Juha Nurmi</a> (leading the ahmia.fi project and outreach in Finland), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000843.html" rel="nofollow">Leiah Jansen</a> (creating the Tor Project’s design and branding), <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000844.html" rel="nofollow">Pearl Crescent</a> (developing Tor Browser and Tor Launcher), and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000845.html" rel="nofollow">Isabela Bagueros</a> (project-managing all of the above).</p>

<p>Colin C. sent out the May report for the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000832.html" rel="nofollow">Tor help desk</a>, while Isabela sent out a comprehensive <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000839.html" rel="nofollow">core Tor report</a>, and Mike Perry reported on the Tor Browser team’s <a href="https://lists.torproject.org/pipermail/tor-reports/2015-June/000846.html" rel="nofollow">monthly progress</a>.</p>

<h1>Miscellaneous news</h1>

<p>Anthony G. Basile <a href="https://lists.torproject.org/pipermail/tor-talk/2015-June/038051.html" rel="nofollow">announced</a> version 20150531 of Tor-ramdisk, the micro Linux distribution whose only  purpose is to host a Tor server in an environment that maximizes security and privacy. This release updates Tor and the Linux kernel, along with other key software.</p>

<p>David Goulet <a href="https://lists.torproject.org/pipermail/tor-dev/2015-May/008871.html" rel="nofollow">announced</a> the release of Torsocks 2.1.0, featuring support for TCP fast open, outbound localhost connections, saner warning defaults, and more; see the changelog for full details.</p>

<p>Philipp Winter added <a href="https://lists.torproject.org/pipermail/tor-dev/2015-May/008898.html" rel="nofollow">functionality</a> to <a href="https://gitweb.torproject.org/user/phw/sybilhunter.git/" rel="nofollow">sybilhunter</a>, the tool for detecting attempts to take control of a large part of the Tor network, that produces a visualization of similarities between relay descriptors. “Please let me know if you have any suggestions on how to improve the tool or its visualisation”, writes Philipp.</p>

<p>Matthew Finkel <a href="https://lists.torproject.org/pipermail/tor-dev/2015-May/008883.html" rel="nofollow">updated</a> proposal 237, which concerns making all relays into directory servers by default, to include plans for a NotDir consensus flag, because if the proposal is implemented, relays that do not respond to directory requests will be more unusual (and hence more deserving of a flag) than those that do.</p>

<p>Nick Mathewson sent out a <a href="https://lists.torproject.org/pipermail/tor-dev/2015-June/008917.html" rel="nofollow">draft</a> of proposal 245, which suggests ways to deprecate the mostly-defunct TAP circuit extension protocol without breaking anything.</p>

<p>Thomas White asked for <a href="https://lists.torproject.org/pipermail/tor-talk/2015-May/037985.html" rel="nofollow">feedback</a> (via an online survey) on his  upcoming shared onion service hosting platform: “The priority and thought behind the service is to enable people who aren’t usually confident, skilled or bothered to go through the process of creating their own hidden service, thus outsourcing the development, deployment and maintainance…to a third party company like mine.”</p>

<p>This issue of Tor Weekly News has been assembled by Harmony.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

