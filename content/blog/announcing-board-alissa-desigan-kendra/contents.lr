title: Announcing new Board members
---
pub_date: 2021-05-26
---
author: isabela
---
tags: board of directors
---
summary: We are excited to announce that three new members are joining the Tor Project’s Board of Directors: Alissa Cooper, Desigan (Dees) Chinniah, and Kendra Albert! Each new member comes to Tor with a different set of expertise that will help the organization and our community. At the end of this post, you can read each of their bios.
---
_html_body:

<p>We are excited to announce that three new members are joining the Tor Project’s Board of Directors: Alissa Cooper, Desigan (Dees) Chinniah, and Kendra Albert! Each new member comes to Tor with a different set of expertise that will help the organization and our community. At the end of this post, you can read each of their bios.</p>
<p>Please join us in welcoming Alissa, Dees, and Kendra to the Board!</p>
<p><strong>Alissa Cooper</strong> is a Chief Technology Officer and Fellow at Cisco Systems and served in a variety of leadership roles in the Internet Engineering Task Force (IETF). We are excited that Alissa is joining the Board, her expertise will help Tor continue to mature as an organization. </p>
<blockquote><p>"I am honored to be joining the board of the Tor Project, an organization I have long admired for producing one of the most powerful and enduring privacy technologies on the Internet.”  — Alissa</p>
</blockquote>
<p><strong>Desigan Chinniah</strong> (aka @cyberdees) is a long time supporter of Tor. He is a creative technologist with a strong background in the Free Software movement as well as in the industry with his experience as an investor and on product. We are looking forward to his contribution to the Board and to Tor. </p>
<blockquote><p><em>"I've cheered on Tor from afar for many years during my time at Mozilla Firefox and beyond. More recently I've seen just how powerful it's efforts in privacy infrastructure for the internet can be during the #EndSARS, #FreeHongKong, #BlackLivesMatter and other pivotal movements. I’m humbled and honored to join this special community to push forward their mission."</em> — Dees</p>
</blockquote>
<p><strong>Kendra Albert</strong> is a public interest technology lawyer with a special interest in computer security and in protecting marginalized speakers and users. They serve as a clinical instructor at the Cyberlaw Clinic at Harvard Law School, where they teach students to practice law by working with pro bono clients. We are also honored to have Kendra with us and their legal expertise will be a big bonus to Tor.</p>
<blockquote><p><em>"I have long admired the Tor Project's work in protecting private access to the Internet, especially in a time of increasing crackdowns on adult content and pervasive corporate surveillance. I'm honored to join Tor's board and play a part in its future."</em> — Kendra</p>
</blockquote>
<p>And as a reminder, the other nine members of the <a href="https://www.torproject.org/about/people/#board">Tor Project’s Board</a> are: Bruce Schneier, Cindy Cohn, Chelsea Komlo, Gabriella Coleman, Julius Mittenzwei, Matt Blaze, Nighat Dad, Rabbi Rob, and Ramy Raoof.</p>
<p><strong>Full Biographies of Incoming Board Members:</strong></p>
<p><strong>Alissa Cooper</strong>: Alissa Cooper is a VP/CTO and Fellow at Cisco Systems. Her work advances the state of the art at the intersection of engineering, policy, and technical standards. She previously served as Vice President of Technology Standards at Cisco and in a variety of leadership roles in the Internet Engineering Task Force (IETF), including serving as IETF Chair from 2017 to 2021. She served as the chair of the IANA Stewardship Coordination Group (ICG) from 2014 to 2016. At Cisco she was responsible for driving privacy and policy strategy within the company's portfolio of real-time collaboration products before being appointed as IETF Chair. Prior to joining Cisco, Alissa served as the Chief Computer Scientist at the Center for Democracy and Technology, where she was a leading public interest advocate and technologist on issues related to privacy, net neutrality, and technical standards. Alissa holds a PhD from the Oxford Internet Institute and MS and BS degrees in computer science from Stanford University.</p>
<p><strong>Desigan Chinniah</strong>: Dees or cyberdees, is a creative technologist. He has a portfolio of advisory roles and board positions within technology organizations in areas that include machine learning on encrypted data via homomorphic encryption (Zama), connectivity and edge of the network content delivery within emerging markets (BRCK), and alternative business models for the web via open protocols and web standards (Coil). Dees co-created Grant for the Web, a $100M philanthropic fund to boost open, fair, and inclusive standards and innovation for creators. He occasionally makes early stage investments with a focus on diverse and unrepresented founders. Dees is a stalwart of the web and has had check-ins at various dot-coms most notably almost a decade at Mozilla, starting at Firefox 3.8. A self-confessed geek, Dees lives in London with his wife, Sanne and their two kids, Summer Skye &amp; Kiran Quinn. Visit: <a href="https://desiganchinniah.com">https://desiganchinniah.com</a> for more. </p>
<p><strong>Kendra Albert</strong>: Kendra Albert is a public interest technology lawyer with a special interest in computer security and in protecting marginalized speakers and users. They serve as a clinical instructor at the Cyberlaw Clinic at Harvard Law School, where they teach students to practice law by working with pro bono clients. Kendra is also the founder and director of the Initiative for a Representative First Amendment. Before they joined the Clinic, Kendra worked with Marcia Hofmann at Zeitgeist Law. They serve on the board of the ACLU of Massachusetts, and as a legal advisor for Hacking // Hustling.<br />
 </p>

