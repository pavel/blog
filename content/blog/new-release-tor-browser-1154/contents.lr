title: New Release: Tor Browser 11.5.4 (Android, Windows, macOS, Linux)
---
pub_date: 2022-10-12
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5.4 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5.4 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5.4/).

Our Android release follows our alpha branch to the ESR 102 release train. Please see the [12.0a2 blog post](https://blog.torproject.org/new-alpha-release-tor-browser-120a2/) for details.

Tor Browser 11.5.4 backports the following security updates from Firefox ESR 102.3 to to Firefox ESR 91.13 on Windows, macOS and Linux:
- [CVE-2022-40959](https://www.mozilla.org/en-US/security/advisories/mfsa2022-41/#CVE-2022-40959)
- [CVE-2022-40960](https://www.mozilla.org/en-US/security/advisories/mfsa2022-41/#CVE-2022-40960)
- [CVE-2022-40958](https://www.mozilla.org/en-US/security/advisories/mfsa2022-41/#CVE-2022-40958)
- [CVE-2022-40956](https://www.mozilla.org/en-US/security/advisories/mfsa2022-41/#CVE-2022-40956)
- [CVE-2022-40962](https://www.mozilla.org/en-US/security/advisories/mfsa2022-41/#CVE-2022-40962)

Tor Browser 11.5.4 updates GeckoView on Android to 102.3.0esr. We also backport the following Android-specific security updates from Firefox 104 and 105:
- [CVE-2022-36317](https://www.mozilla.org/en-US/security/advisories/mfsa2022-28/#CVE-2022-36317)
- [CVE-2022-38474](https://www.mozilla.org/en-US/security/advisories/mfsa2022-33/#CVE-2022-38474)
- [CVE-2022-40961](https://www.mozilla.org/en-US/security/advisories/mfsa2022-41/#CVE-2022-40961)

The full changelog since [Tor Browser 11.5.3](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.5) is:

- All Platforms
  - [Bug tor-browser-build#40629](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40629): Bump snowflake version to 9ce1de4eee4e
  - [Bug tor-browser-build#40633](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40633): Remove Team Cymru hard-coded bridges
  - [Bug tor-browser#41326](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41326): Update preference for remoteRecipes
- Windows + macOS + Linux
  - [Bug tor-browser-build#40624](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40624): Change placeholder bridge addresses to make snowflake and meek work with ReachableAddresses/FascistFirewall
  - [Bug tor-browser#41303](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41303): YEC 2022 Takeover for Desktop Stable
  - [Bug tor-browser#41310](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41310): Backport ESR 102.3 security fixes to 91.13-based Tor Browser
  - [Bug tor-browser#41323](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41323): Tor-ify notification bar gradient colors (branding)
  - [Bug tor-browser#41338](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41338): The arrow on the search bar should be flipped for RTL languages
- Windows + macOS
  - [Bug tor-browser#41307](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41307): font whitelist typos
- Android
  - Updated GeckoView to 102.3.0esr
  - [Bug tor-browser#41089](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41089): Add tor-browser build scripts + Makefile to tor-browser
  - [Bug tor-browser#41094](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41094): Enable HTTPS-Only Mode by default in Tor Browser Android
  - [Bug tor-browser#41159](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41159): Remove HTTPS-Everywhere extension from esr102-based Tor Browser Android
  - [Bug tor-browser#41166](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41166): Backport fix for CVE-2022-36317: Long URL would hang Firefox for Android (Bug 1759951)
  - [Bug tor-browser#41167](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41167): Backport fix for CVE-2022-38474: Recording notification not shown when microphone was recording on Android (Bug 1719511)
  - [Bug tor-browser#41302](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41302): YEC 2022 Takeover for Android Stable
  - [Bug tor-browser#41312](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41312): Backport Firefox 105 Android security fixes to 102.3-based Tor Browser
  - [Bug tor-browser#41314](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41314): Add YEC 2022 strings to torbutton and fenix
- Build System
  - Android
    - Update Go to 1.18.7
