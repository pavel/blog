title: Tails 2.6 is out
---
pub_date: 2016-09-20
---
author: tails
---
tags:

tails
anonymous operating system
tails releases
---
categories:

partners
releases
---
_html_body:

<p>This release fixes <a href="https://tails.boum.org/security/Numerous_security_holes_in_2.5/" rel="nofollow">many security issues</a> and users should upgrade as soon as possible.</p>

<h2>New features</h2>

<ul>
<li>We enabled <a href="https://en.wikipedia.org/wiki/address%20space%20layout%20randomization" rel="nofollow">address space layout randomization</a> in the Linux kernel (<span class="geshifilter"><code class="php geshifilter-php">kASLR</code></span>) to improve protection from buffer overflow attacks.</li>
<li>We installed <a href="https://tails.boum.org/contribute/design/random/#rngd" rel="nofollow"><span class="geshifilter"><code class="php geshifilter-php">rngd</code></span></a> to improve the entropy of the random numbers generated on computers that have a hardware random number generator.</li>
</ul>

<h2>Upgrades and changes</h2>

<ul>
<li>Upgrade <em>Tor</em> to <a href="https://blog.torproject.org/blog/tor-0287-released-important-fixes" rel="nofollow">0.2.8.7</a>.</li>
<li>Upgrade <em>Tor Browser</em> to <a href="https://blog.torproject.org/blog/tor-browser-605-released" rel="nofollow">6.0.5</a>.</li>
<li>Upgrade to Linux 4.6. This should improve the support for newer hardware (graphics, Wi-Fi, etc.)</li>
<li>Upgrade <em>Icedove</em> to <a href="https://www.mozilla.org/en-US/thunderbird/45.2.0/releasenotes/" rel="nofollow">45.2.0</a>.</li>
<li>Upgrade <em>Tor Birdy</em> to <a href="https://gitweb.torproject.org/torbirdy.git/tree/ChangeLog?id=0.2.0" rel="nofollow">0.2.0</a>.</li>
<li>Upgrade <em>Electrum</em> to <a href="https://github.com/spesmilo/electrum/blob/7dbb23e8c6acfa40795d861b192c205dbb4b4268/RELEASE-NOTES" rel="nofollow">2.6.4</a>.</li>
<li>Install firmware for Intel SST sound cards (<span class="geshifilter"><code class="php geshifilter-php">firmware<span style="color: #339933;">-</span>intel<span style="color: #339933;">-</span>sound</code></span>).</li>
<li>Install firmware for Texas Instruments Wi-Fi interfaces (<span class="geshifilter"><code class="php geshifilter-php">firmware<span style="color: #339933;">-</span>ti<span style="color: #339933;">-</span>connectivity</code></span>).</li>
<li>Remove <span class="geshifilter"><code class="php geshifilter-php">non<span style="color: #339933;">-</span>free</code></span> APT repositories. We documented how to <a href="https://tails.boum.org/doc/advanced_topics/additional_software/#repository" rel="nofollow">configure additional APT repositories</a> using the persistent volume.</li>
<li>Use a dedicated page as the homepage of <em>Tor Browser</em> so we can customize it for our users.</li>
<li>Set up the trigger for RAM erasure on shutdown earlier in the boot process. This should speed up shutdown and make RAM erasure more robust.</li>
</ul>

<h2>Fixed problems</h2>

<ul>
<li>Disable the automatic configuration of <em>Icedove</em> when using <a href="https://en.wikipedia.org/wiki/OAuth" rel="nofollow">OAuth</a>.<br />
This should fix the automatic configuration for GMail accounts. (<a href="https://labs.riseup.net/code/issues/11536" rel="nofollow">#11536</a>)</li>
<li>Make the <em>Disable all networking</em> and <em>Tor bridge mode</em> options of <em>Tails Greeter</em> more robust. (<a href="https://labs.riseup.net/code/issues/11593" rel="nofollow">#11593</a>)</li>
</ul>

<p>For more details, read our <a href="https://git-tails.immerda.ch/tails/plain/debian/changelog" rel="nofollow">changelog</a>.</p>

<h2>Known issues</h2>

<ul>
<li>For some users memory wiping fails more often than in Tails 2.5, and for some users it fails less often. Please report any such changes to <a href="https://labs.riseup.net/code/issues/11786" rel="nofollow">#11786</a>.</li>
</ul>

<p>See the list of <a href="https://tails.boum.org/support/known_issues/" rel="nofollow">long-standing issues</a>.</p>

<h2>Get Tails 2.6</h2>

<ul>
<li>To install, follow our <a href="https://tails.boum.org/install/" rel="nofollow">installation instructions</a>.</li>
<li>To upgrade, an automatic upgrade is available from 2.5 to 2.6.
<p>If you cannot do an automatic upgrade or if you fail to start after an automatic upgrade, please try to do a <a href="https://tails.boum.org/upgrade/" rel="nofollow">manual upgrade</a>.</p></li>
<li><a href="https://tails.boum.org/install/download/" rel="nofollow">Download Tails 2.6.</a></li>
</ul>

<h2>What's coming up?</h2>

<p>Tails 2.7 is <a href="https://tails.boum.org/contribute/calendar/" rel="nofollow">scheduled</a> for November 8.</p>

<p>Have a look at our <a href="https://labs.riseup.net/code/projects/tails/roadmap" rel="nofollow">roadmap</a> to see where we are heading to.</p>

<p>We need your help and there are many ways to <a href="https://tails.boum.org/contribute/" rel="nofollow">contribute to Tails</a> (<a href="https://tails.boum.org/contribute/how/donate/" rel="nofollow">donating</a> is only one of them). Come <a href="https://tails.boum.org/about/contact/#tails-dev" rel="nofollow">talk to us</a>!</p>

<h2>Support and feedback</h2>

<p>For support and feedback, visit the <a href="https://tails.boum.org/support/" rel="nofollow">Support section</a> on the Tails website.</p>

