title: Privacy Cafe (Lisbon)
---
author: steph
---
start_date: 2020-02-15
---
body:

![](https://privacylx.org/img/privacy-cafe-mill-feb2020.gif)

[English version below]

**Data:** Sábado, 15 de Fevereiro, 2020  

**Hora:** 15:00 - 20:00  

**Local:** [MILL](http://mill.pt/contactos), Calçada do Moinho de Vento, 14B, 1150-236 Lisboa  

**Mapa:** <https://www.openstreetmap.org/way/98032128>

Queres saber como a tua vida está a ser vigiada de forma regular e consistente? Compreende os riscos e aprende a proteger-te e à tua comunidade.

Junta-te a nós para uma tarde de discussões e workshops para conhecer melhor o mundo da vigilância em massa, e aprender sobre as ferramentas básicas que podes usar para proteger a tua privacidade, anonimato e segurança geral online, sem qualquer conhecimento prévio ou base técnica.

Traz os teus próprios dispositivos! Haverá apoio para quem que quiser experimentar algumas das ferramentas que serão discutidas. Nestes workshops, vais aprender como recuperar os teus dados e proteger-te neste época de vigilância em massa.

Algumas das temas que podes encontrar nesta edição do Privacy Cafe:

* Como funciona a Internet
* Circunvenção da censura
* Introdução ao Tor
* Como aceder a websites através do Tor
* Aprenda sobre metadados e como removê-los de imagens e outros arquivos
* O que é uma VPN e como (não) utilizá-la
* Gerenciadores de senhas e senhas seguras
* Lojas de aplicações alternativas e que promovem a tua liberdade
* Como é que somos seguidos online?
* Enviar mensagens de forma segura e fácil (encriptação ponta-a-ponta)
* Software Livre/Libre de Código Aberto com alternativas ao capitalismo de vigilância
* Criar um plano de segurança (modelo de ameaças)

Alguns workshops serão conduzidos em Inglês e outros em Português, dependendo do orador. Mas decorrerão em paralelo, pelo que há sempre alternativa”

Queres propor uma tema, workshop, ou ajudar de outra forma qualquer? Ótimo! Podes falar connosco nos seguintes canais:

* Chat
	+ Matrix: <https://riot.im/app/#/room/#privacylx:matrix.org>
	+ IRC: [IRC (#privacylx on OFTC)](https://webchat.oftc.net/?channels=privacylx)
* Forum: <https://cafe.privacylx.org/t/privacy-cafe-001-mill-lisbon/428>
* Email: [contact@privacylx.org](mailto:contact@privacylx.org)

-----

**Date:** Saturday, 15 February, 2020  

**Time:** 15:00 - 20:00  

**Location:** [MILL](http://mill.pt/contactos), Calçada do Moinho de Vento, 14B, 1150-236 Lisboa  

**Map:** <https://www.openstreetmap.org/way/98032128>

* Portuguese: <https://privacylx.org/events/privacy-cafe-mill-feb2020/>
* English: <https://privacylx.org/en/events/privacy-cafe-mill-feb2020/>

Want to know how your life is being regularly and consistently surveilled? Understand the risks and learn how to protect yourself and your community.

Join us for an afternoon of discussions and hands-on activities to better understand the world of mass surveillance, and to learn about basic tools you can use to protect your privacy, anonymity and overall security online without requiring any previous knowledge on the topic or technical background.

Bring your own devices! Help will be provided to those who want to try some of the tools that will be discussed. During these workshops you will learn how to reclaim your data and protect yourself in the age of mass surveillance.

Some of the topics you can encounter at this edition of the Privacy Cafe:

* How Internet works
* Circumventing censorship
* Introduction to Tor
* How to access websites via Tor
* Learn about metadata and how to remove them from images and others files
* What is a VPN and how (not) to use it
* Password managers and secure passwords
* Alternative app stores for software that promotes your freedom
* Tracking and digital surveillance
* Secure messaging without server dependencies
* Free/Libre Open Source Software alternatives to surveillance capitalism
* Creating your security plan (threat model).

Some workshops will be conducted in English and others in Portuguese, depending on the speaker, but these will happen in parallel so that there is always an alternative.

Want to propose a topic, workshop or otherwise help out? Great! You can reach us through the following channels:

* Chat
	+ Matrix: <https://riot.im/app/#/room/#privacylx:matrix.org>
	+ IRC: [IRC (#privacylx on OFTC)](https://webchat.oftc.net/?channels=privacylx)
* Forum: <https://cafe.privacylx.org/t/privacy-cafe-001-mill-lisbon/428>
* Email: [contact@privacylx.org](mailto:contact@privacylx.org)

